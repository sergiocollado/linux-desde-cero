original text: http://www.linuxfromscratch.org/lfs/view/stable/


# CAPÍTULO 1: INTRODUCCIÓN

## 1.1 COMO CREAR UN SISTEMA LINUX DESDE CERO

Para crear un sistema linux desde cero, LFS ("Linux From Scratch" en inglés) nos basaremos una una distribución ya existente (como Debian, OpenMandriva, Fedora o openSUSE). En este sistema linux, que será el sistema anfitrión, será usado como punto de partida, en el cual usar los programas necesaros, como el compilador o la consola para desarrollar el nuevo sistema. Se ha de selecional la opción de desarrollo ("development") durante la instalación de la distribución para poder acceder a estas herramientas.

Como alternativa a intalar otra distribución se puede usar un LiveCD de otra distribución.

El capítulo 2 de este texto describe como crear una nueva partición linux y un sistema de archivos. En ella el nuevo linux LFS se compilará e instalará. En el capítulo 3 se indica los paquetes y pareches que sera necesario descargar para construir el sistema LFS y como guardarlos en el nuevo sistema de archivos. El capítulo 4 preseta como organizar el entorno de trabajo. Es importante leer el capítulo 4 detenidamente, ya que explica distintos detalles importantes que han de tenerse en cuenta antes de empezar a trabajar en el capítulo 5 y posteriores.

El capítulo 5 explica la instalacion de varios paquetes de software que formarán la base de software que formará la cadena de desarrollo ("toolchain" en inglés) que se usará para montar el sistema en el capítulo 6. Algunos de estos paquetes se usaran para resolver algunas dependencias circulares -- como que para compilar un compilador, se necesita un compilador--

Así el capítulo 5 muestra como construir una primera cadena de desarrollo, incluyendo Binutils y GCC (esta primera aproximación que estos dos progrmas seran reinstalados). El siguiente paso será construir Glibc, la libreria de C. Glibc será compilado con las anteriormente construidas. En una segunda fase la cadena de programas de desarrollo, será construida. Al finalizar este paso, el sistema LFS ya no dependerá de la distribución anfitrión, salvando el nucleo del kernel que se está ejecutando. 

Este efuerzo de aislar el nuevo sistema de la distribución anfitriona, puede parecer excesiva, pero una explicación completa se aporta en la sección 5.2 "Notas técnicas de la cadena de desarrollo". 

En el capítulo 6, se monta el sistema LFS completo. El programa "chroot" (change root) se usa para crear un entorno virtual y que usa una nueva consola cuyo directorio raiz está referenciado a la partición de LFS. Esto es muy cercano a reiniciar el sistema e indicar al kernel que monte el sistema LFS como el directorio raiz. El sistema realmente no es reinicializado, pero en cambio usando "chroot" ya que usar un sistema que pueda arrancar requiere un trabajo no es necesario en este punto del desarrollo. La mayor ventaja de usar "chroot" es que permite seguir
usando el sistema anfitrion mientras se construye todo el LFS. Mientras se ejecutan las compilaciones de los distintos componentes, se puede seguir usando el sistema anfitrion.

Para terminar la instalación, la configuración se explica en el capítulo 7, y el kernel y el bootloader se definen el el capítulo 8. El capítulo 9 tiene la información sobre desarrollos más allá de este texto. Después de que todos los pasos sean completados, se puede reiniciar el computador, con el nuevo sistema LFS. 

Este es el resumen de el proceso. Información detallada, se presenta en cada uno de los pasos y las descripciones del software necesario. Puntos complicados serán explicados y clarificados, y todo encajará por su propio peso al recorrer el desarrollo del sistema LFS.

# CAPÍTULO 2: PREPARANDO EL SISTEMA ANFITRION

## 2.1 INTRODUCCIÓN

En este capítulo se presentan las herramientas necesarias para construir un sistema linux desde cero (LFS) y su instalación. Se creará una partición que albergará al nuevo sistema. Se creará la partición, se creará un sistema de archivos en el y se montará.

## 2.2 REQUERIMIENTOS DEL SISTEMA ANFITRION

El sistema anfritrion deberia tener los siguientes programas, al menos con la version indicada o superior. No deberia haber ningun problema en la mayoria de distribuciones modernas. Pero hay que tener en cuenta que algunas distribuciones guardaran los archivos de cabecera en distintos paquetes, normalmente bajo la forma "<nombre-del-paquete>-devel" o "<nombre-del-paquete>-dev". Hay que asegurarse de intalar dichos paquetes si se proveen con la distribución del sistema anfitrion.

Bash-3.2 (/bin/sh deberia ser un link simbólico o explicito  a la consola bash)

Binutils-2.25 (Versions superiores a 2.31.1 no se recomiendan, ya que no han sido probadas)

Bison-2.7 (/usr/bin/yacc deberia ser un link a bison, o un pequeño script que ejecute bison)

Bzip2-1.0.4

Coreutils-6.9

Diffutils-2.8.1

Findutils-4.2.31

Gawk-4.0.1 (/usr/bin/awk should be a link to gawk)

GCC-4.9 incluyendo el compilador de C++, g++ (Versiones superiores a 8.2.0 no se recomiendan ya que no han sido probadas) 

Glibc-2.11 (Versions superiores a 2.28 no se recomiendan ya que no han sido probadas)

Grep-2.5.1a

Gzip-1.3.12

Linux Kernel-3.2

El motivo para esa versión especifica del kernel es que es necesaria para la construcción de glibc en el capítulo 6, y la recomendación de los desarrolladores. También es encesaria para udev.

Si el kernel del sistema anfitrion es anterior a 3.2, se encesitará reemplazar el kerenl con una versión más actual. Hay dos modos para realizar esto. Primero, investigar si le proveedor de Linux le posee un paquete con el kernel 3.2 o posterior. Is es así, puede intalarlo. Si no existe esa posibilidad, puede compilar el kernel usted mismo. Instrucciones para compilar el krenel y configurar el bootlooder (suponiendo que se use GRUB) se encuentran en el capítulo 8.

M4-1.4.10

Make-4.0

Patch-2.5.4

Perl-5.8.8

Sed-4.1.5

Tar-1.22

Texinfo-4.7

Xz-5.0.0

Aviso:
Los links simbólicos señalados anteriormente son necesarios para construir el sistema LFS, usando las instruccines en este texto. Los links simbólicos apuntan a otros elementos de software (como dash, mawk ...) pueden funcionar pero no estan verificados o soportados por el equipo de desarrollo de LFS, y pueden requerir acciones adicionales o parches u otros paquetes de software.

Para verificar las versiones de los programas necesarios se puede usar el siguiente scrip

```bash
#!/bin/bash
# Script para verificar los numeros de version de los programas necesarios.
export LC_ALL=C
bash --version | head -n1 | cut -d" " -f2-4
MYSH=$(readlink -f /bin/sh)
echo "/bin/sh -> $MYSH"
echo $MYSH | grep -q bash || echo "ERROR: /bin/sh no apunta a bash"
unset MYSH

echo -n "Binutils: "; ld --version | head -n1 | cut -d" " -f3-
bison --version | head -n1

if [ -h /usr/bin/yacc ]; then
  echo "/usr/bin/yacc -> `readlink -f /usr/bin/yacc`";
elif [ -x /usr/bin/yacc ]; then
  echo yacc is `/usr/bin/yacc --version | head -n1`
else
  echo "yacc na sido encontrado" 
fi

bzip2 --version 2>&1 < /dev/null | head -n1 | cut -d" " -f1,6-
echo -n "Coreutils: "; chown --version | head -n1 | cut -d")" -f2
diff --version | head -n1
find --version | head -n1
gawk --version | head -n1

if [ -h /usr/bin/awk ]; then
  echo "/usr/bin/awk -> `readlink -f /usr/bin/awk`";
elif [ -x /usr/bin/awk ]; then
  echo awk is `/usr/bin/awk --version | head -n1`
else 
  echo "awk no ha sido encontrado" 
fi

gcc --version | head -n1
g++ --version | head -n1
ldd --version | head -n1 | cut -d" " -f2-  # glibc version
grep --version | head -n1
gzip --version | head -n1
cat /proc/version
m4 --version | head -n1
make --version | head -n1
patch --version | head -n1
echo Perl `perl -V:version`
sed --version | head -n1
tar --version | head -n1
makeinfo --version | head -n1
xz --version | head -n1

echo 'int main(){}' > dummy.c && g++ -o dummy dummy.c
if [ -x dummy ]
  then echo "g++ compilation OK";
  else echo "g++ compilation failed"; fi
rm -f dummy.c dummy
```
## 2.3 ETAPAS PARA CONSTRUIR LFS

El sistema LFS está pensado para ser construido en una sola sesión. Esto quiere significar que el sistema no será apagado durante el proceso. Pero no que tenga que construirse de una sola vez. La clave es que ciertos procedimientos han de ser repetidos después de un reinicio si se continua montando el sistema LFS despues de reiniciar el computador, en distintos puntos del desarrollo.

### 2.3.1. CAPÍTULOS 1-4

Estos capítulos se desarrollan en el sistema anfitrion. Al reiniciar el sistema ha de tenerse en cuenta lo siguiente:

- Los procesos hechos como el usuario raiz después de la sección 2.4, necesitan que las variables de entorno definidas para el usuario raiz.


### 2.3.2 CAPÍTULO 5

- Se ha de montar la partición /mnt/lfs.

- Se ha de ejecutar 'su -lfs' antes de realizar cualquier acción en el capítulo 5. Todas las instrucciones del capítulo 5 han de realizarse con el usuario **lfs**

- Las instrucciones y procedimientos de la sección '5.3 Instrucciones generales de compilación' son cruciales. Si hay cualquier duda sobre si se ha de instalar cualquier paquete, asegurarse primero de haber eliminado todas las tarballs, re-extraido todos los ficheros, y haber completado todas las indicaciones de esa sección.

### 2.3.3 CAPÍTULOS 6-8

- Se ha de montar la partición /mnt/lfs.

- Al entrar en chroot, la varialble de entorno LFS se ha de definir a 'root'. A parte de eso, la variable de entorno LFS, no se usa más.

- Se ha de montar el sistema de archivos virtual. Esto se puede realizar antes o después de entrar en chroot, cambiandolo mediante una consola del sistema anfitrión, y como el usuario 'root' ejecutar los comandos de la sección '6.2.2 Montando y populando /dev' y en la sección 6.2.3 'Montando el sistema de archivos Virtual del kernel'.

- Al entrar en chroot, la varialble de entorno LFS se ha de definir a 'root'.


## 2.4 Creación de una nueva partición
 
Igual que la mayoría de los sistemas operativos, LFS normalmente se instala en una partición dedicada. La recomendación es construir el sistema LFS en una partición vacía disponible, o si se tiene suficiente espacio sin particiones, crear una partición.
 
Un sistema mínimo requiere una partición de 6 gigabytes (GB). Esto es suficiente para guardar todas las tarballs del código y compilar los paquetes. De todas formas, si el sistema LFS está destinado a ser el sistema Linux primario, se necesitará más espacio. Una partición de 20 GB es una tamaño razonable, que todavía conserva espacio para crecer. El sistema LFS en si mismo no necesitaría tanto espacio. Una gran parte de los requerimientos de este espacio es tener suficiente espacio libre de forma temporal, así como también poder añadir más elementos al sistema LFS una vez sea completado. También, la compilación de paquetes necesita mucho espacio de memoria, pero esta se recuperará y estará disponible después de que el paquete se haya instalado.
 
Ya que muchas veces no siempre se dispone de suficiente memoria RAM, para la compilación de procesos, es una buena idea usar una pequeña partición del disco como espacio ‘swap’. Esto será usado por el kernel para guardar datos, que sean usados poco frecuentemente, y liberar más memoria que estará disponible para procesos activos. La partición swap para un sistema LFS puede ser la misma que la usado por el sistema anfitrión, en tal caso, no es necesario crearla.
 
Se utilizará un programa de particiones como cfdisk o fdisk con la posibilidad de usar comandos con la opción de nombrar el disco duro en el que se creará la nueva partición - por ejemplo /dev/sda para el IDE (Integrated Drive Electronics). Habrá que crear una partición swap nativa, si se necesita. Por favor usar las referencias cfdisk(8) o fdisk(8) en caso que no se conozca todavía cómo usar esos programas.

NOTA:  Para los usuarios avanzados, comentar que pueden usarse otros esquemas de particiones. El nuevo sistema LFS puede ser usado en un  sistema RAID o en un volumen LVM. De todas formas algunas de estas opciones requieren el uso de ‘initramfs’ que es un concepto avanzado. Estas metodologías de particiones no son recomendadas para un primer intento al crear un sistema LFS.

Recordad el nombre de la nueva partición (ejemplo: sda5). En este texto, nos referiremos a ella como la partición LFS. También recordar la designación del la partición swap. Estos nombres 
se necesitarán más adelante para el archivo /etc/fstab.

### 2.4.1 Otras notas sobre las particiones
Frequentemente se piden recomendaciones sobre las particiones en las listas de correo de LFS. Este es un tema muy subjetivo. Lo normal para la mayoría de distribuciones es usar la totalidad del espacio con excepción de una pequeña partición swap. Esto no es lo más óptimo para LFS por varias razones. REduce la flexibilidad, hace más difícil compartir datos entre las distintas distribuciones o es más complicado construir el sistema lfs, las  copias de seguridad requieren de más tiempo, y se puede desperdiciar espacio de disco al asignar las distintas estructuras de sistemas de archivo. 

#### 2.4.1.1. La partición raíz (‘root’)
La partición raíz (‘root’) (que no se ha de confundir con el directorio /root), un espacio de 10GB es un buen compromiso para la mayoría de sistemas. Esto permite suficiente espacio para construir el sistema LFS y la mayoría de BLFS, pero es suficientemente pequeña, que permite que varias particiones se puedan crear para experimentar.

#### 2.4.1.2. La partición Swap
La mayoría de distribuciones crean automáticamente una partición swap. Generalmente el tamaño recomendado de la partición swap es el doble de la memoria RAM física, de todas formas no se necesita frecuentemente. Si el espacio de disco es limitado, se puede usar una partición swap de 2 BG, y monitorizar la cantidad de memoria que está siendo swapped.

#### 2.4.1.3. La partición de la Bios Grub
Si el disco de arranque (‘boot disk’) ha sido particionado con una tabla GUID (GPT- Guid Partition Table’), entonces una parte, normalmente 1 MB, de la partición ha de ser creado, si no ha sido creado anteriormente. La partición no se formatea, y ha de permanecer disponible para GRUB para ser usada en la instalación del programa de arranque (‘boot loader’). Esta partición normalmente se etiquetará como ’BIOS Boot’ si se usa **fdisk** o tendrá un código EF02 si se usa **gdisk**

NOTA: la partición de la Bios Grub debe estar en el disco que la BIOS usa para arrancar el sistema. Esto no es necesariamente el disco donde la partición raíz LFS	ha de estar. Los discos en un sistema pueden usar distintos tipos de tablas de partición. Los requerimientos para esta partición dependen únicamente en del tipo de tabla de partición del disco de arranque.


#### 2.4.1.4. Otras particiones recomendadas
Hay otras particiones que no son necesarias, pero deberían considerarse cuando se diseña la organización del disco. La lista a continuación, pretende ser una guía.

- /boot - Altamente recomendada. Esta partición se usa para guardar los kernels y otra información en el arranque. Para minimizar potenciales problemas de arranque con discos, ésta ha de ser la primera partición física del disco. Una partición con un tamaño de 100 MB es adecuada.
- /home - Altamente recomendada. Para compartir el directorio del usuario y modificaciones entre múltiples distribuciones o sistemas LFS. El tamaño suele ser grande y depende de la memoria y espacio disponible.
- /usr - Una partición /usr separada, normalmente puede usarse para tener un servidor para un pequeño cliente o acceso remoto. Esto no es algo que se suela usar en un sistema LFS. Una partición de 5 GB es un tamaño adecuado para la mayoría de casos.
- /opt - este directorio es el mas util en BLFS donde múltiples instalaciones de paquetes como Gnome o KDE pueden ser instaladas, sin necesitar incluir los ficheros en la jerarquía /usr. Si se usa, 5 o 10 GB suelen ser adecuados.
- /tmp - Una partición /tmp separada es rara, pero es útil si se configura un pequeño cliente. En esta partición si se usa, normalmente no necesitará ser mayor de 2 GB.
- /usr/src - Esta partición es muy útil para proveer un lugar donde guardar el código de BLFS y compartirlo entre distintos LFS. También puede usarse para paquetes BLFS. 	Es razonable usar una partición de entre 30-50 GB.

Cualquier partición separada que se quiera montar el arrancar el sistema, necesita ser especificada en /etc/fstab. Detalles sobre cómo definirlas las particiones se presentarán en la sección 8.2 “Creando el archivo /etc/fstab”.


## 2.5 Creación de un sistema de archivos en la partición.

Una vez se ha creado una nueva partición se puede crear en ella un sistema de archivos. LFS puede usar cualquier sistema de archivos reconocido por el kernel de Linux, pero los más típicos son ext3 y ext4. La elección del tipo de sistema de archivos puede ser compleja y dependerá de las características de los archivos y el tamaño de la partición. Por ejemplo:

 - ext2: es adecuado para pequeñas particiones que no se actualizan frecuentemente, por ejemplo /boot.
 - ext3: es una mejora de ext2, que incluye un diario para ayudar a recuperar el estado de la partición en caso de una apagado forzado. Normalmente se usa como un sistema de archivos de propósito general.
 - ext4: La última versión del sistema de archivos ext. Incluye varias mejoras, como gestión de tiempo en nanosegundos, creación y gestión de archivos de gran tamaño (16TB), y mejoras en su velocidad.

Otros sistemas de archivos incluyen FAT32, NTFS, ReiserFS, JFS, y XFS que se usan para fines especializados. Se puede encontrar más información sobre estos tipos de sistemas de archivos en: https://en.wikipedia.org/wiki/Comparison_of_file_systems

LFS supone que el sistema de archivos raíz (/) es de tipo ext4. Para crear un sistema de archivos ext4, en la partición LFS, se haria como:

```bash
mkfs -v -t ext4 /dev/<xxx>
```

Si se está usando una partición swap, no hace falta formatearla. Si se está creando una nueva partición *swap*, se necesitará inicializar con el comando:

```bash
mkswap /dev/<yyy>
```

Hay que sustituir yyy con el nombre de la partición swap.

## 2.6 Definiendo la variable $LFS

A lo largo de este texto la variable LFS se usará varias veces. Se ha de asegurar que esta variable este siempre definida a lo largo de todo el proceso de construcción del sistema LFS. Debería estar definida con el nombre del directorio donde se construirá el sistema LFS. - Usaremos /mnt/lfs como ejemplo, pero la elección del directorio es libre. Si se está construyendo el sistema LFS en una partición separada, este directorio será el punto en el que se monte la partición. Se definirá la variable con el siguiente comando:

```bash
export LFS=/mnt/lfs
```

La definición de esta variable es beneficiosa, porque comandos como ‘mkdir -v $LFS/tools’ pueden ser escritos literalmente. La consola sustituirá automáticamente ‘$LFS´ con /mnt/lfs o cualquiera que sea su definición correspondiente. 

CUIDADO: No se debe olvidar verificar si la variable LFS está definida, siempre que se salga y se vuelva a entrar en el directorio de trabajo (como cuando se hace un `su` a ‘root’ u a otro usuario). Se puede verificar su valor con el comando: 

``` 
	Echo $LFS
```

Debe asegurarse que la respuesta a este comando, muestra la dirección del lugar donde se está construyendo el sistema, que según el ejemplo sería ‘/mnt/lfs’. Si la respuesta no coincide debe utilizarse el comando especificado anteriormente para definir la dirección correcta. 

NOTA: Un modo de asegurarse que la variable siempre tiene un valor correcto, es editar el archivo .bash_profile, tanto en el directorio ‘home’ del usuario, y en ‘/root/.bash_profile’ y añadir el comando anterior. Además, la consola, especificada en /etc/passwd file para todos los usuarios, que necesita la variable ‘LFS’ ha de ser ‘bash’ para asegurar, que se incorpora el archivo ‘/root/.bash_profile’ como parte del proceso de login.

Otra consideración es el método que se usa para acceder al sistema anfitrión. Si al acceder se hace a partir de un entorno gráfico, el archivo ‘.bash_profile’ no se usará normalmente cuando se lance un terminal virtual. Además algunas distribuciones no ejecutan las instrucciones .bashrc al invocar la consola bash en sesiones no-interactivas. Hay que asegurarse de añadir el comando de exportar, antes de probar las sesiones no-interactivas.

## 2.7 Montando la nueva partición

Una vez que un sistema de archivos a sido creado, la partición necesita ser hecha accesible. Para hacer esto, la partición necesita ser montada en un punto dado. Para los propósitos de este texto, se asume que el sistema de archivos, necesita ser montado en la dirección apuntada por la variable LFS, como se describió en la sección anterior.

Para montar la partición en ese punto se usa el comando:

```bash
mkdir -pv $LFS
mount -v -t ext4 /dev/<xxx> $LFS
```

Hay que reemplazar xxx con la designación de la partición LFS.

Si se usan múltiples particiones LFS (por ejemplo una para / y otra para /usr), se pueden montar así:


```bash
mkdir -pv $LFS
mount -v -t ext4 /dev/<xxx> $LFS
mkdir -v $LFS/usr
mount -v -t ext4 /dev/<yyy> $LFS/usr
```

Habrá que reemplazar xxx y yyy con sus nombres correspondientes.

Hay que asegurarse que la nueva partición está instalada, con permisos que no sean demasiado restrictivos (como el de las opciones ‘nosuid’ o ‘nodev’). Hay que ejecutar el comando ‘mount’ sin parámetros para verificar que opciones se definen para la partición LFS. Si las opcioens ‘nosuid’ o ‘nodev’ están definidas, entonces la partición ha de remontase.

AVISO: Las instrucciones anteriores, asumen que no se va a reiniciar el computador a lo largo del proceso del sistema LFS. Si se apaga el sistema será necesario o remontar la partición cada vez se reinicie el proceso de construcción o modificar el archivo del  sistema anfitrión /etc/fstab para que remonte la partición automáticamente cada vez que se reinicie. Por ejemplo:

```bash
/dev/<xxx>  /mnt/lfs ext4   defaults      1     1
```

Si se usan particiones adicionales, estas deberan incluirse también.

Si se está usando una partición swap, hay que asegurarse que está habilitada, con el comando `swapon’ 

```bash
/sbin/swapon -v /dev/<zzz>
```

Hay que sustituir zzz con el nombre de la partición swap.

Ahora que hay establecido un espacio de trabajo, es el momento de descargar los paquetes necesarios. 


# 3 Paquetes y parches

## 3.1 Introducción

Este capítulo presenta una serie de paquetes que necesitan ser descargados para construir un sistema básico de Linux. Las versiones de software indicadas, corresponden a las versiones que se ha verificado que funcionan y este texto se basa en ellas. Se desaconseja el uso de versiones más nuevas, porque los comandos de una versión pueden no funcionar en una más reciente. Los paquetes de software más recientes pueden presentar también problemas que requieren otras soluciones. Estas soluciones, se verificarán y estabilizarán por el equipo de desarrollo de este texto.

Las direcciones para su descarga, pueden no siempre ser accesibles. Si una dirección ha cambiado desde que se escribió este texto, Google (http://www.google.com/) proporciona un buscador muy util para la mayoría de los paquetes. Si esto tuviera éxito se puede probar con una alternativa a esa descarga, como se discute en  http://www.linuxfromscratch.org/lfs/packages.html#packages.

Los paquetes descargados y parches necesitarán ser guardados en un directorio conveniente que sea accesible durante todo el proceso de construcción. Un directorio en el que también se puedan descomprimir las fuentes y construirlas. $LFS/sources puede ser usado tanto para guardar las ‘tarballs’ y parches, como para directorio de trabajo. Usando este directorio, los elementos necesarios, se localizan en la partición LFS, y estarán disponibles, durante todas las etapas del proceso de desarrollo.

Para crear este directorio, se ejecutará el siguiente comando, como usuario raíz (‘root’), antes de comenzar las descargas.

```bash
Mkdir -v $LFS/sources
```

Este directorio ha de tener permisos de escritura y ‘sticky’. ‘Sticky’ quiere indicar que en un directorio aunque varios usuarios tengan permiso de escritura en un directorio, únicamente el propietario de un archivo, puede borrarlo dentro de ese directorio. El siguiente comando puede la escritura y el modo ‘sticky’:

```bash
chmod -v a+wt $LFS/sources
```

Un método sencillo de descargar todos los paquetes y parches necesarios es usando wget-list como partida del comando ‘wget’. Por ejemplo:

```bash
wget --input-file=wget-list --continue --directory-prefix=$LFS/sources
```

Además, a partir de LFS-7.0, existe un archivo,  md5sums, que puede ser usado, para verificar, que todos los paquetes correctos están disponibles antes de proceder. Hay que guardar este archivo en $LFS/sources y ejecutar:

```bash
pushd $LFS/sources
md5sum -c md5sums
popd
```


# 3.2 TODOS LOS PAQUETES

Hay que descargar u obtener los siguientes paquetes:

Acl (2.2.53) - 513 KB: <br>  
Download: http://download.savannah.gnu.org/releases/acl/acl-2.2.53.tar.gz <br>  
MD5 sum: 007aabf1dbb550bcddde52a244cd1070 <br>  

Attr (2.4.48) - 457 KB: <br>  
Home page: https://savannah.nongnu.org/projects/attr <br>  
Download: http://download.savannah.gnu.org/releases/attr/attr-2.4.48.tar.gz <br>  
MD5 sum: bc1e5cb5c96d99b24886f1f527d3bb3d <br>  

Autoconf (2.69) - 1,186 KB: <br>  
Home page: http://www.gnu.org/software/autoconf/ <br>  
Download: http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.xz <br>  
MD5 sum: 50f97f4159805e374639a73e2636f22e <br>  

Automake (1.16.1) - 1,499 KB: <br>  
Home page: http://www.gnu.org/software/automake/ <br>  
Download: http://ftp.gnu.org/gnu/automake/automake-1.16.1.tar.xz <br>  
MD5 sum: 53f38e7591fa57c3d2cee682be668e5b <br>  

Bash (4.4.18) - 9,242 KB: <br>  
Home page: http://www.gnu.org/software/bash/ <br>  
Download: http://ftp.gnu.org/gnu/bash/bash-4.4.18.tar.gz <br>  
MD5 sum: 518e2c187cc11a17040f0915dddce54e <br>  

Bc (1.07.1) - 411 KB: <br>  
Home page: http://www.gnu.org/software/bc/ <br>  
Download: http://ftp.gnu.org/gnu/bc/bc-1.07.1.tar.gz <br>  
MD5 sum: cda93857418655ea43590736fc3ca9fc <br>  

Binutils (2.31.1) - 19,989 KB: <br>  
Home page: http://www.gnu.org/software/binutils/ <br>  
Download: http://ftp.gnu.org/gnu/binutils/binutils-2.31.1.tar.xz <br>  
MD5 sum: 5b7c9d4ce96f507d95c1b9a255e52418 <br>  

Bison (3.0.5) - 1,910 KB: <br>  
Home page: http://www.gnu.org/software/bison/ <br>  
Download: http://ftp.gnu.org/gnu/bison/bison-3.0.5.tar.xz <br>  
MD5 sum: 3e54f20988ecd1b62044e25481e5f06b <br>  

Bzip2 (1.0.6) - 764 KB: <br>  
Download: http://anduin.linuxfromscratch.org/LFS/bzip2-1.0.6.tar.gz <br>  
MD5 sum: 00b516f4704d4a7cb50a1d97e6e8e15b <br>  

Check (0.12.0) - 747 KB: <br>  
Home page: https://libcheck.github.io/check <br>  
Download: https://github.com/libcheck/check/releases/download/0.12.0/check-0.12.0.tar.gz <br>  
MD5 sum: 31b17c6075820a434119592941186f70 <br>  

Coreutils (8.30) - 5,234 KB: <br>  
Home page: http://www.gnu.org/software/coreutils/ <br>  
Download: http://ftp.gnu.org/gnu/coreutils/coreutils-8.30.tar.xz <br>  
MD5 sum: ab06d68949758971fe744db66b572816 <br>  

DejaGNU (1.6.1) - 514 KB: <br>  
Home page: http://www.gnu.org/software/dejagnu/ <br>  
Download: http://ftp.gnu.org/gnu/dejagnu/dejagnu-1.6.1.tar.gz <br>  
MD5 sum: 2ac8405a4c7ca8611d004fe852966c6f <br>  

Diffutils (3.6) - 1,366 KB: <br>  
Home page: http://www.gnu.org/software/diffutils/ <br>  
Download: http://ftp.gnu.org/gnu/diffutils/diffutils-3.6.tar.xz <br>  
MD5 sum: 07cf286672ced26fba54cd0313bdc071 <br>  

Eudev (3.2.5) - 1,814 KB: <br>  
Download: https://dev.gentoo.org/~blueness/eudev/eudev-3.2.5.tar.gz <br>  
MD5 sum: 6ca08c0e14380f87df8e8aceac123671 <br>  

E2fsprogs (1.44.3) - 7,394 KB: <br>  
Home page: http://e2fsprogs.sourceforge.net/ <br>  
Download: https://downloads.sourceforge.net/project/e2fsprogs/e2fsprogs/v1.44.3/e2fsprogs-1.44.3.tar.gz <br>  
MD5 sum: 6bd765f3cf8f15740cdf81e71e88f2a4 <br>  

Elfutils (0.173) - 8,482 KB: <br>  
Home page: https://sourceware.org/ftp/elfutils/ <br>  
Download: https://sourceware.org/ftp/elfutils/0.173/elfutils-0.173.tar.bz2 <br>  
MD5 sum: 35decb1ebfb90d565e4c411bee4185cc <br>  

Expat (2.2.6) - 502 KB: <br>  
Home page: https://libexpat.github.io/ <br>  
Download: https://prdownloads.sourceforge.net/expat/expat-2.2.6.tar.bz2 <br>  
MD5 sum: ca047ae951b40020ac831c28859161b2 <br>  

Expect (5.45.4) - 618 KB: <br>  
Home page: https://core.tcl.tk/expect/ <br>  
Download: https://prdownloads.sourceforge.net/expect/expect5.45.4.tar.gz <br>  
MD5 sum: 00fce8de158422f5ccd2666512329bd2 <br>  

File (5.34) - 821 KB: <br>  
Home page: https://www.darwinsys.com/file/ <br>  
Download: ftp://ftp.astron.com/pub/file/file-5.34.tar.gz <br>  
MD5 sum: 44b0b6983462b18f96403d4d3ad80254 <br>  
 
Nota:  <br>  
File (5.34) El archivo puede no estar disponible en la dirección indicada. Los administradores del sitio suelen retirar las versiones antiguas cuando se presentan nuevas. Una opción alternativa es usar una dirección que contenga la versión correcta, como:
http://www.linuxfromscratch.org/lfs/download.html#ftp.

Findutils (4.6.0) - 3,692 KB: <br>  
Home page: http://www.gnu.org/software/findutils/ <br>  
Download: http://ftp.gnu.org/gnu/findutils/findutils-4.6.0.tar.gz <br>  
MD5 sum: 9936aa8009438ce185bea2694a997fc1 <br>  

Flex (2.6.4) - 1,386 KB: <br>  
Home page: https://github.comwestes/flex <br>  
Download: https://github.com/westes/flex/releases/download/v2.6.4/flex-2.6.4.tar.gz <br>  
MD5 sum: 2882e3179748cc9f9c23ec593d6adc8d <br>  

Gawk (4.2.1) - 2,916 KB: <br>  
Home page: http://www.gnu.org/software/gawk/ <br>  
Download: http://ftp.gnu.org/gnu/gawk/gawk-4.2.1.tar.xz <br>  
MD5 sum: 95cf553f50ec9f386b5dfcd67f30180a <br>  

GCC (8.2.0) - 61,974 KB: <br>  
Home page: https://gcc.gnu.org/ <br>  
Download: http://ftp.gnu.org/gnu/gcc/gcc-8.2.0/gcc-8.2.0.tar.xz <br>  
MD5 sum: 4ab282f414676496483b3e1793d07862 <br>  

GDBM (1.17) - 917 KB: <br>  
Home page: http://www.gnu.org/software/gdbm/ <br>  
Download: http://ftp.gnu.org/gnu/gdbm/gdbm-1.17.tar.gz <br>  
MD5 sum: f20ce117abc6f302ecf62c34d41c1ecf <br>  

Gettext (0.19.8.1) - 7,041 KB: <br>  
Home page: http://www.gnu.org/software/gettext/ <br>  
Download: http://ftp.gnu.org/gnu/gettext/gettext-0.19.8.1.tar.xz <br>  
MD5 sum: df3f5690eaa30fd228537b00cb7b7590 <br>  

Glibc (2.28) - 16,098 KB: <br>  
Home page: http://www.gnu.org/software/libc/ <br>  
Download: http://ftp.gnu.org/gnu/glibc/glibc-2.28.tar.xz <br>  
MD5 sum: c81d2388896379997bc359d4f2084239 <br>  

GMP (6.1.2) - 1,901 KB: <br>  
Home page: http://www.gnu.org/software/gmp/ <br>  
Download: http://ftp.gnu.org/gnu/gmp/gmp-6.1.2.tar.xz <br>  
MD5 sum: f58fa8001d60c4c77595fbbb62b63c1d <br>  

Gperf (3.1) - 1,188 KB: <br>  
Home page: http://www.gnu.org/software/gperf/ <br>  
Download: http://ftp.gnu.org/gnu/gperf/gperf-3.1.tar.gz <br>  
MD5 sum: 9e251c0a618ad0824b51117d5d9db87e <br>  

Grep (3.1) - 1,339 KB: <br>  
Home page: http://www.gnu.org/software/grep/ <br>  
Download: http://ftp.gnu.org/gnu/grep/grep-3.1.tar.xz <br>  
MD5 sum: feca7b3e7c7f4aab2b42ecbfc513b070 <br>  

Groff (1.22.3) - 4,091 KB: <br>  
Home page: http://www.gnu.org/software/groff/ <br>  
Download: http://ftp.gnu.org/gnu/groff/groff-1.22.3.tar.gz <br>  
MD5 sum: cc825fa64bc7306a885f2fb2268d3ec5 <br>  

GRUB (2.02) - 5,970 KB: <br>  
Home page: http://www.gnu.org/software/grub/ <br>  
Download: https://ftp.gnu.org/gnu/grub/grub-2.02.tar.xz <br>  
MD5 sum: 8a4a2a95aac551fb0fba860ceabfa1d3 <br>  

Gzip (1.9) - 748 KB: <br>  
Home page: http://www.gnu.org/software/gzip/ <br>  
Download: http://ftp.gnu.org/gnu/gzip/gzip-1.9.tar.xz <br>  
MD5 sum: 9492c6ccb2239ff679a5475a7bb543ed <br>  

Iana-Etc (2.30) - 201 KB: <br>  
Home page: http://freecode.com/projects/iana-etc <br>  
Download: http://anduin.linuxfromscratch.org/LFS/iana-etc-2.30.tar.bz2 <br>  
MD5 sum: 3ba3afb1d1b261383d247f46cb135ee8 <br>  

Inetutils (1.9.4) - 1,333 KB: <br>  
Home page: http://www.gnu.org/software/inetutils/ <br>  
Download: http://ftp.gnu.org/gnu/inetutils/inetutils-1.9.4.tar.xz <br>  
MD5 sum: 87fef1fa3f603aef11c41dcc097af75e <br>  

Intltool (0.51.0) - 159 KB: <br>  
Home page: https://freedesktop.org/wiki/Software/intltool <br>  
Download: https://launchpad.net/intltool/trunk/0.51.0/+download/intltool-0.51.0.tar.gz <br>  
MD5 sum: 12e517cac2b57a0121cda351570f1e63 <br>  

IPRoute2 (4.18.0) - 677 KB: <br>  
Home page: https://www.kernel.org/pub/linux/utils/net/iproute2/ <br>  
Download: https://www.kernel.org/pub/linux/utils/net/iproute2/iproute2-4.18.0.tar.xz <br>  
MD5 sum: 8b8680e91390c57cab788fbf8e929479 <br>  

Kbd (2.0.4) - 1,008 KB: <br>  
Home page: http://ftp.altlinux.org/pub/people/legion/kbd <br>  
Download: https://www.kernel.org/pub/linux/utils/kbd/kbd-2.0.4.tar.xz <br>  
MD5 sum: c1635a5a83b63aca7f97a3eab39ebaa6 <br>  

Kmod (25) - 540 KB: <br>  
Download: https://www.kernel.org/pub/linux/utils/kernel/kmod/kmod-25.tar.xz <br>  
MD5 sum: 34f325cab568f842fdde4f8b2182f220 <br>  

Less (530) - 332 KB: <br>  
Home page: http://www.greenwoodsoftware.com/less/ <br>  
Download: http://www.greenwoodsoftware.com/less/less-530.tar.gz <br>  
MD5 sum: 6a39bccf420c946b0fd7ffc64961315b <br>  

LFS-Bootscripts (20180820) - 32 KB: <br>  
Download: http://www.linuxfromscratch.org/lfs/downloads/8.3/lfs-bootscripts-20180820.tar.bz2 <br>  
MD5 sum: 57c9501afe65b95bc4428f344ab2c85e <br>  

Libcap (2.25) - 64 KB: <br>  
Home page: https://sites.google.com/site/fullycapable/ <br>  
Download: https://www.kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-2.25.tar.xz <br>  
MD5 sum: 6666b839e5d46c2ad33fc8aa2ceb5f77 <br>  

Libffi (3.2.1) - 920 KB: <br>  
Home page: https://sourceware.org/libffi/ <br>  
Download: ftp://sourceware.org/pub/libffi/libffi-3.2.1.tar.gz <br>  
MD5 sum: 83b89587607e3eb65c70d361f13bab43 <br>  

Libpipeline (1.5.0) - 810 KB: <br>  
Home page: http://libpipeline.nongnu.org/ <br>  
Download: http://download.savannah.gnu.org/releases/libpipeline/libpipeline-1.5.0.tar.gz <br>  
MD5 sum: b7437a5020190cfa84f09c412db38902 <br>  

Libtool (2.4.6) - 951 KB: <br>  
Home page: http://www.gnu.org/software/libtool/ <br>  
Download: http://ftp.gnu.org/gnu/libtool/libtool-2.4.6.tar.xz <br>  
MD5 sum: 1bfb9b923f2c1339b4d2ce1807064aa5 <br>  

Linux (4.18.5) - 99,411 KB: <br>  
Home page: https://www.kernel.org/ <br>  
Download: https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.18.5.tar.xz <br>  
MD5 sum: 22851fe6c82db6673a844bbb7c62df67 <br>  

Nota:
El kernel del linux se actualiza relativamente a menudo, muchas veces debido a que se encuentran fallos o vulnerabilidades. La última version del kernel es la 4.18.x y deberia ser la usada, a menos que la página de erratas publique lo contrario.
Para usuarios con velocidad limitada o una ancho de banda limitado, que deseen actualizar el kernel de Linux, exite una linea base de los paquetes y parches, que pueden ser descargados independientemente. Esto puede ahorrar algun tiempo o coste con respecto a la actualizacion de una relase menor.

M4 (1.4.18) - 1,180 KB: <br>  
Home page: http://www.gnu.org/software/m4/ <br>  
Download: http://ftp.gnu.org/gnu/m4/m4-1.4.18.tar.xz <br>  
MD5 sum: 730bb15d96fffe47e148d1e09235af82 <br>  

Make (4.2.1) - 1,375 KB: <br>  
Home page: http://www.gnu.org/software/make/ <br>   
Download: http://ftp.gnu.org/gnu/make/make-4.2.1.tar.bz2 <br>  
MD5 sum: 15b012617e7c44c0ed482721629577ac <br>  

Man-DB (2.8.4) - 1,738 KB: <br>  
Home page: https://www.nongnu.org/man-db/ <br>  
Download: http://download.savannah.gnu.org/releases/man-db/man-db-2.8.4.tar.xz <br>  
MD5 sum: ab41db551f500e4a595b11203b86c67a <br>  

Man-pages (4.16) - 1,592 KB: <br>  
Home page: https://www.kernel.org/doc/man-pages/ <br>  
Download: https://www.kernel.org/pub/linux/docs/man-pages/man-pages-4.16.tar.xz <br>  
MD5 sum: ad9f1ff81276fe8d90d077484d6d4b5e <br>  

Meson (0.47.1) - 1,221 KB: <br>  
Home page: https://mesonbuild.com <br>  
Download: https://github.com/mesonbuild/meson/releases/download/0.47.1/meson-0.47.1.tar.gz <br>  
MD5 sum: 5ed95fd4e9c7634f7cf3482d352804e7 <br>  

MPC (1.1.0) - 685 KB: <br>  
Home page: http://www.multiprecision.org/ <br>  
Download: https://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz <br>  
MD5 sum: 4125404e41e482ec68282a2e687f6c73 <br>  

MPFR (4.0.1) - 1,380 KB: <br>  
Home page: https://www.mpfr.org/ <br>  
Download: http://www.mpfr.org/mpfr-4.0.1/mpfr-4.0.1.tar.xz <br>  
MD5 sum: b8dd19bd9bb1ec8831a6a582a7308073 <br>  

Ninja (1.8.2) - 181 KB: <br>  
Home page: https://ninja-build.org/ <br>  
Download: https://github.com/ninja-build/ninja/archive/v1.8.2/ninja-1.8.2.tar.gz <br>  
MD5 sum: 5fdb04461cc7f5d02536b3bfc0300166 <br>  

Ncurses (6.1) - 3,288 KB: <br>  
Home page: http://www.gnu.org/software/ncurses/ <br>  
Download: http://ftp.gnu.org/gnu/ncurses/ncurses-6.1.tar.gz <br>  
MD5 sum: 98c889aaf8d23910d2b92d65be2e737a <br>  

OpenSSL (1.1.0i) - 5,326 KB: <br>  
Home page: https://www.openssl.org/ <br>  
Download: https://openssl.org/source/openssl-1.1.0i.tar.gz <br>  
MD5 sum: 9495126aafd2659d357ea66a969c3fe1 <br>  

Patch (2.7.6) - 766 KB: <br>  
Home page: https://savannah.gnu.org/projects/patch/ <br>  
Download: http://ftp.gnu.org/gnu/patch/patch-2.7.6.tar.xz <br>  
MD5 sum: 78ad9937e4caadcba1526ef1853730d5 <br>  

Perl (5.28.0) - 12,120 KB: <br>  
Home page: https://www.perl.org/ <br>  
Download: https://www.cpan.org/src/5.0/perl-5.28.0.tar.xz <br>  
MD5 sum: f3245183c0a08f65e94a3333995af08e <br>  

Pkg-config (0.29.2) - 1,970 KB: <br>  
Home page: https://www.freedesktop.org/wiki/Software/pkg-config <br>  
Download: https://pkg-config.freedesktop.org/releases/pkg-config-0.29.2.tar.gz <br>  
MD5 sum: f6e931e319531b736fadc017f470e68a <br>  

Procps (3.3.15) - 884 KB: <br>  
Home page: https://sourceforge.net/projects/procps-ng <br>  
Download: https://sourceforge.net/projects/procps-ng/files/Production/procps-ng-3.3.15.tar.xz <br>  
MD5 sum: 2b0717a7cb474b3d6dfdeedfbad2eccc <br>  

Psmisc (23.1) - 290 KB: <br>  
Home page: http://psmisc.sourceforge.net/ <br>  
Download: https://sourceforge.net/projects/psmisc/files/psmisc/psmisc-23.1.tar.xz <br>  
MD5 sum: bbba1f701c02fb50d59540d1ff90d8d1 <br>  

Python (3.7.0) - 16,526 KB: <br>  
Home page: https://www.python.org <br>  
Download: https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tar.xz <br>  
MD5 sum: eb8c2a6b1447d50813c02714af4681f3 <br>  

Python Documentation (3.7.0) - 6,077 KB: <br>  
Download: https://docs.python.org/ftp/python/doc/3.7.0/python-3.7.0-docs-html.tar.bz2 <br>  
MD5 sum: c4637cf2a9a6ebb8c5e9dc8ff13fb4cb <br>  

Readline (7.0) - 2,842 KB: <br>  
Home page: https://tiswww.case.edu/php/chet/readline/rltop.html <br>  
Download: http://ftp.gnu.org/gnu/readline/readline-7.0.tar.gz <br>  
MD5 sum: 205b03a87fc83dab653b628c59b9fc91 <br>  

Sed (4.5) - 1,245 KB: <br>  
Home page: http://www.gnu.org/software/sed/ <br>  
Download: http://ftp.gnu.org/gnu/sed/sed-4.5.tar.xz <br>  
MD5 sum: ade8f8c2c548bf41f74db2dcfc37e4e3 <br>  

Shadow (4.6) - 1,639 KB: <br>  
Download: https://github.com/shadow-maint/shadow/releases/download/4.6/shadow-4.6.tar.xz <br>  
MD5 sum: b491fecbf1232632c32ff8f1437fd60e <br>  

Sysklogd (1.5.1) - 88 KB: <br>  
Home page: http://www.infodrom.org/projects/sysklogd/ <br>  
Download: http://www.infodrom.org/projects/sysklogd/download/sysklogd-1.5.1.tar.gz <br>  
MD5 sum: c70599ab0d037fde724f7210c2c8d7f8 <br>  

Sysvinit (2.90) - 111 KB: <br>  
Home page: https://savannah.nongnu.org/projects/sysvinit <br>  
Download: http://download.savannah.gnu.org/releases/sysvinit/sysvinit-2.90.tar.xz <br>  
MD5 sum: 7b6a16bde3da494b6aac7283b79c81de <br>  

Tar (1.30) - 2,059 KB: <br>  
Home page: http://www.gnu.org/software/tar/ <br>  
Download: http://ftp.gnu.org/gnu/tar/tar-1.30.tar.xz <br>  
MD5 sum: 2d01c6cd1387be98f57a0ec4e6e35826 <br>  

Tcl (8.6.8) - 9,665 KB: <br>  
Home page: http://tcl.sourceforge.net/ <br>  
Download: https://downloads.sourceforge.net/tcl/tcl8.6.8-src.tar.gz <br>  
MD5 sum: 81656d3367af032e0ae6157eff134f89 <br>  

Texinfo (6.5) - 4,399 KB: <br>  
Home page: http://www.gnu.org/software/texinfo/ <br>  
Download: http://ftp.gnu.org/gnu/texinfo/texinfo-6.5.tar.xz <br>  
MD5 sum: 3715197e62e0e07f85860b3d7aab55ed <br>  

Time Zone Data (2018e) - 346 KB: <br>  
Home page: https://www.iana.org/time-zones <br>  
Download: https://www.iana.org/time-zones/repository/releases/tzdata2018e.tar.gz <br>  
MD5 sum: 97d654f4d7253173b3eeb76a836dd65e <br>  

Udev-lfs Tarball (udev-lfs-20171102) - 11 KB: <br>  
Download: http://anduin.linuxfromscratch.org/LFS/udev-lfs-20171102.tar.bz2 <br>  
MD5 sum: d92afb0c6e8e616792068ee4737b0d24 <br>  

Util-linux (2.32.1) - 4,455 KB: <br>  
Home page: http://freecode.com/projects/util-linux <br>  
Download: https://www.kernel.org/pub/linux/utils/util-linux/v2.32/util-linux-2.32.1.tar.xz <br>  
MD5 sum: 9e5b1b8c1dc99455bdb6b462cf9436d9 <br>  

Vim (8.1) - 10,995 KB: <br>  
Home page: https://www.vim.org <br>  
Download: ftp://ftp.vim.org/pub/vim/unix/vim-8.1.tar.bz2 <br>  
MD5 sum: 1739a1df312305155285f0cfa6118294 <br>  

XML::Parser (2.44) - 232 KB: <br>  
Home page: https://github.com/chorny/XML-Parser <br>  
Download: https://cpan.metacpan.org/authors/id/T/TO/TODDR/XML-Parser-2.44.tar.gz <br>  
MD5 sum: af4813fe3952362451201ced6fbce379 <br>  

Xz Utils (5.2.4) - 1030 KB: <br>  
Home page: https://tukaani.org/xz <br>  
Download: https://tukaani.org/xz/xz-5.2.4.tar.xz <br>  
MD5 sum: 003e4d0b1b1899fc6e3000b24feddf7c <br>  

Zlib (1.2.11) - 457 KB: <br>  
Home page: https://www.zlib.net/ <br>  
Download: https://zlib.net/zlib-1.2.11.tar.xz <br>  
MD5 sum: 85adef240c5f370b308da8c938951a68 <br>  
Total size of these packages: about 367 MB <br>  

## 3.3 PARCHES NECESARIOS

Ademas de los paquetes, tambien son necesarios distintos parches. Estos parches corrigen algunos defectos en el software, y que deben ser corregidos. Los parches tambien llevan a cabo pequeñas modificaciones que hacen que sea más sencillo trabajar con los paquetes. Los siguientes parches, seran necesarios para construir un sistema LFS:

Bzip2 Documentation Patch - 1.6 KB:
Download: http://www.linuxfromscratch.org/patches/lfs/8.3/bzip2-1.0.6-install_docs-1.patch

MD5 sum: 6a5ac7e89b791aae556de0f745916f7f

Coreutils Internationalization Fixes Patch - 168 KB:
Download: http://www.linuxfromscratch.org/patches/lfs/8.3/coreutils-8.30-i18n-1.patch

MD5 sum: a9404fb575dfd5514f3c8f4120f9ca7d

Glibc FHS Patch - 2.8 KB:
Download: http://www.linuxfromscratch.org/patches/lfs/8.3/glibc-2.28-fhs-1.patch

MD5 sum: 9a5997c3452909b1769918c759eff8a2

Kbd Backspace/Delete Fix Patch - 12 KB:
Download: http://www.linuxfromscratch.org/patches/lfs/8.3/kbd-2.0.4-backspace-1.patch

MD5 sum: f75cca16a38da6caa7d52151f7136895

Ninja Limit Jobs Patch - 2.5 KB:
Download: http://www.linuxfromscratch.org/patches/lfs/8.3/ninja-1.8.2-add_NINJAJOBS_var-1.patch

MD5 sum: f537a633532492e805aa342fa869ca45

Sysvinit Consolidated Patch - 2.5 KB:
Download: http://www.linuxfromscratch.org/patches/lfs/8.3/sysvinit-2.90-consolidated-1.patch

MD5 sum: b7872b43a97a81bac4f9fc8ffaef4364

El tamaño total aproximado de estos paquetes es de 189.4 KB.

Adicionalmente a los parches anteriores, existe otros parches adicionales, creados por la comunidad LFS. Estos parches opcionales, resuelven pequeños problemas, o permiten funcinalidades, que no estan activadas por defecto. Por favor sientase libre, para usar dichos parches, y utilizar otros parches que necesite.
http://www.linuxfromscratch.org/patches/downloads/ 

# 4. PREPARATIVOS FINALES

## 4.1 PRESENTACIÓN
En este capítulo, se realizarán algunas tareas adicionales para prepararse a montar un sistema de archivos temporales. Crearemos un directorio en $LFS para la instalación de las herramientas temporales, añadiremos un usuario sin privilegios, para reducir los riesgos, y crearemos un entorno apropiado para que ese usuario pueda desarrollar. También se explicará la unidad de tiempo que se usará para medir cuánto tiempo se necesitará para construir los paquetes necesarios, o "SBUs", y se dará alguna información sobre los  de paquetes de test.

## 4.2 CREANDO EL DIRECTORIO $LFS/tools

Todos los programas que se compilen en el capítulo 5, se instalarán en $LFS/tools, para mantenerlos separados de los programas que se instalarán en el capítulo 6. Todos los programas compilados aquí son herramientas temporales, que no formarán parte del sistema LFS. Al mantener estos programas en un directorio separado, se pueden descartar fácilmente después de su uso. Esto también evita que estos programas acaben en algún directorio del sistema anfitrión (que es algo que puede ocurrir fácilmente al realizar el capítulo 5).

Para crear este directorio, con el usuario ‘root’, se ejecutará:

``` bash
mkdir -v $LFS/tools
```

El siguiente paso es crear un symlink a ‘/tools’ en el sistema anfitrion. Esto apuntará al directorio en la partición LFS. Habrá que ejecutar este comando también como el usuario raíz ‘root’:

```bash
ln -sv $LFS/tools /
```

NOTA: El comando anterior es correcto. El comando ‘ln’ tiene algunas variaciones sintácticas, pero para estar seguros, es mejor verificar ‘info coreutils ln’ y ‘ln(1)’, antes de reportar lo que podría no ser un error.

El symlink creado, permite a la cadena de herramientas ser compilada, así que siempre se puede referir a esta con ‘/tools’, implicando que el compilador, el ensamblador y el linker, funcionarán en el capítulo 5 (cuando todavía sea necesario usar algunos programas del sistema anfitrión)  y en el siguiente  (cuando se haga ‘chroot’ a la partición LFS).

## 4.3 Añadiendo el usuario LFS.

Cuando uno es el usuario ‘root’, si se comete cualquier error, el daño puede inhabilitar el sistema. Por tanto, se recomienda construir todo en este capítulo, como un usuario sin privilegios especiales. Se puede usar su propio nombre, pero para que sea más sencillo establecer un entorno de trabaj de cero, se crea un nuevo usuario, denominado ‘lfs’ como miembro de un nuevo grupo, también llamado ‘lfs’, y se usa este durante el proceso de instalacion. Como usuario ‘root’, se han de ejecutar los siguientes comandos, para añaidr un nuevo usuario.

```bash
groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs
```

Explicación de los comandos:


  -s /bin/bash

Hace que la consola bash sea la utilizada por defecto para ese usuario.

  -g lfs

Añade al usuario al grupo ‘lfs’

-m
Crea un directorio ‘home’ para el usuario ‘lfs’ 
 
-k /dev/null
Evita que se copie la estructura de los archivos de un directorio esqueleto (por defecto es /etc/skel) y cambia la localización de la entrada al dispositivo de datos nulos (null device) 
lfs
El nombre del usuario y del grupo.
 
Para identificarse como el usuario ‘lfs’ (que es distinto a cambiar al usuario ‘lfs’ cuando se está identificado como el usuario raíz ‘root’, y que no requiere que el usuario ‘lfs’ tenga un password), se define una contraseña para el usuario ‘lfs’
```bash
passwd lfs
```
 
Damos al usuario `lfs’ acceso a al directorio ‘$LFS/tools’, haciendole el propietario del directorio.
```bash
chown -v lfs $LFS/tools
```
A continuación nos identificamos como el usuario ‘lfs’, por medio de la consola:
 
```bash
su - lfs
```

El guión ‘-’ indica iniciar una nueva consola con identificación, en oposición a una consola sin identificación. La explicación detallada entre ambos tipos de consola, puede encontrarse en las páginas del manual de ‘bash’ y ‘bash(1)’.
 
## 4.4 Montando el entorno de desarrollo

Para definir un correcto entorno de desarrollo, se han de crear dos archivos nuevos de arranque para la consola ‘bash’. Mientras estamos identificados como usuario ‘lfs’ ejecutaremos el siguiente comando para crear un nuevo archivo ‘.bash_profile’:
 
```bash
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF
```

Cuando estamos identificados como usario ‘lfs’, la consola inicial es normalmente una consola de login, que lee del archivo /etc/profile del sistema anfitrión (probablemente conteniendo algunos definiciones y variables de entorno) y luego del archivo `.bash_profile’. El comando ‘exec env -i .../bin/bash’ reemplaza la consola en ejecución con una nueva, en un entorno completamente vacío, a excepción de las variables: HOME, TERM y PS1. Esto nos asegura que no hay otras definiciones en el entorno potencialmente peligrosas, y que puedan pasar del sistema anfitrión al sistema que queremos construir LFS. Esta técnica se usa para asegurar un entorno de trabajo estable y limpio.

La nueva instancia de la consola es una consola sin identificación (non-login shell), la cual no lee los archivos /etc/profile o .bash_profile, pero que en cambio lee de los archivos ‘.bashrc’. Para crear el archivo ‘.bashrc’ se ejecuta:

```bash
cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH
EOF
```

El comando `set +h’ inhabilita las funciones hash de la consola bash. Los hashes son una función habitualmente utilizada en bash, los usa para apuntar las direcciones completas de los archivos ejecutables que usa habitualmente, para de esa forma evitar buscarlos en la variable de entorno PATH cada vez que se necesitan. Pero, en este caso, las herramientas serán nuevas, y van a ser usadas, tan pronto como sean instaladas. Inhabilitando la función hash, la consola buscará siempre en la variable de entorno PATH, cuando necesite ejecutar un programa. Tan pronto como la consola encuentre las nuevas herramientas compiladas en $LFS/tools, y tan pronto como estas estén disponibles, sin necesidad de buscar versiones anteriormente utilizadas de ese programa en otras direcciones.

Al definir la máscara de creación de archivos (umask) a 022, se asegura que los nuevos archivos creados, y directorios únicamente serán editables por su propietarios, pero que pueden ser leídos y ejecutados por cualquiera (suponiendo los modos por defecto son usados en las llamadas del sistema open(2), y los nuevos archivos tiene el modo de permisos 644 y directorios el modo 755) 

La variable LFS debería estar definida para indicar el punto en el que se va a montar el sistema.

La variable LC_ALL gestiona localización de ciertos programas, haciendo que sus mensajes sigan las convenciones de los países especificados. Definiendo LC_ALL como “POSIX” o “C” (ambos son equivalentes) asegura que todo resultara como se espera en el entorno “chroot”.

La variable LFS_TGT define un descripción de máquina compatible para usarse cuando se construya el compilador cruzado y linker, y cuando se compile la cadena de desarrollo temporal. Más información es especificada en la sección 5.2 “Notas técnicas sobre la cadena de desarrollo”.

Poniendo la dirección “/tools/bin” a la cabeza de PATH, todos los programas que se instalarán en el capítulo 5, se cogerán inmediatamente por la consola después de su instalación. Esto, en combinación con la deshabilitación del hash. Limita el riesgo de que antiguas versiones de los programas sean usados desde el sistema anfitrión, cuando los mismos programas que necesitaremos están disponibles en el sistema anfitrión

Finalmente, disponemos del entorno de trabajo totalmente preparado para construir las herramientas temporales, el perfil del usuario creado es:


```bash
source ~/.bash_profile
```


## 4.5 Sobre los tiempos de construcción.

Resulta interesante conocer de antemano, aproximadamente cuánto durará la compilación e instalación de cada paquete. Ya que LFS, puede ser construido en distintos sistemas, es imposible tener una previsión precisa en todos los casos. El paquete más grande (Glib) dura aproximadamente 20 minutos en el sistema más veloz, pero puede llevar hasta 3 días en otros sistemas más lentos. En vez de proporcionar tiempos, se utilizará la Unidad Estándar de tiempo (en inglés “SBU: Standard Build Unit”).

La medida SBU funciona tal y como se explica a continuación. El primer paquete de software que se compilará según este texto, será el paquete “Binutils” en el capítulo 5. El tiempo requerido para compilar este paquete se asumirá como la unidad te tiempo de construcción SBU. El resto de paquetes se expresaran de forma relativa a este tiempo.

Por ejemplo, se considerará un paquete cuyo tiempo de compilación es 4.5 SBUs. Esto quiere decir que si al sistema que se está utilizando, necesito 10 minutos para compilar e instalar Binutils, entonces aproximadamente necesitará 45 minutos para compilar el ejemplo de 4.5 SBUs. Afortunadamente, la mayoría de tiempos son menores que el que se necesita para Binutils.

De forma general, los SBUs no son totalmente precisos, porque dependen de muchos factores, incluyendo la versión que utiliza el sistema anfitrión de GCC. Pero estos proporcionan una estimación de cuánto puede llevar la instalación de un paquete, aunque a veces puede haber variaciones de docenas de minutos, en algunos casos.

NOTA:
En muchos sistemas modernos con múltiples procesadores o núcleos, la compilación paralela de un paquete puede reducirse ejecutando un “make paralelo”, tanto definiendo una variable de entorno o indicando al programa make, cuantos procesadores están disponibles. Por ejemplo un Core2Duo puede ejecutar dos procesos simultáneos, con:


```bash
export MAKEFLAGS='-j 2'
```

O simplemente, indicandosel a make, con:

```bash
make -j2
```

Cuando se utilizan múltiples procesadores, de esta forma, las unidades SBUs de este texto, variarán su precisión incluso más. En algunos casos, la invocación a ‘make’ incluso fallará. Analizando la salida de proceso de construcción, será incluso más difícil ya que las líneas de los distintos procesos estarán entremezcladas. Si se encuentra este problema, lo mejor es reiniciar la construcción paso a paso, volver a usar un único procesador, y analizar cada mensaje de error. 


## 4.6 Información sobre los tests.

La mayoría de los paquetes de software tienen un grupo de tests. Se recomienda ejecutar estos grupos de test. Al hacer una construcción nueva es una muy buena idea, porque puede proporcionar una verificación extra, que indica que todo se ha construido correctamente. Un test que pasa todas sus verificaciones normalmente asegura que el paquete está funcionando correctamente, y como se espera por parte de sus desarrolladores. Aunque que pasen los test no prueba que los paquetes estén totalmente libre de errores.

Algunos grupos de test son más importantes que otros. Por ejemplo, los test que para el núcleo de la cadena de trabajo: GCC, Binutils y Glibc- son de la mayor importancia dado su papel central en el correcto funcionamiento del sistema.

Los grupos de test para GCC y Glibc pueden llevar un tiempo significativo para completarse, especialmente el hardware poco rápido, pero se recomiendan altamente su ejecución.

NOTA:
La experiencia muestra que no se gana mucho al ejecutar los test del capítulo 5. No hay forma de evitar que el sistema anfitrión tenga cierta influencia en los test ejecutados, y a veces causando fallos inexplicables. Ya que las herramientas que se construyen en el capítulo 5 son temporales y se descartaran eventualmente, no se recomienda ejecutar los test del capítulo 5, al menos para el lector de nivel medio. Las instrucciones para ejecutar estos test se proporcionan para el beneficio de testers y desarrolladores, pero son opcionales.

Un sucedido habitual cuando se ejecutan los test para Binutils y GCC es  la ejecución de pseudo terminales (PTYs). Esto puede resultar en un elevado número de fallos en los test. Las razones para esto pueden ser varias, pero la más probable es que el sistema anfitrión no tenga definido correctamente el sistema de archivos ‘devpts’. Este evento se comenta en gran detalle en la dirección: http://www.linuxfromscratch.org/lfs/faq.html#no-ptys.

En algunas ocasiones los test fallaran, pero debido a razones conocidas por los desarrolladores, y que no son consideradas críticas. Para consultar los registros, están en la dirección: http://www.linuxfromscratch.org/lfs/build-logs/8.3/ . Esta dirección es válida para todos los test de este texto.

# 5 Construyendo un sistema temporal.

## 5.1 Introducción

En este capítulo se muestra como construir un sistema Linux mínimo. Este sistema poseerá tan solo las herramientas necesarias para montar el sistema LFS final, en el capítulo 6, y permitirá un entorno de trabajo, más adecuado para el usuario, que le permitiría un entorno mínimo.

Hay dos pasos en la construcción de este sistema mínimo. El primero es construir una cadena de herramientas nueva e independiente del sistema anfitrión (compilador, ensamblador, linker, librerías, y algunas utilidades). Y el segundo paso es utiliziar esta nueva cadena de herramientas para construir un nuevo sistema.

Las herramientas compiladas en este capítulo serán instaladas en directorio $LFS/tools para mantenerlas separadas de los archivos que se instalarán en el siguiente capítulo y los directorios de producción del sistema anfitrión. Ya que los distintos paquetes que se compilarán aquí serán temporales, no queremos que influyan en el que será el sistema LFS.

## 5.2 Notas técnicas de la cadena de herramientas de desarrollo

En esta sección se explica algunas de las razones y los detalles técnicos que son la base del método de construcción. No es necesario comprender inmediatamente todo el contenido de esta sección. La mayor parte de la información será una aclaración posterior a cada acción. Se puede volver a esta sección en cualquier momento durante el proceso.

El objetivo final del capítulo 5 es producir un área temporal que contenga un 
Entorno conocido de herramientas que puedan ser aisladas del sistema anfitrión. Usando el comando ‘chroot’, los comandos en los siguientes capítulos serán contenidos en ese entorno, asegurando un entorno limpio, libre de errores donde construir el sistema LFS objetivo. El proceso de construcción ha sido pensado para minimizar los riesgos para nuevos lectores y proveer al mismo tiempo del máximo valor educacional.

NOTA:
Antes de continuar, hay que tener claro el nombre de la plataforma de trabajo. Un simple sistema de determinar el nombre es ejecutar el script ‘config.guess’ que viene con el código fuente de muchos paquetes. Hay que descomprimir el código fuente de Binutils y ejecutar el script: ‘./config.guess’ y anotar la salida del programa. Por ejemplo, para un procesador Intel 32-bits, la salida del script será: ‘i686-pc-linux-gnu’. En un sistema de 64-bits, la salida del script será: ‘x86_64-pc-linux-gnu’.

También hay que conocer el nombre del linker dinámico de la plataforma, también conocido como ‘dynamic loader’ (que no hay que confundir con el linker standard, ‘ld’ que es parte de Binutils). El linker dinámico de Glibc encuentra y carga las librerías compartidas que necesita un programa y prepara el programa para ejecutar, y luego lo ejecuta. El nombre del liker dinámico para un sistema basado en un procesador Intel de 32-bits será ‘ld-linux.so.2’ (ld-linux-x86-64.so.2 para sistemas de 64-bits). Un sistema seguro para determinar su nombre es inspeccionar un binario al azar del sistema anfitrión y ejecutar: ‘readelf -l <nombre del binario> | grep interpreter’ y anotar la salida. La referencia que informa todas las plataformas está en el archivo shlib-versions, en el código fuente del root de Glibc.

Algunas anotaciones técnicas relevantes sobre como es el método de construcción del capítulo 5:

 - Pequeñas variaciones del nombre de la plataforma de trabajo, en el que se cambia el nombre del “vendedor” por medio de la variable LFS_TGT, garantiza que en la primera construcción de Binutils y GCC genera un compilador y linkador cruzado compatible. En vez de producir binarios para otra arquitectura, el compilador y linkador cruzado generará binarios compatibles con el hardware que se está usando.  

 - Las librerías temporales se compilan con un compilador cruzado. Dado que un compilador cruzado, por su definición no se basa en nada de su sistema anfitrión, este método elimina potenciales fuentes de “contaminación” entre el sistema objetivo y el sistema anfitrión, reduciendo la probabilidad de cabeceras o librerías del sistema anfitrión sean incorporadas en las nuevas herramientas generadas. La compilación cruzada también permite la posibilidad de construir versiones de 32-bits y 64-bits.

 - Las opciones correctas de compilación del código de GCC le indica al compilador cuál será el linker dinámico que será usado.
 

Binutils se instala primero porque el script ‘configure’ se ejecuta en ambos GCC y Glibc, y realiza varios test en el ensamblador y el linker para determinar qué características del software se activan o desactivan. Esto es más importante de lo que puede parecer inicialmente. Una configuración incorrecta de GCC o Glib, puede resultar en una cadena de herramientas de trabajo, incorrecta, y el impacto de esto puede no ser evidente hasta el final del proceso de construcción y de distribución entera. El fallo de un test normalmente detectará el error antes de que se avance en el trabajo.

Binutils instalá su ensamblador y linker en dos direcciones: ‘/tools/bin’ y ‘/tools/$LFS_TGT/bin’. Las herramientas en una dirección son linkadas a la otra. Un aspecto importante del linker es su orden de búsqueda de librerías. Información detallada puede ser obtenida del comando ‘ld’ pasando la indicación `--verbose’. Por ejemplo, un ‘ld --verbose | grep SEARCH’ mostrará los directorios de la búsqueda actual y su orden. Esto muestra qué archivos se linkan, con el comando ‘ld’, por ejemplo compilando un programa de ejemplo y pasando la indicación ‘--verbose’ a el liner. Por ejemplo: ‘gcc dummy.c -WI, --verbose 2>&1 | grep succeeded’ mostrará todos los archivos que se han abierto durante el linkado.

El siguiente programa es GCC. Un ejemplo de la salida cuando se ejecuta el script ‘configure’:

```bash
checking what assembler to use... /tools/i686-lfs-linux-gnu/bin/as
checking what linker to use... /tools/i686-lfs-linux-gnu/bin/ld
```

Esto es importante, por las razones mencionadas anteriormente. También demuestra que el script de configuración de GCC no busca en los directorios del PATH por las herramientas que usará. De todas formas, durante lo operación del propio gcc, el mismo revisa directorios que no tienen por qué ser necesariamente usados. Para mirar cuales usará el linker standard de gcc se puede con el comando ‘gcc -print-prog-name=ld’

Información detallada puede obtenerse de gcc, definiendo el indicador -v como opción mientras se compila un programa de ejemplo, como: ‘gcc -v dummy.c’. Esto mostrará información detallada sobre el preprocesador, compilación, ensamblado y linkado, además de los directorios de búsqueda del gcc y su orden.

A continuación se instalarán las cabeceras de Linux API. Esto permitirá a la librería C estándar (Glibc) hacer de interface con las funcionalidades que provee el núcleo de Linux (LInux kernel)

El siguiente paquete que se instalará será Glibc. La consideración más importante para construir Glibc es el compilador, las herramientas binarias, y las cabeceras del núcleo de Linux. El compilador normalmente no es un problema ya que Glibc siempre usa el compilador en relación al parámetro ‘--host’ que se especifica en su script de configuración; por ejemplo en nuestro caso, el compilador será ‘i686-lfs-linux-gnu-gcc’. Las herramientas binarias y las cabeceras del núcleo pueden ser más complicadas. Por tanto, no tomaremos riesgos y usaremos las definiciones de configuración disponibles para definir las opciones y selecciones correctas. Después de ejecutar el script ‘configure’, verificaremos que los contenidos del archivo ‘config.make’ en glibc-buildirecority para todos los detalles relevantes. Hacer notar que el uso de CC=”i686-lfs-gnu-gcc” para controlar wue herramientas binarias serán usadas y el uso de  -nostdinc y -isystemflags para controlar las direcciones de los archivos para incluir del compilador. Estos puntos han de subrayarse como aspectos importantes del paquete de Glibc - es muy independiente en términos de su construcción y normalmente no depende de los valores por defecto de las cadenas de herramientas.

Durante la segunda compilación de Binutils, se podrá utilizar la opción ‘--with-lib-path’, para definir las direcciones en que buscará ‘ld’.

Para la segunda compilación de GCC, las fuentes deben de ser modificadas para indicar a GCC que use el nuevo linker dinámico. Si esto no se hacer, el resultado será que los programas compilados con ese GCC, tendrán el linker dinámico del sistema anfitrión, del directorio /lib, integrado en ellos, lo que invalidará el objetivo de ser independiente del sistema anfitrión. De este punto en adelante, el núcleo de la cadena de herramientas es auto-contenido. En el resto de capítulo, el resto de paquetes serán construidos con el nuevo Glibc en ‘/tools’.

Una vez entremos en el entorno de ‘chroot’ en el capítulo 6, el primer paquete que se instalará será Glibc, dada su naturaleza auto-contenida mencionada anteriormente. Una vez Glibc esté instalado en ‘/usr’, se desarrollará un un cambio en las opciones por defecto de la cadena de herramientas, y a continuación se procederá a construir el resto del sistema objetivo LFS.

## 5.3 Instrucciones generales de compilación

Al construir los paquetes se han hecho distintas asunciones en las instrucciones.

 - Varios de los paquetes son parcheados antes de compilarlos, pero tan solo cuando el paquete lo necesita para corregir un problema. Un parche es a menudo necesario tanto en este capítulo como en el siguiente, pero hay alguno que solo se necesita en uno de los pasos. De ahí, que no hay que preocuparse si las instrucciones para descargar un parche parecen faltar. Mensajes de aviso sobre ‘offset’ o ‘fuzz’ pueden también aparecer al aplicarse un parche. No se preocupe sobre estos avisos, cuando el parche ha sido aplicado correctamente.

 - Durante la compilación de la mayoría de los paquetes, habrá varios avisos que serán reportados. Estos son normales y pueden ignorarse. Estos avisos, son únicamente notas de atención sobre partes obsoletas, pero no invalidas, o sintaxis de C o C++. Los standards de C cambian a menudo y algunos paquetes todavía usan un estándar antiguo. Esto no es un problema, pero se notifica con avisos.

 - Verificar una última vez que la variable de entorno LFS está definida adecuadamente:

```bash
echo $LFS
```

Hay que asegurarse que la salida muestra la dirección del punto donde se ha montado la partición que es ‘/mnt/lfs’, según nuestro ejemplo.

Por último subrayar dos últimos puntos:

NOTA 1: Las instrucciones suponen que los requerimientos del sistema anfitrión, incluyendo los links simbólicos, se han definido correctamente.
 
 - ‘Bash’ es la consola 
 - ‘Sh’ es un link simbólico a ‘bash’
 - ‘/usr/bin/awk’ es un link simbólico a ‘gawk’
 - ‘/usr/bin/yacc’ es un link simbólico a ‘bison’ o un pequeño programa que ejecuta ‘bison’

NOTA 2: Volver a subrayar sobre el proceso de construcción:
 
- Guardar todas las fuentes y parches en un directorio que sea accesible desde el entorno de chroot, como ‘/mnt/lfs/sources/’. No guardar las fuentes en ‘/mnt/lfs/tools/’.
 - Moverse al directorio de las fuentes
 - Para cada paquete:
     - Usar el programa ‘tar’, para extraer el paquete que va a ser construido. Hay que asegurarse de ser el usuario ‘lfs’ al extraer el paquete.
     - Cambiar al directorio creado cuando el paquete ha sido extraído
     - Seguir las instrucciones para construir el paquete.
     - Volver al directorio de fuentes.
     - Borrar el directorio de las fuentes descomprimidas, a menos que se indique lo contrario.

## 5.4 Binutils 2.31.1 pasada 1

El paquete ‘Binutils’ contiene un linker, ensamblador y otras herramientas para manejar archivos objeto.

**Tiempo aproximado de construcción**: 1 SBU

**Espacio en el disco requerido**: 576 MB

### 5.4.1 Instalación de Binutils cruzado

Es importante que Binutils sea el primer paquete en compilarse ya que tanto Glibc y GCC ejecutan varias pruebas de test en el linker disponible y en el ensamblador para determinar cuáles de sus propias características pueden activar.

La documentación de Binutils recomienda construirlo en un directorio dedicado a tal propósito:

```bash
mkdir -v build
cd       build
```

NOTA: Para que los valores SBU listados en el resto de este libro sean de utilidad, se ha de tomar nota de cuanto tiempo tarda en construirse este paquete desde la configuración hasta la primera instalación. Esto puede hacerse fácilmente, utilizando el comando time, alrededor de los comandos, de la siguiente manera: ‘ time { ./configure ... && ... && make install; }’
 
NOTA: Los valores aproximados de construcción y espacio de disco necesario indicados en el capítulo 5, no incluyen los test. 


Ahora para compilar Binutils:

```bash
../configure --prefix=/tools            \
             --with-sysroot=$LFS        \
             --with-lib-path=/tools/lib \
             --target=$LFS_TGT          \
             --disable-nls              \
             --disable-werror
```

Estas opciones de configuración significan:


Estas opciones de configuración significan:

--prefix=/tools
Le indica al script de configuración que instale Binutils en el directorio /tools

--with-sysroot=$LFS
Para compilación cruzada, indica al sistema a mirar en $LFS para las librerías que vaya a necesitar

--with-lib-path=/tools/lib
Especifica que libreria será configurada para que use el linker

--target=$LFS_TGT
Ya que la descripción de la variable $LFS_TGT es ligeramente distinta que el valor devuelto por el script ‘config.guess’, esto indicará al script ‘configure’ como ajustar el sistema de construcción de Binutils para construir el linker cruzado

--disable-nls
Deshabilita la internacionalización, que no es necesaria para las herramientas intermedias

--disable-werror
Deshabilita la interrupción del proceso de construcción en el caso que haya avisos del compilador del sistema anfitrión.

Continuamos con la compilación del paquete:

```bash
make
```

Después de esto, la compilación estará completada. Normalmente ejectutariamos los test del paquete de software, pero en esta pronta etapa, la plataforma de test (Tcl, Expect y DejaGNU) no está todavía. Los beneficios de ejecutar los test en este punto, son mínimos ya que los programas de esta primera etapa, serán pronto reemplazados por los programas de la segunda pasada.

Si estamos construyendo una arquitectura x86_64, crearemos un link simbólico para asegurar la cadena de herramientas.

Se instala el paquete

```bash
make install
```
más información sobre este paquete en la sección 6.16.2 "Contenido de Binutils".

## 5.5 INSTALACION DE GCC

El paquete GCC contiene el compilador GNU, que incluye los compiladores de C y C++.

**Tiempo aproximado de construcción** : 14.3 SBU

**Espacio requerido en el disco:** 2.2 GB
 
### 5.5.1 Instalación del compilador cruzado GCC 8.2.0 - primera pasada

GCC requiere los paquetes GMP, MPFR y MPC. Si estos paquetes no están incluidos en la distribución anfitrión, se construirán con GCC. Hay que descomprimir cada paquete en el directorio del código fuente de GCC y renombrar los directorios resultantes de manera que los procesos de construcción de GCC los utilicen de manera automática.

**NOTA**: Hay frecuentes malentendidos sobre este capítulo. Los procedimientos son los mismo que cualquier otro capítulo como se describió anteriormente (Instrucciónes de construcción de paquetes). Primero extraer el tarball del gcc del directorio de fuentes y moverlo al directorio creado. Solo entonces se debe avanzar con las instrucciones a continuación. 

```bash
tar -xf ../mpfr-4.0.1.tar.xz
mv -v mpfr-4.0.1 mpfr
tar -xf ../gmp-6.1.2.tar.xz
mv -v gmp-6.1.2 gmp
tar -xf ../mpc-1.1.0.tar.gz
mv -v mpc-1.1.0 mpc
```

El comando a continuación cambiará la localización del linker dinámico de GCC para usar el que se ha instalado en ‘/tools’. También elimina el directorio ‘/usr/include’ del camino de búsqueda de GCC.  Issue:

```bash
for file in gcc/config/{linux,i386/linux{,64}}.h
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done
```

El caso anterior, es un poco complicado de seguir. Analizemos punto a punto. Inicialmente copiamos los archivos ‘gcc/config/linux.h’, ‘gcc/config/i386/linux.h’, y gcc/config/i368/linux64.h a un archivo añadido con el mismo nombre pero con un sufijo añadido ‘.orig’. Entonces la primera expresión ‘sed’ antepone ‘/tools’ a cada ocurrencia de ‘/lib/ld’, ‘/lib64/ld’ o ‘/lib32/ld’, mientras que la segunda reemplaza las instancias hard-codeadas de ‘/usr’. A continuación añadimos las declaraciones de definiciones con que modifican el prefijo del archivo de comienzo por defecto., al final del archivo. Notar que ‘/’ finales en ‘/tools/lib’ son necesarios. Finalmente, se usa ‘touch’ para actualizar el timestamp de los archivos copiados. Cuando se usa en conjunto con cp -u, esto evita los cambios inesperados en los archivos originales, en el caso que por despiste se ejecute esta rutina dos veces.

Por último en sistemas x86_64, se define el nombre del directorio por defecto de las librerías como “lib”:

```bash
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
 ;;
esac
```
La documentación de GCC recomienda construirlo en un directorio dedicado:

```bash
mkdir -v build
cd       build
```

Preparación para la compilación:

```bash
../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=/tools                                \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --with-local-prefix=/tools                     \
    --with-native-system-header-dir=/tools/include \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libmpx                               \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++
```
Explicación de las opciones de configuración:

--with-newlib

Como en este punto no se pueden usar las librerias de C todavía, este indicador asegura que la constante ‘inhibit_libc’ está definida cuando se construye libgcc

--without-headers

Cuando crear un compilador cruzado completo, GCC necesita las cabeceras de las librerías estándar completas, que correspondan a la arquitectura del sistema objetivo. Para nuestro objetivo estas cabeceras no serán necesarias. Esta opción evita que GCC las busque.

--with-local-prefix=/tools

Los prefijos locales indican la localización en el sistema que GCC buscará para instalar localmente los archivos que se han de incluir (‘include’). Por defecto esta en ‘/usr/local’. Hay que definirlo como ‘/toolshelps’ para que se excluya la localización del sistema anfitrión ‘/usr/local’ y excluir esta de la ruta de búsqueda de GCC.

--with-native-system-header-dir=/tools/include

Por defecto en GCC incluye en su búsqueda ‘/usr/include’ para los archivos de cabecera del sistema. Además de el ‘sysroot switch’, esto normalmente se traducirá como ‘$LFS/usr/include’. De todas formas las cabeceras que serán instaladas en las dos secciones siguientes irán a ‘$LFS/tools/include’. Este cambio asegura que GCC las encontrará correctamente. En la segunda pasada de GCC, este mismo cambio asegurará que no se pueda recurrir a las cabeceras del sistema anfitrión. 

--disable-shared

Este cambio fuerza a GCC a unir (‘linkar’) sus librerías internas de forma estática. Se hace esto para evitar posibles interferencias con el sistema anfitrión.

--disable-decimal-float, --disable-threads, --disable-libatomic, --disable-libgomp, --disable-libmpx, --disable-libquadmath, --disable-libssp, --disable-libvtv, --disable-libstdcxx

Estas opciones deshabilitan el soporte de los números decimales con coma flotante, hilos (threading), libatomic, libgomp, libmpx, libquadmath, libssp, libvtv,y la librería estándar C++ respectivamente. Estas características fallarían en la compilación cuando se construyera el compilador cruzado y no son necesarias en la talea de la compilación cruzada de libc. 

--disable-multilib

En sistemas de arquitectura x86_64, LFS no soporta todavía la configuración multilib. Esta opción no tienen ningún significado para x86.

--enable-languages=c,c++

Esta opción asegura que solo se construyen los compiladores de C y C++. Estos son los únicos lenguajes necesarios.


Se compila GCC ejecutando

```bash
make
```

La compilación está completa. En este punto, los grupos de test, normalmente se ejecutarian, pero como se indicó anteriormente, estos test no estan todavia colocados. Los beneficios de ejecutar estos test en este punto son mínimos ya que los programas de esta primera pasada serán eliminados en próximos pasos.


Para instalar el paquete, se realiza:

```bash
make install
```
Detalles sobre este paquete de software, se pueden encontrar en la sección 	6.21.2 “Contenidos de GCC”.

##  5.6 Linux -4.18.5 Cabeceras API

Las APIS de la cabecera de Linux (en linux-4.18.5.tar.xz) expone la API del kernel para el uso de Glibc.

**Tiempo aproximado de construcción** : 0.1 SBU

**Espacio requerido en el disco:** 910 MB

### 5.6.1 Instalación de las cabeceras de Linux

El kernel de Linux ha de presentar su interfaz de programación de aplicaciones (API) para que pueda ser usada por la librería de C del sistema (en el caso de LFS es Glibc). Esto se hace sanitizando varas cabeceras de C que están incluidas en el paquete del kernel de Linux.

Hay que asegurarse que no hay archivos obsoletos en el paquete:

```bash
make mrproper
```

Ahora se ha de extraer del software la parte visible al usuario de las cabeceras del kernel. Estos están situados en un directorio local intermedio y se copian en la localización de destino ya que en el proceso de extracción se borran los archivos existentes en el fichero de destino.

```bash
make INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* /tools/include
```

Detalles sobre este paquete están en la Sección 6.7.2 “Contenidos de la API de las cabeceras de Linux”


## 5.7 Glibc - 2.28

Glibc es el paquete que contiene la librería principal de C. Esta librería provee las funcionalidades básicas para la reserva de memoria, búsqueda de directorios, abrir y cerrar archivos, lectura y escritura de archivos, cadenas de caracteres, identificación de patrones, funciones matemáticas et cetera.

**Tiempo aproximado de construcción** : 4.7 SBU

**Espacio requerido en el disco:** 882 MB

### 5.7.1 Instalacion de Glibc

La documentación de Glibc recomienda construirlo en un directorio dedicado.

```bahs
mkdir -v build
cd       build
```

A continuación se prepara para construirlo

```bash
../configure                             \
      --prefix=/tools                    \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2             \
      --with-headers=/tools/include      \
      libc_cv_forced_unwind=yes          \
      libc_cv_c_cleanup=yes
```






Explicación de las opciones de configuración:


--host=$LFS_TGT, --build=$(../scripts/config.guess)

Todas los opciones anteriores implican que la configuración con la que se construirá Glibc es para un compilador cruzado, usando un linker cruzado y el compilador cruzado en ‘/tools’.

--enable-kernel=3.2

Esta opción indica que se compilara la librería de manera que soporte los kernel 3.2 y posteriores de Linux. Soluciones para hacer funcionar kernel anteriores no serán activadas.

--with-headers=/tools/include

Esta opción indica a Glibc que se compile con los archivos de cabecera que se han instalado recientemente en el directorio ‘/tools’, de manera que sepa exactamente qué características tienen el kernel, para que pueda optimizarse el mismo de manera acorde al kernel.

--libc_cv_forced_unwind=yes

El linker que se instaló durante la sección 5.4, “Binutils-2.31.1  - Primera pasada” fue compilado cruzadamente y como tal no puede ser usado hasta que Glibc haya sido instalado. Esto implica que el test de la configuración para verificar la instalación fallará debido a que se apoya en el linker. La opción ‘libc_cv_forced_unwind=yes’ variable se especifica para informar a ‘configure’ que no es necesario ejecutar el test.


libc_cv_c_cleanup=yes
Se indica al script ‘configure’ que el test no se ejecute y que además está configurada la gestión de C cleanup.


Durante esta etapa podría aparecer la siguiente alerta:





```
configure: WARNING:
*** These auxiliary programs are missing or
*** incompatible versions: msgfmt
*** some features will be disabled.
*** Check the INSTALL file for required versions.
```


El programa incompatible o que falta ‘msgfmt’ normalmente no perjudicial. Este programa ‘msgfmt’  es parte del paquete `Gettext` y que debería aportar la distribución anfitrión.

NOTA: Ha habido casos, en el que este paquete a fallado al construirse, cuando se ha usado un ‘make’ en paralelo. Si ocurriera esto, se debería volver a ejecutar el comando con la opción ‘-j1’.

Se compila el paquete:

```
make
```


Se instala el paquete

```bash
make install
```

ATENCIÓN: En este punto es importante hacer un alto, y comprobar que las funciones básicas de compilación y linkado están funcionando como deberían. Para realizar una verificación de seguridad, se puede ejecutar el siguiente comando:

```bash
echo 'int main(){}' > dummy.c
$LFS_TGT-gcc dummy.c
readelf -l a.out | grep ': /tools'
```

Si todo va bien, no debería haber errores, y la salida del último comando, debería ser:

```bash
[Requesting program interpreter:  /tools/lib64/ld-linux-x86-64.so.2]
```

Hay que tener en cuenta, que para sistemas con arquitectura de 32 bits, el nombre del intérprete será: /tools/lib/ld-linux.so.2.

Si la salida es distinta a la anterior, o no hay salida, entonces algo no ha salido correctamente. Habría que investigar e identificar el problema para corregirlo. Este error hay que corregirlo antes de poder seguir más adelante.

Una vez ya todo sea correcto, podemos limpiar los archivos y probarlos de nuevo:

```bash
rm -v dummy.c a.out
```

NOTA: cuando se construya Binutils, también servirá como una comprobación adicional para ver que la cadena de herramientas ha sido construida de forma correcta. Si el paquete Binutils no se construyera correctamente, sería un indicador de que algo no ha ido bien con la instalación de Binutils, GCC o Glibc.

Detalles de este paquete están en la Sección 6.9.3 “Contenidos de Glibc”



## 5.8. Libstdc++ from GCC-8.2.0

Libstdc++ es la libreria standard de C++. Se necesita para compilar el código C++ (parte de GCC está escrito en C++), pero se tuvo que evitar su instalación cuando se construyó gcc en la primera pasada, ya que depende de glibc, que no estaba disponible todavía en ‘/tools’.


**Tiempo aproximado de construcción** : 0.5 SBU
**Espacio requerido en el disco:** 803 MB

### Instalación de libstdc++

NOTA: Libstdc++ es parte de las fuentes de GCC. Así que primero se ha de descomprimir GCC y moverlo al directorio gcc-8.2.0.

Para crear un directorio separado para Libstdc++ se ejecutarán:

```
mkdir -v build
cd       build
```

Se ha de preparar Libstdc++ para compilarlo:

```
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --prefix=/tools                 \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-threads     \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/8.2.0
```

Significado de las opciones de configuración:

--host=...

Indica que hay que usar el compilador cruzado que acabamos de construir en lugar de usar el que está en ‘/usr/bin’.

--disable-libstdcxx-threads

Al no estar construidas las librerías the hilos ‘C threads’, C++ tampoco puede construirlas.

--disable-libstdcxx-pch

Esto evita la instalación de archivos de cabecera precompilados, que son los que no necesitamos en este punto.

--with-gxx-include-dir=/tools/$LFS_TGT/include/c++/8.2.0

Este es la localización donde los ficheros de cabecera estándar serán buscados por el compilador de C++. En una construcción normal, esta información serán automáticamente pasada a las opciones de configuración de Libstdc++ desde el directorio base. En nuestro caso, esta información se ha de especificar explícitamente.

Se compila ejecutando los comandos:


```bash
make
```

Y se instala:


```bash
make install
```

Detalles de este paquete se encuentran en la sección 6.21.2 “contenidos de GCC”


## 5.9. Binutils-2.31.1 - Pasada 2

El paquete de Binutils contiene un linkador, un ensamblador, y otras herramientas para gestionar objetos de archivos.

**Tiempo aproximado de construcción** : 1.1 SBU
**Espacio requerido en el disco:** 593 MB

### 5.9.1 Instalación de Binutils

Se ha de crear de nuevo un directorio para el paquete:

```bash
mkdir -v build
cd       build
```

Se prepara Binutils para compilarlo

```bash
CC=$LFS_TGT-gcc                \
AR=$LFS_TGT-ar                 \
RANLIB=$LFS_TGT-ranlib         \
../configure                   \
    --prefix=/tools            \
    --disable-nls              \
    --disable-werror           \
    --with-lib-path=/tools/lib \
    --with-sysroot
```

Significado de las opciones de configuración:

CC=$LFS_TGT-gcc AR=$LFS_TGT-ar RANLIB=$LFS_TGT-ranlib

Como es una construcción nativa de Binutils, la definición de estas variables asegura que la construcción del sistema usa el compilador cruzado, y las herramientas asociadas, en vez de las herramientas del sistema anfitrión.


--with-lib-path=/tools/lib

Esto le indica al script de configuración la dirección donde ha de buscar las librerías para compilar Binutils, indicando al enlazador /tools/lib. Esto evita que el enlazador busque por los directorios de librerías del sistema anfitrión.


--with-sysroot

La definición de sysroot, permite al enlazador encontrar los objetos compartidos que necesitan otros objetos explícitamente incluidos en el enlazador. Sin esto, algunos paquetes pueden no construirse con éxito en algunos sistemas anfitriones.


para compilar el paquete:


```
 make
```

Para instalar el paquete:

```
make install
```

Para preparar el enlazador para la parte de reajuste en el próximo capítulo:

```
make -C ld clean
make -C ld LIB_PATH=/usr/lib:/lib
cp -v ld/ld-new /tools/bin
```

El significado de los parámetros:

-C ld clean

Esto indica al programa make que elimine todos los archivos compilados en el directorio ld

-C ld LIB_PATH=/usr/lib:/lib

Esta opción reconstruye todo en el subdirectorio ld. Indicando la variable de Makefile LIB_PATH en la línea de comandos, nos permite sobreescribir el valor por defecto de las herramientas temporales y apuntar a la localización final correcta. El valor de esta variable indica al enlazador el valor por defecto del camino de búsqueda de librerías. Esto será de utilidad en el siguiente capítulo.

Detalles sobre este paquete están localizados en la Sección 6.16.2 “Contenidos de Binutils”

## 5.10 GCC-8.2.0 - Pasada 2

El paquete GCC contiene el compilador de GNU, que incluye los compiladores de C y C++


**Tiempo aproximado de construcción** : 11 SBU

**Espacio requerido en el disco:** 3.4 GB

### 5.10.1. Instalación de GCC

En nuestra primera construcción de GCC se han instalado un par de cabeceras internas del sistema. Normalmente, una de ellas, limits.h, incluye a su vez, la cabecera del sistema limits.h, en este caso /tools/include/limits.h. De todas, maneras, en la primera construcción de gcc /tools/include/limits.h no existía, así que las cabeceras internas que ha instalado GCC es una parte auto-contenida y que no incluye características extendidas de las cabeceras del sistema. Esto era adecuado para construir de forma temporal libc, pero en esta construcción de GCC ahora requiere todas las cabeceras internas. Se crea una versión completa de las cabeceras internas usando un comando idéntico a lo que la construcción de GCC hace en circunstancias normales.


```bash
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
  `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/include-fixed/limits.h
```

De nuevo, se cambia la localización del enlazador dinámico de GCC para que use el instalado en /tools

```bash
for file in gcc/config/{linux,i386/linux{,64}}.h
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done
```

Si se construye en un sistema x86_64, se cambia nombre del directorio por defecto para librerías de 64-bits, por “lib”:

```bash
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
esac
```

Como en la primera construcción de GCC son necesarios los paquetes GMP, MPFR y MPC. Se descomprimen las tarballs y se mueven en los directorios con los nombres requeridos:


```bash
tar -xf ../mpfr-4.0.1.tar.xz
mv -v mpfr-4.0.1 mpfr
tar -xf ../gmp-6.1.2.tar.xz
mv -v gmp-6.1.2 gmp
tar -xf ../mpc-1.1.0.tar.gz
mv -v mpc-1.1.0 mpc
```

De nuevo, se crea un directorio nuevo para la construcción:


```bash
mkdir -v build
cd       build
```

Antes de comenzar a construir el GCC, hay que recordar desactivar cualquier variable de entorno que sobreescriba las directivas de optimización por defecto.

Nos preparamos para lanzar la construcción:

```bash
CC=$LFS_TGT-gcc                                    \
CXX=$LFS_TGT-g++                                   \
AR=$LFS_TGT-ar                                     \
RANLIB=$LFS_TGT-ranlib                             \
../configure                                       \
    --prefix=/tools                                \
    --with-local-prefix=/tools                     \
    --with-native-system-header-dir=/tools/include \
    --enable-languages=c,c++                       \
    --disable-libstdcxx-pch                        \
    --disable-multilib                             \
    --disable-bootstrap                            \
    --disable-libgomp
```



Significado de las nuevas opciones:

--enable-languages=c,c++

Asegura que se construyen tanto el compilador de c como el de c++


--disable-libstdcxx-pch

No se construye la cabecera precompilada (PCH) para libstcd++. Esto requiere mucho espacio, y no nos es útil.


--disable-bootstrap

Para construcciones nativas de GCC, el valor por defecto es hacer una construcción con “bootstrap”. Esto no solo compila GCC, pero además lo compila varias veces. Esto usa los programas que se han compilado en la primera fase, para que se compile GCC una segunda vez, y entonces una tercera vez. La segunda y tercera iteraciones son comparadas para asegurarse que se puede reproducir sin error alguno. Esto también implica que se ha compilado correctamente. De todas formas, el método de construcción de LFS, ha de proveer un compilador que no necesite recurrir al “bootstrap” cada vez.


Se compila el paquete

```bash
make
```

Se instala el paquete:

```bash
make install
```

Como punto y final, se crea un link simbólico. Muchos programas y scripts ejecutan cc en vez de gcc, lo cual se usa para mantener los programas genéricos y así puedan ser usados por todo tipo de sistemas UNIX, en los cuales el compilador GNU C no fuera instalado. La ejecución de cc permite al administrador del sistema libre para decidir qué compilador de C instalar.


```bash
ln -sv gcc /tools/bin/cc
```


ATENCIÓN:

En este punto, es obligatorio pararse y asegurarse que las funciones básicas, (compilación y enlazado) de la nueva cadena de herramientas, funcionan como se espera. Para realizar esta verificación de seguridad, se ejecutan los siguientes comandos:


```bash
echo 'int main(){}' > dummy.c
cc dummy.c
readelf -l a.out | grep ': /tools'
```

Si todo funciona correctamente, no debería haber errores, y el resultado del último comando debería ser:

```bash
[Requesting program interpreter: /tools/lib64/ld-linux-x86-64.so.2]
```

Hay que tener en cuenta que el enlazador dinámico para máquinas de 32-bits será /tools/lib/ld-linux.so.2

Si la salida no es como la que se muestra o no hay salida ninguna, entonces es que algo no es correcto. Hay que investigar y re-trazar los pasos para descubrir dónde está el problema y corregirlo. Este problema ha de ser resuelto antes de continuar. Primero, se realizará la verificación de sanidad, usando gcc en vez de cc. Si esto funciona, entonces lo que falta es el link simbólico /tools/bn/cc. Se instala el link simbólico como se indicó anteriormente. A continuación se asegura que el PATH es correcto. Esto se puede verificar ejecutando echo $PATH y verificando que /tools/bin está definido a la cabeza de la lista. Si el PATH no es correcto esto significa que no se está logado como el usuario *lfs* o que algún paso de la Sección 4.4 “Definiendo el entorno” no se ha ejecutado correctamente.

Una vez verificado que todo funciona como se espera, se eliminan los archivos del test.


```bash
rm -v dummy.c a.out
```

Más detalles de este paquete se definen en la sección 6.21.2 “Contenidos de GCC”.


### 5.11 TCL-8.6.9

El paquete TCL contiene las herramientas de comandos del lenguaje (Tool Command Language)

**Tiempo aproximado de construcción** : 0.9 SBU

**Espacio requerido en el disco:** 66 GB

 
### 5.11.1 Instalacion del TCL

Este paquete y los dos siguientes (Expect y DejaGNU) se instalan para apoyar los tests para GCC, Binutils y otros paquetes. La instalación de estos tres paquetes puede parecer excesiva, pero es tranquilizador, si no esencial, saber si las herramientas más importantes están funcionando correctamente. Incluso si los tests no se ejecutan en este capítulo (no son obligatorios), estos paquetes son necesarios para ejecutar los tests del capítulo 6.

Hay que tener en cuenta que el paquete Tcl que se usa en una versión mínima que se usa para ejecutar los tests LFS. Para el paquete completo, ver la sección BLFS Tcl procedures. 

Para preparar Tcl para la compilación:
 
```bash
cd unix
./configure --prefix=/tools
```
  
Construcción del paquete:

```bash
make
```

Ahora la compilación está completa. Como se presentó antes, la ejecución de los test no es obligatoria para las herramientas temporales de este capítulo. Para ejecutar los test para el paquete Tcl, se ejecuta:


```bash
TZ=UTC make test
```

Los test para Tcl suelen presentar fallos bajo ciertos sistemas anfitrión cuando no se conocen suficiente. Por eso los fallos aquí no son sorpresa, y no se consideran críticos. El parámetro TZ=UTC coordina la zona de tiempo al sistema universal de tiempo UTC, pero solo durante la duración de los test. Esto asegura que los test del reloj se ejecutan correctamente.Los detalles de la variable de entorno TZ se presentan el en capítulo 7.

Se instalará el paquete: 

```bash
make install
```
Ha de permitirse la escritura en la librería, para que los símbolos de debug puedan ser retirados posteriormente.

```bash
chmod -v u+w /tools/lib/libtcl8.6.so
```

Se han de instalar las cabeceras de Tcl. En el siguiente paquete: ‘Expect’ los necesita para poder ser construido.

```bash
make install-private-headers
```

Ha de crearse también un link simbólico:

```bash
ln -sv tclsh8.6 /tools/bin/tclsh
```

### 5.11.2 Contenidos de TCL

**Programas instalados** : tclsh (link a tclsh8.6) y  tclsh8.6

**Librerias instaladas:** libtcl8.6.so, libtclstub8.6.a

Pequeña descripción:

 - tclsh8.6: Consola de comandos de Tcl
 - tclsh: Enlace a  tclsh8.6
 - libtcl8.6.so: Libreria de Tcl
 - libtclstub8.6.a: Libreria Stub de Tcl



### 5.12 Expect-5.45.4

El paquete Expect contiene un programa para gestionar comunicaciones interactivas con otros programas

**Tiempo aproximado de construcción** : 0.1 SBU

**Espacio requerido en el disco:** 3.9 GB


#### 5.11.1 Instalación de Expect.

Primero se fuerza al script de configuración de Expect a usar /bin/stty en vez the /usr/local/bin/stty que puede venir del sistema anfitrión. Esto asegurará que nuestro test permanezca correcto hasta la construcción final de la cadena de herramientas
 
```bash
cp -v configure{,.orig}
sed 's:/usr/local/bin:/bin:' configure.orig > configure
```

Ahora se prepara para la compilación:


```bash
./configure --prefix=/tools       \
            --with-tcl=/tools/lib \
      --with-tclinclude=/tools/include
```

Significado de las opciones de instalación:

--with-tcl=/tools/lib
Esto asegura que los scripts de configuración encuentran la instalación de TCL en las localización de las herramientas temporales en vez de localizarlos en el sistema anfitrión en caso de que estuvieran instalados en este.

--with-tclinclude=/tools/include
Esto indica a Expect explicitamente donde encontrar las cabeceras internas de TCL. Al usar esta opción se evita que la configuración falle debido a que no puede localizar automáticamente las cabeceras de TCL.


Se construye el paquete:

```bash
make
```

La compilación estará completa. Como se discutió anteriormente, la ejecución de los test no es obligatoria para las herramientas temporales que se presentan en este capítulo. De todas formas, para ejecutar los test correspondientes a Expect, se ejecutaría el comando:

```
make test
```

Anotar que los test para Expect, son conocidos fallos bajo ciertas condiciones en el sistema anfitrión que no son controlables. De esa manera, fallos en este tes, no se consideran críticos.

Para instalar el paquete:

```bash
make SCRIPTS="" install
```

El parámetro: 

SCRIPTS=""

Evita la instalación de scripts suplementarios que no son necesarios.


#### 5.11.2 Contenidos de Expect.

**Programa instalado**: expect

**Libreria instalada**: libexpect-5.45.so

Breve descripción:

Expect se comunica con otros programas interactivos de acuerdo a un guión.

Libexpect-5.45.so  Contiene funciones que permiten Expect ser usado como una extensión TCL o usado directamente desde C o C++ (sin Tcl)


### 5.13 DejaGNU-1.6.2

El paquete DejaGNU es un entorno para testear otros programas


**Tiempo aproximado de construcción** :menos de  0.1 SBU

**Espacio requerido en el disco:** 3.2 GB

#### 5.13.1 Instalación de  DejaGNU-1.6.2

Se ha de preparar DejaGNU para su compilación:

```bash
./configure --prefix=/tools
```

Se construye, e instala el paquete.

```bash
make install
```

Para testear los resultados, ejecutamos

```bash
make check
```

####  5.13.2 Contenidos de DejaGNU

Programa instalado: runtest

Breve descripción:

runtest  un script que localiza la consola __shell__  correcta, y entonces ejecuta el programa runtest.


### 5.14 M4-1.4.18

El paquete M4 contiene un procesador de macros

**Tiempo aproximado de construcción** :menos de  0.2 SBU

**Espacio requerido en el disco:** 20 MB

#### 5.14.1 Instalación de M4

Primero, hacemos algunos arreglos que son necesarios por glibc-2.28

```bash
     sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h
```

Se prepara M4 para la compilación

```bash
./configure --prefix=/tools
```

Se compila el paquete:

```bash
    make
```

La compilación ahora está finalizada. Como se comentó anteriormente, la ejecución de los test no es obligatoria para las herramientas temporales en este capítulo. Para ejecutar los test de M4, se ejecutan los comandos:

```bash
make check
```

Y se instala el paquete:

```bash
make install
```

Detalles sobre este paquete se pueden encontrar en el capítulo 6.14.2 “Contenidos de M4”


### 5.15 Ncurses-6.1

Ncurses es un programa que contiene librerías para la gestión de impresión de caracteres en pantallas.


**Tiempo aproximado de construcción** :menos de  0.6 SBU

**Espacio requerido en el disco:** 41 MB


#### 5.15.1 Instalación de  Ncurses

Primero hay que asegurar que está el programa **_gack_** en la instalación.

```bash
sed -i s/mawk// configure
```

Se prepara Ncurses para la instalación:

```bash
./configure --prefix=/tools \
            --with-shared   \
            --without-debug \
            --without-ada   \
            --enable-widec  \
            --enable-overwrite
```

El significado de las opciones de configuración son:


--without-ada

  Esto asegura que Ncurses no incluya soporte para el compilador de Ada, que puede estar presente en el sistema anfitrión pero no este disponible una vez se entre en un entorno de **chroot**.


--enable-overwrite

 Esto le dice a Ncurses que instale sus archivos de cabecera en /tools/include, en vez de /tools/include/ncurses, para asegurar que otros paquetes puedan encontrar correctamente las cabeceras de Ncurses 


--enable-widec

  Esta opción hace que las librerías de caracteres extendidos (por ejemplo libncursesw.so.6.1) se construirían en vez de las normales (por ejemplo libncurses.so.61). Estas librerías de caracteres extendidos se usan en tanto los sistemas de 8 bits como multibytes, mientras que las librerías normales trabajan con tamaños de 8-bits. Las librerías basadas en caracteres de 8-bits o multi bits son compatibles a nivel de código, pero no son compatibles a nivel binario.


Este paquete tiene un grupo de tests, pero únicamente se puede ejecutar después de que el paquete ha sido instalado. Los tests están situados en el directorio /test. Léase el archivo README en ese directorio para más detalles.

Para instalar el paquete:

```bash
      make install
ln -s libncursesw.so /tools/lib/libncurses.so
```

Más detalles sobre este paquete, se encuentran en la sección 6.24.2, “Contenidos de Ncurses”


### 5.16 Bash-5.0

Este paquete contiene el terminal Bash

**Tiempo aproximado de construcción** :menos de  0.4 SBU

**Espacio requerido en el disco:** 67 MB


#### 5.16.1 Instalación de Bash

Hay que preparar el paquete Bash para su compilación.

```bash
make
```

Una vez finalizada la compilación. Como se comentó anteriormente, la ejecución del test no es obligatoria para las herramientas temporales en este capítulo. De todas formas, para ejecutar los test, se usan los comandos:


```
make test
```

Para instalar el programa:

```bash
make install
```

Para crear un link, para aquellos programas que usen **sh** como consola

```bash
ln -sv bash /tools/bin/sh
```

Más detalles sobre este paquete están en la Sección 6.34.2 “Contenidos de Bash”


### 5.17 Bison-3.3.2

Este paquete contiene un generador de parser


**Tiempo aproximado de construcción** :menos de  0.3 SBU

**Espacio requerido en el disco:** 37 MB


#### 5.17.1 Instalación de Bison

Hay que preparar el paquete Bison para su compilación


```bash
./configure --prefix=/tools
```

Compilarlo:

```bash
make
```

Para verificarlo:

```bash
make check
```

Para instalarlo:

```bash
make install
```

Más detalles sobre este paquete están en la Sección 6.31.2 “Contenidos de Bison”


### 5.18 Bzip2-1.0.6

El paquete Bzip2 contiene programas para comprimir  y descomprimir archivos. Comprimir los archivos con bzip tienen un rango de compresion mucho mejor que con el anterior gzip.


**Tiempo aproximado de construcción** :menos de  0.1 SBU

**Espacio requerido en el disco:** 5.5 MB


#### 5.18.1 Instalación de Bzip2

El paquete Bzip2 no tiene un script **configure**. Se compila y verifica con:

```bash
make
```

Se instala el paquete con:

```bash
make PREFIX=/tools install
```

Detalles sobre el contenido del paquete Bzip2 se encuentran en la sección 6.22.2 “Contenidos de Bzip2”.


### 5.19 Coreutils-8.30

El paquete Coreutils contiene utilidades para mostrar y definir las características básicas del sistema.


**Tiempo aproximado de construcción** :menos de  0.8 SBU

**Espacio requerido en el disco:** 148 MB


#### 5.19.1 Instalación de Coreutils

Para preparar el paquete para su compilación se ejecuta:

```bash
./configure --prefix=/tools --enable-install-program=hostname
```

El significado de las opciones de configuración son:

--enable-install-program=hostname
Esto permite al binario de *hostname* ser construido e instalado. Esta opción está deshabilitada por defecto y es necesaria por el entorno de test de Perl

Para compilar el paquete:

```bash
make
```

La compilación estária completa. Como se discutió anteriormente, ejecutar los test de componente no son un requisito necesario en las herramientas temporales de este capítulo. Para verificar estos test en este caso, se ejecutaría con el comando:

```bash
make RUN_EXPENSIVE_TESTS=yes check
```

La opción  RUN_EXPENSIVE_TESTS=yes   indica que se ejecuten tests extra, que se suelen considerar caros en relación a su uso de CPUs y uso de memoria en algunas plataformas, aunque no son un problema normalmente en Linux.

Para instalar el paquete:

```bash
make install
```

Más detalles sobre este paquete en la sección 6.54.2 “Contenidos de Coreutils”.



### 5.20 Diffutils-3.7

El paquete Diffutils contiene programas que muestran la diferencia entre archivos o directorios.


**Tiempo aproximado de construcción** :menos de  0.2 SBU

**Espacio requerido en el disco:** 26 MB


#### 5.19.1 Instalación de Diffutils

Para preparar el paquete para su compilación se ejecuta:

```bash
./configure --prefix=/tools
```

Para compilar el paquete 

```
make
```

La compilación debería estar completa. Como se mencionó anteriormente, ejecutar los test, no es obligatorio para las herramientas temporales de este capítulo. De todos modos, para ejecutar los tests para este paquete:

```bash
make check
```

Para instalar el paquete:

```bash
make install
```

Más detalles sobre este paquete en la sección 6.56.2 “Contenidos de Diffutils”.

### 5.21 File-5.36

El paquete File contiene una utilidad para determinar el tipo de un archivo o archivos.


**Tiempo aproximado de construcción** :menos de  0.1 SBU

**Espacio requerido en el disco:** 18 MB


#### 5.21.1 Instalación de File

Para preparar el paquete para su compilación se ejecuta:


```bash
./configure --prefix=/tools
```

Para compilar el paquete:

```bash
make
```

La compilación debería estar completa. Como se mencionó anteriormente, ejecutar los test, no es obligatorio para las herramientas temporales de este capítulo. De todos modos, para ejecutar los tests para este paquete:


```bash
make check
```

Para instalarlo

```bash
make install
```

Más detalles sobre este paquete en la sección 6.12.2 “Contenidos de File”.



### 5.22 Findutils-4.6.0

El paquete Findutils contiene programas para encontrar archivos. Estos programas hacen búsquedas recursivas a través de un árbol de directorios para crear, mantener y buscar en una base de datos (normalmente es más rápido que solo hacer una búsqueda recursiva, pero menos fiable si la base de datos no ha sido actualizada recientemente).


**Tiempo aproximado de construcción** :menos de  0.3 SBU

**Espacio requerido en el disco:** 36 MB


#### 5.22.1 Instalación de Findutils

Primero hay que crear unos archivos que necesita glibc-2.28

```bash
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' gl/lib/*.c
sed -i '/unistd/a #include <sys/sysmacros.h>' gl/lib/mountlist.c
echo "#define _IO_IN_BACKUP 0x100" >> gl/lib/stdio-impl.h
```

Se prepara Findutils para su compilación



```bash
./configure --prefix=/tools
```

Se compila el paquete

```bash
make
```

La compilación está finalizada. Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utilizan los siguientes comandos:


```bash
make check
```

Para instalar el paquete:

```bash
make install
```

Detalles en este paquete se encuentran en la sección 6.58.2 “Contenidos de Findutils”.

### 5.23 Gawk-4.2.1

El paquete Gawk contiene utilidades para la manipulación de textos.


**Tiempo aproximado de construcción** :menos de  0.2 SBU

**Espacio requerido en el disco:** 43 MB


#### 5.22.1 Instalación de Gawk

Se ha de preparar Gawk para su compilación

```bash
./configure --prefix=/tools
```

Se compila el paquete

```bash
make
```

La compilación está completa, como se comentó anteriormente probar los test no es obligatorio para las herramientas temporales en este equipo. De todas formas para ejecutar los test, se ejecuta:

```bash
make check
```

Para instalar el paquete:

```bash
make install
```

Detalles sobre este paquete, en la sección: 6.57.2 “Contenidos de Gawk”


### 5.24 Gettext-0.19.8.1

El paquete Gettext contiene utilidades la internacionalización y localización. Esto permite a los programas compilarse con el soporte del lenguaje local NLS (del inglés Native Language Support), permitiendo que los mensajes estén en el lenguaje del usuario.

**Tiempo aproximado de construcción** :menos de  0.9 SBU

**Espacio requerido en el disco:** 173 MB

#### 5.24.1 Instalación de Gettext

Para las herramientas temporales que estamos montando, solamente será necesario construir e instalar tres programas de Gettext. 


Se prepara Gettext para la compilación:

```bash
cd gettext-tools
EMACS="no" ./configure --prefix=/tools --disable-shared
```

Significado de las opciones de compilación:

EMACS="no"

Esto evita que el script de configuración determine dónde instalar los archivos de Lisp de Emacs, ya que es conocido que este test falla en algunos tipos de sistemas

--disable-shared
En este momento no es necesario instalar las librerías compartidas de Gettext, y no es necesario por tanto compilarlas.

Se compila el paquete:

```bash
make -C gnulib-lib
make -C intl pluralx.c
make -C src msgfmt
make -C src msgmerge
make -C src xgettext
```

Como solamente se han compilado tres programas, no es posible ejecutar los test sin la necesidad de compilar más librerías de Gettext. Por tanto no es recomendable intentar ejecutar los test en este punto.

Se instalan los programas:  msgfmt, msgmerge y xgettext 

```bash
cp -v src/{msgfmt,msgmerge,xgettext} /tools/bin
```

Detalles sobre este paquete, en la sección: 6.47.2 “Contenidos de Gettext”

### 5.25 Grep-3.3

El paquete Grep contiene programas para buscar dentro de archivos

**Tiempo aproximado de construcción** :menos de 24 SBU

**Espacio requerido en el disco:** 173 MB


#### 5.25.1 Instalación de Grep

Se prepara Grep para la compilación

```bash
./configure --prefix=/tools
```

Se compila el paquete:

```bash
make
```

La compilación está ahora completa.Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utiliza el siguiente comando:

```bash
make check
```

Se instala el paquete:

```bash
make install
```

Más detalles sobre este paquete en la sección 6.33.2 “Contenidos de Grep”


### 5.26 Gzip-1.10

El paquete Grep contiene programas para comprimir y descomprimir archivos

**Tiempo aproximado de construcción** :menos de 0.1 SBU

**Espacio requerido en el disco:** 10 MB


#### 5.26.1 Instalación de Gzip

Se prepara Gzip para la compilación:

```bash
./configure --prefix=/tools
```

Se compila el paquete:

```bash
make
```

La compilación está ahora completa.Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utiliza el siguiente comando:


```bash
make check
```

Se instala el paquete:

```bash
make install
```

Más detalles sobre este paquete en la sección 6.62.2 “Contenidos de Gzip”



### 5.27 Make-4.2.1

El paquete Make contiene un programa para compilar paquetes

**Tiempo aproximado de construcción** :menos de 0.1 SBU

**Espacio requerido en el disco:** 12 MB


#### 5.27.1 Instalación de Make

Se prepara hay que sortear un error debido a glibc-2.2.7 y posteriores

```bash
sed -i '211,217 d; 219,229 d; 232 d' glob/glob.c
```

Se prepara Make para la compilación:


```bash
./configure --prefix=/tools --without-guile
```

El significado de las opciones de compilación:

--without-guile

Esto asegurará que Make-4.2.1 no se enlazará con las librerías Guile que pudieran estar presentes en el sistema anfitrión, pero que no estarán disponibles en el entorno chroot del próximo capítulo.


Para compilar los paquetes:

```bash
make
```

Ahora se ha completado la compilación. Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utiliza el siguiente comando:


```bash
make check
```


Para instalar el paquete:

```bash
make install
```

Detalles sobre este paquete se encuentran en la Sección 6.66.2 Contenidos de Make.



### 5.28 Patch-2.7.6

El paquete Patch contiene un programa para modificar o crear archivos aplicando un “parche” mediante un archivo, normalmente creado por el programa “diff”.


**Tiempo aproximado de construcción** 0.2 SBU

**Espacio requerido en el disco:** 12 MB


#### 5.28.1 Instalación de Patch

Se prepara para la compilación:

```bash
./configure --prefix=/tools
```
Se compila

```bash
make
```

Ahora se ha completado la compilación. Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utiliza el siguiente comando:


```bash
make check
```

Se instala el paquete

```bash
make install
``` 

Detalles de este paquete están especificados en la Sección 6.67 Contenidos de Patch




### 5.29 Perl-5.28.1

El paquete Perl, contiene el lenguaje Perl (“Practical Extraction and Report Language”)


**Tiempo aproximado de construcción** 1.6 SBU

**Espacio requerido en el disco:** 275 MB


#### 5.29.1 Instalación de Perl

Se prepara Perl para la compilación:

```bash
sh Configure -des -Dprefix=/tools -Dlibs=-lm -Uloclibpth -Ulocincpth
```

Significado de las opciones de configuración:

-des

Es una combinación de tres opciones: -d usa los valores por defecto para todos los items; -e asegura la terminación completa de todas las tareas; - s silencia los mensajes no necesarios.


-Uloclibpth amd -Ulocincpth


Estas opciones anulan las variables que causa que la configuración busque  los componentes instalados localmente que puedan existir en el sistema anfitrión. 


Para construir el paquete:

```bash
make
```

Algunas otras utilidades y librerías han de instalarse a la vez:


```bash
cp -v perl cpan/podlators/scripts/pod2man /tools/bin
mkdir -pv /tools/lib/perl5/5.28.1
cp -Rv lib/* /tools/lib/perl5/5.28.1
```

Detalles sobre este paquete están en la Sección 6.40.2 “Contenidos de Perl”.



### 5.30 Python-3.7.2

El paquete Python 3 contiene el entorno de desarrollo de Python. Este es util para la programación orientada a objetos, escritura de scripts, y prototipar programas o desarrollo de aplicaciones.


**Tiempo aproximado de construcción** 1.5 SBU

**Espacio requerido en el disco:** 371 MB


#### 5.30.1 Instalación de Python

Este paquete primero construye el intérprete de Python, y luego algunos módulos estándar de Python. El script principal para construir módulos está escrito en Python, y usa direcciones fijas al sistema anfitrión en los directorios:  /usr/include y /usr/lib. Para evitar este uso, usar: 


```bash
sed -i '/def add_multiarch_paths/a \        return' setup.py
```

Se prepara Python para la compilación:


```bash
./configure --prefix=/tools --without-ensurepip
```

El significado de las opciones de configuración:


--without-ensurepip

Esta opción desactiva el instalador de Python, que no es necesario en este punto.


Para construir el paquete:

```bash
make
```

La compilación está completa ahora. Los test requieren Tk y X Windows y no pueden ser ejecutados en este punto.


Se instala el paquete

```bash
make install
``` 

Detalles sobre este paquete están en la Sección 6.51.2 “Contenidos de Python 3”.


### 5.31 Sed-4.7

El paquete Sed contiene un editor cadenas de caracteres.


**Tiempo aproximado de construcción** 0.2 SBU

**Espacio requerido en el disco:** 20 MB


#### 5.31.1 Instalación de Sed

Se prepara Sed para la compilación:


```bash
./configure --prefix=/tools
```

Se compila el paquete

```bash
make
```

La compilación está finalizada.Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utiliza el siguiente comando:

```bash
make check
```

Para instalar el paquete:

```bash
make install
```

Más detalles sobre este paquete, en la Sección 6.28.2 “Contenidos de Sed”.

### 5.32 Tar-1.31
 
El paquete Tar contiene un programa para archivos.


**Tiempo aproximado de construcción** 0.3 SBU

**Espacio requerido en el disco:** 38 MB


#### 5.32.1 Instalación de Tar

Se prepara Tar para la compilación:

```bash
./configure --prefix=/tools
```

Se compila el paquete

```bash
make
```

La compilación está finalizada.Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utiliza el siguiente comando:

```bash
make check
```

Para instalar el paquete:

```bash
make install
```

Más detalles sobre este paquete, en la Sección 6.69.2  “Contenidos de Tar”.


### 5.33 Texinfo-6.5
 
El paquete Texinfo contiene programas para escribir, leer y convertir páginas de información.

**Tiempo aproximado de construcción** 0.3 SBU

**Espacio requerido en el disco:** 104 MB


#### 5.33.1 Instalación de Texinfo

Se prepara Tar para la compilación:


```bash
./configure --prefix=/tools
```

NOTA: Como parte del proceso de configuración, se ejecuta un test que indica un error en TestXS_la-TestXS.lo. Este no es relevante para LFS y se ha de ignorar.

Se compila el paquete

```bash
make
```

se instala el paquete:

```bash
make install
```

Más detalles sobre este paquete, en la Sección 6.70.2  “Contenidos de Texinfo”.




### 5.34 Xz-5.2.4
 
El paquete Xz contiene programas para comprimir y descomprimir archivos. Esto proporciona la capacidad para usar los nuevos formatos de compresión lzma y xz. La compresión de archivos de texto con xz presenta un porcentaje de compresión mejor que con los comandos tradicionales gzip o bzip2.


**Tiempo aproximado de construcción** 0.2 SBU

**Espacio requerido en el disco:** 18 MB


#### 5.34.1 Instalación de Texinfo

Se prepara Xz para la compilación:


```bash
./configure --prefix=/tools
```

Se compila el paquete

```bash
make
```


La compilación está finalizada.Como se mencionó anteriormente los tests no son obligatorios para las herramientas temporales de este capítulo. Pero para ejecutarlos se utiliza el siguiente comando:

```bash
make check
```


se instala el paquete:

```bash
make install
```

Más detalles sobre este paquete, en la Sección 6.45.2  “Contenidos de Xz”.





### 5.35 Stripping

 Los pasos en esta sección son opcionales, pero si la partición LFS es pequeña es útil conocer de qué elementos se puede prescindir. Los ejecutables y librerías construidos hasta ahora contienen hasta 70 MB de símbolos de debug que no son necesarios. Se pueden quitar estos simbolos con:


```bash
strip --strip-debug /tools/lib/*
/usr/bin/strip --strip-unneeded /tools/{,s}bin/*
```

Estos comandos se saltaran un cierto número de archivos, reportando que no reconoce el tipo de archivo. La mayor parte de estos archivos son binarios. También se usa el comando del sistema strip para incluir el binario en /tools. Hay que tener cuidado de no usar --strip-unneeded en las librerías. Las librerías estáticas pueden destruirse y los paquetes de la cadena de herramientas necesitarán ser construidos de nuevo.

Para salvar más espacio, se puede quitar la documentación:

```bash
rm -rf /tools/{,share}/{info,man,doc}
```

Y se eliminan los archivos innecesarios:

```bash
find /tools/{lib,libexec} -name \*.la -delete
```

En este punto, se deberían tener al menos 3 GB de espacio en $LFS que puedan ser usados para construir e instalar Glibc y Gcc en la siguiente etapa. Si  puede compilar e instalar Glibc, se puede compilar e instalar el resto también.



### 5.36 Cambiando la pertenencia.


NOTA: Los comandos en el resto de este libro se han de realizar, mientras se está identificado como usuario root y no como usuario lfs. También se recomienda verificar que la variable de entorno $LFS está apuntando al entorno de root.

Actualmente, el directorio $LFS/tools es propiedad del usuario lfs, un usuario que solo existe en el sistema anfitrión. Si el directorio $LFS	/tools se mantuviera como esta, sus archivos pertenecerian a un usuario con una ID de usuario que no tendría su cuenta correspondiente. Esto es arriesgado, ya que posteriormente se podría crear un cuenta con esta ID de usuario, y éste,  entonces sería el dueño del directorio y todos sus archivos, pudiendo exponerlos con fines maliciosos.

Para evitar esta situación se puede añadir el usuario lfs, al nuevo sistema LFS después cuando se crea el archivo /etc/passwd, teniendo la precaución de asignar el mismo usuario y grupo ID que en el sistema anfitrión. Mejor aún, cambiar la propiedad del directorio $LFS/tools al usuario root mediante el siguiente comando: 

```bash
chown -R root:root $LFS/tools
```

Aunque el directorio $LFS/tools puede ser borrado una vez que el sistema LFS ha sido terminado, puede mantenerse para montar sistemas LFS adicionales, de la misma versión de este texto. Aunque la manera de hacer una copia de $LFS/tools es una preferencia personal.


ATENCIÓN: Si se intenta mantener de forma temporal las herramientas para construir otros sistemas LFS, ahora es el momento de hacer un back up. Los comandos del capítulo 6 modificarán las herramientas en este punto, y las dejaran inoperativas para posteriores construcciones.

# II. Construyendo el sistema LFS.

## instalando el sistema de software  básico.

### Introducción

En este capítulo, se comenzará a construir el sistema LFS. Asi, se hara un chroot (cambio de la raíz del sistema) a un mini-sistema linux temporal, se harán unas pequeñas últimas preparaciones y se comenzarán a instalar paquetes.

La instalación del software es directa. Aunque en muchos casos las instrucciones de instalación pudieran ser más cortas o más genéricas, se ha optado por presentar las instrucciones completas para cada paquete para minimizar las posibilidades de errores. La llave a conocer lo que hace a un sistema linux es conocer cada paquete que es usado y para que se usa.

No se recomienda usar optimizaciones. Estas pueden hacer que un programa se ejecute ligeramente más rápido, pero puede también ser causa de problemas o complicaciones en la compilación o cuando se ejecuta un programa. Si un paquete no compila cuando se usen optimizaciones, hay que tratar de compilarlo sin la optimización y ver si eso arregla el problema. Incluso si el paquete no compila cuando se usa la optimización también hay el riesgo de que se haya compilado incorrectamente debido a la complejidad de las interacciones entre el código y las herramientas de construcción. Apuntar también que las opciones -march y -mtune con opciones que no se indiquen en este texto no han sido verificadas. Esto puede generar pequeños problemas en la cadena de herramientas (Binutils, GCC y Glibc). Las posibles ganancias potenciales conseguidas usando las optimizaciones del compilador son normalmente muy sobrepasadas por los riesgos que implican. Aquellos que construyan por primera vez el sistema LFS, son animados a montar el sistema sin optimizaciones. Los sistemas creados, de todas formas se ejecutarán rápido y serán estables a la vez.

El orden en el que se instalarán los paquetes en este capítulo necesita ser seguido rigurosamente para asegurar que no hay ningún programa que accidentalmente requiera una ruta que apunte a /tools y que esté definida explícitamente así. Por la misma razón, no se han de compilar paquetes distintos en paralelo. Compilar en paralelo puede ahorrar tiempo (especialmente en sistemas de varios núcleos), pero puede resultar en un programa que tenga una ruta directa a /tools , y no relativa, lo que causaría que el programa no funcionase cuando se elimine ese directorio.

Antes de las instrucciones de instalación, cada página de instalación presenta información sobre cada paquete, incluyendo una descripción concisa de que contiene, y cuanto tiempo se necesita para construir aproximadamente. Siguiendo a las instrucciones de instalación, hay una lista de programas y librerías (junto con una breve descripción de estas) sobre que instala cada paquete.

AVISO: Los valores SBU y el espacio de disco requerido, incluyen los test para todos los paquetes del capítulo 6.

En los procesos del capítulo 6, se elimina o deshabilita la instalación de la mayoría de las librerías estáticas. Normalmente esto se realiza usando la opción --disable-static en “toconfigure”. En otros casos, métodos alternativos son necesarios. En unas pocas ocasiones, especialmente en glib y gcc, el uso de librerías estáticas, se mantiene esencial a la forma general de la construcción del paquete.

Para una discusión más en detalle de las librerías se puede leer el capítulo  Libraries: Static or shared? del libro BLFS.


#### 6.2 Preparando el sistema de archivos virtual del kernel.

Varios sistemas de archivos serán exportados por el kernel y se usaran para comunicar con el kernel. Estos sistemas de archivos son virtuales y no requieren de espacio de disco para ellos.  El contenido de los sistemas de archivos reside en memoria. Se comienza creado los directorios en los cuales los sistemas de archivos se montarán:


Se comienza creando los directorios en los que se montará el sistema: 

```bash
mkdir -pv $LFS/{dev,proc,sys,run}
```

#### 6.2.1 Creando los nodos de dispositivos iniciales

Cuando el kernel inicia el sistema, este necesita la presencia de algunos nodos de dispositivos, en particular el de 'console' y 'null'. Los nodos de los dispositivos han de ser creados en el disco duro, de tal manera que esten disponibles antes que `udevd` haya comenzado, y cuando Linux haya comenzado con: `init=/bin/bash`


Se crean los dispositivos con los siguientes comandos:

```bash
mknod -m 600 $LFS/dev/console c 5 1
mknod -m 666 $LFS/dev/null c 1 3
```

#### 6.2.2 Montado y populando /dev

El método recomendado para popular el directorio /dev con dispositivos es montar un sistema de archivos virtual (como tmpfs) en el directorio /dev, y permitir que los dispositivos sean creados dinámicamente en ese sistema de archivos virtuales a medida que sean detectados o se acceda a ellos. La creación de dispositivos se realiza normalmente durante el proceso de arranque por Udev. Ya que el nuevo sistema no tienen todavía Udev y no ha sido iniciado todavía, es necesario montar y popular /dev manualmente. Esto se hace mediante un ‘bind mounting’ del sistema anfitrión al directorio /dev. Un ‘bind mount’ es un tipo de montura de archivos especial que permite crear el espejo de un directorio en otra localización. Para ello se usa el siguiente comando: 

```bash
mount -v --bind /dev $LFS/dev
```

#### 6.2.3 Montando el sistema de archivos virtual del kernel

A continuación se montan el resto de archivos del sistema virtual del kernel.

```bash
mount -vt devpts devpts $LFS/dev/pts -o gid=5,mode=620
mount -vt proc proc $LFS/proc
mount -vt sysfs sysfs $LFS/sys
mount -vt tmpfs tmpfs $LFS/run
```

Significado de las opciones para montar devpts

gid=5

Esto asegura que todos los nodos de dispositivos creados en devpts tienen como propietario el grupo ID 5. Este es el ID que se usará después para el grupo tty. Se usa el ID de este grupo, en vez de un nombre ya que el sistema anfitrión puede usar un ID distinto para el grupo tty.
 
mode=0620

Eso asegura que todos los nodos de dispositivos creados tienen el modo 0620 (usuario puede escribir y leer, y el grupo puede escribir). Junto con las opciones anteriores, esto asegura que los nodos de dispositivo que cumplen los requerimientos de grantpt(), esto es el binario auxiliar del Glibc **pt_chown** que no está instalado por defecto, no es necesario


En algunos sistemas anfitriones, /dev/shm es un link simbólico a /run/shm. El /run/tmpfs fue montado sobre el, así que en este caso solo será necesario crear un directorio.


```bash
if [ -h $LFS/dev/shm ]; then
  mkdir -pv $LFS/$(readlink $LFS/dev/shm)
fi
```


### 6.3 Gestor de paquetes.

Una petición común al este texto es el gestor de paquetes. Un gestor de paquetes permite llevar la cuenta de los archivos instalados haciendo sencillo quitarlos y actualizar los paquetes de software. Tanto con binarios como con librerías, un gestor de paquetes gestionaría la instalación de los archivos de configuración. Antes de entrar en el tema: NO  -- está sección no trata ni recomienda ningún gestor de paquetes en particular. En lo que se centra en en presentar un aproximación a las técnicas más comunes de cómo funcionan. El gestor de paquetes ideal para usted, puede estar entre estas técnicas o puede ser una combinación de dos o más de estas técnicas. Esta sección brevemente menciona ocurrencias que puedan presentarse al actualizar paquetes.

Algunas de las razones por las que no se presenta un gestor de paquetes en este texto, son:

La gestión de paquetes de software desvía el centro de atención del objetivo de este libro -- mostrar cómo se construye un sistema linux.
Hay varias soluciones para la gestión de paquetes, cada una teniendo sus ventajas e inconvenientes. Presentar una de ellas que satisfaga a todas las audiencias es difícil.

Sobre este tema hay algunos consejos en la sección de Consejos del proyecto.


### 6.3.1 Incidencias al actualizar

Un gestor de paquetes hace sencillo actualizar a versiones más nuevas cuando estas son liberadas. Generalmente las instrucciones en este texto,  LFS  y en BLFS, pueden ser usadas para actualizar a versiones más nuevas. Se presentan a continuación algunos puntos a tener en cuenta cuando se actualicen paquetes, especialmente en un sistema en ejecución.

 - Si Glib necesita ser actualizada a una nueva versión (por ejemplo de glibc-2.16 a glibc-2.20), es más seguro reconstruir el sistema LFS. Aunque se puede ser capaz de reconstruir todos los paquetes según su orden de dependencia, no se recomienda esto.
 - Si un paquete contiene una librería compartida y es actualizada, y el nombre de la librería cambia, entonces todos los paquetes dinámicamente enlazados a la librería necesitan ser re-compilados y enlazados con la nueva librería. (notar que  no hay correlación entre la versión del paquete  y el nombre de la librería). Por ejemplo, consideremos un paquete de nombre foo-1.2.3 que instala una librería compartida con el nombre libfoo.so.1. Puede ser que si se actualiza el paquete a una nueva versión foo-1.2.4 que instale una librería compartida con el nombre libfoo.so.2. En este caso, todos los paquetes que sean dinámicamente enlazados a la librería.so.1 necesitarian ser re-compilados para enlazarlos con libfoo.so.2. Apuntar que no se han de quitar las librerías anteriores hasta que los paquetes hayan sido re-compilados.


### 6.3.2 Técnicas de gestión de paquetes.

A continuación se presentan algunas técnicas comunes para la gestión de paquetes software. Antes de hacer ninguna decisión sobre un gestor de paquetes, es mejor hacer un estudio de las distintas técnicas y particularmente de las sus desventajas. 


#### 6.3.2.1  !Está todo en mi cabeza!

Si, esto es un tipo de gestión de paquetes de software. Algunos individuos no consideran necesario un gestor de paquetes, ya que ellos conocen y saben qué archivos tiene cada paquete. Algunos usuarios tampoco necesitan ningún gestor de paquetes porque planean reconstruir todo el sistema cada vez que cambie un paquete.

#### 6.3.2.2  Instalación en directorios separados

Esta es una forma sencilla que no necesita instalar ningún gestor de paquetes para las instalaciones. Cada paquete se instala en un directorio separado. Por ejemplo, el paquete foo-1.1 se instala en /usr/pkg/foo-1.1 y un enlace simbólico (symlink) se crea desde /usr/pkg/foo hacia /usr/pkg/foo-1.1. Cuando se instale la nueva versión foo-1.2, se instalará en /usr/pkg/foo-1.2 y previo enlace simbólico apuntará a esta nueva versión.

Las variables de entorno como PATH, LD_LIBRARY_PATH, MANPATH, INFOPATH y CPPFLAGS necesitarán ser actualizadas para incluir /usr/pkg/foo. Cuando se quieren gestionar más de algunos paquetes, sin embargo esta estrategia se vuelve inmanejable.


#### 6.3.2.3 Gestión de paquetes con enlaces simbólicos


Esta estrategia es una variación de la estrategia anterior. Cada paquete se instala de igual modo que el esquema anterior. Pero en vez de crear un link simbólico, cada archivo es enlazado con la jerarquía en /usr. Esto elimina la necesidad de incrementar las variables de entorno.  Aunque los enlaces simbólicos pueden ser creados por el usuario, hay muchos gestores de paquetes que han sido escritos usando esta estrategia, algunos de ellos son: Sow, Epkg, Graft y Depot.

La instalación necesita ser simulada, para que el paquete piense que está instalado en /usr mientras que en realidad está instalado en /usr/pkg. Este tipo de instalación no es algo trivial. Por ejemplo, si se considera que se quiere instalar el paquete libfoo-1.1 Las siguientes instrucciones pueden no instalar el paquete correctamente: 
```bash
./configure --prefix=/usr/pkg/libfoo/1.1
make
make install
```
La instalación funcionará, pero los paquetes dependientes pueden no enlazar con libfoo cómo se está esperando. Si se compila un paquete que enlace contra libfoo, se notará que está enlazado con /usr/pkg/libfoo/1.1/libfoo.so.1 en vez de contra /usr/lib/libfoo.so.1 como se esperaría. El modo correcto es usar la estrategia DESTDIR para simular la instalación del paquete. Esta estrategia consiste en lo siguiente.

```bash
./configure --prefix=/usr
make
make DESTDIR=/usr/pkg/libfoo/1.1 install
```

La mayoría de paquetes funcionarán con esta estrategia, pero algunos no lo harán. Para los paquetes que no cumplan, puede que se requiera instalarlos manualmente, o se puede ser más sencillo instalarlos en /opt.


#### 6.3.2.4 En función del tiempo

Con está técnica, a cada archivo se le ponen un sello de tiempo antes de instalar el paquete. Después de la instalación, simplemente usando el comando ‘find’ con las opciones apropiadas se puede generar un registro de todos los archivos instalados después de que fue creado el sello de tiempo del archivo. Un gestor de paquetes diseñado siguiendo esta estrategia es install-log.

Aunque esta estrategia presenta la ventaja de ser simple, tienen dos inconvenientes principales. Si durante la instalación, los archivos se instalan con cualquier otro tiempo que el tiempo actual, dichos archivos no serán identificados por el gestor de paquetes. También, en esta estrategia solo se puede usar cuando se instala un único paquete. Los registros no son fiables si dos paquetes se instalan por medio de dos consolas distintas.


#### 6.3.2.5 Trazando los scripts the instalación

En esta estrategia, los comandos que ejecutan los scripts de instalación son grabados. Hay dos técnicas que uno puede usar: 

La variable de entorno LD_PRELOAD puede definirse para apuntar a una librería que se precargara antes de una instalación. Durante la instalación, esta librería apunta los paquetes que han sido instalados, ya que se incluye a sí misma en varios ejecutables, como ‘cp’, ‘install’, ‘mv’ y apunta las llamadas al sistema que modifican el sistema de archivos. Para que esta estrategia funcione, todos los ejecutables necesitan ser enlazados dinámicamente sin suid o el bit sgid. La precargas de la librería puede causar algunos efectos laterales no deseados durante la instalación. Así pues se recomienda que se ejecuten algunos test para asegurar que el gestor de paquetes no rompe nada y registra todos los archivos necesarios.

La segunda técnica es usar ‘strace’, que registra todas las llamadas al sistema hechas durante la ejecución de los scripts de instalación. 

#### 6.3.2.6 Creando archivos de paquetes

En esta estrategia, la instalación del paquete se finge en un árbol de directorios separado, tal y como se describió en la gestión con enlaces simbólicos. Después de la instalación, se crea un paquete de archivos usando los archivos instalados. Este archivo entonces se usa para instalar el paquete o bien en el sistema local o también en otros sistemas.

Esta estrategia se usa en la mayoría de gestores de paquetes presentes en las distribuciones comerciales. Ejemplos de los gestores de paquetes que siguen esta estrategia son RPM (el cual, se requiere por Linux Standard Base Specification), pkg-utils, Debian's apt, y Gentoo's Portage system. 
Una pista sobre cómo adoptar este estilo de gestión de paquetes por sistemas LFS se encuentra en: http://www.linuxfromscratch.org/hints/downloads/files/fakeroot.txt


La creación de paquetes de archivos que incluyan información sobre dependencias es compleja y va más allá del objetivo de LFS.


Slackware usa una estrategia basada en `tar` con paquetes de archivos. Este sistema de forma intencionada no gestiona dependencias más complejas que las que podría gestionar un gestor de paquetes. Para detalles sobre cómo funciona el gestor de paquetes Slackware, ver: http://www.slackbook.org/html/package-management.html.

#### 6.3.2.7 Gestión del usuario

Esta estrategia, única de LFS, fue ideada por Matthias Benkmann, y está disponible en el  Hints Project. En este esquema, cada paquete se instala como un usuario distinto en las localizaciones estándar. Los archivos que pertenezcan a un paquete son fácilmente identificados comprobando la identidad del usuario. Los ventaja e inconvenientes de esta estrategia, son complicados para describirlos en esta sección, para más detalles ver: http://www.linuxfromscratch.org/hints/downloads/files/more_control_and_pkg_man.txt.


#### 6.3.3 Desplegando LFS en múltiples sistemas


Una de las ventajas de un sistema LFS es que no hay archivos que dependan en la posición de archivos en el sistema de memoria. Clonar un sistema LFS y construirlo en otra computadora con la misma arquitectura que el sistema base, es tan sencillo como usar `tar` en la partición LFS que contiene el directorio raíz ( aproximadamente unos 250 MB descomprimidos para la base de LFS), copiar ese archivo vía una red o CD-ROM al nuevo sistema . En ese punto algunos archivos de configuración han de ser modificados. Los archivos de configuración que pueden ser actualizados: /etc/hosts, /etc/fstab, /etc/passwd, /etc/group, /etc/shadow, /etc/ld.so.conf, /etc/sysconfig/rc.site, /etc/sysconfig/network, and /etc/sysconfig/ifconfig.eth0.

Un kernel modificado podría necesitar ser construido para el nuevo sistema dependiendo sobre distinto hardware y la configuración original del kernel. 

NOTA:
Ha habido notificaciones de problemas cuando se copia entre arquitecturas parecidas pero no idénticas. Por ejemplo, el juego de instrucciones para una arquitectura Intel, no es igual que para un procesador AMD y versiones de algunos procesadores pueden tener instrucciones que no estén disponibles en versiones anteriores. 

Por último el nuevo sistema ha de poderse lanzar, en la Sección 8.4 “Usando GRUB para lanzar el proceso de Inicio”

## 6.4 Entrando en el entorno de chroot.

Es hora de entrar en el entorno de chroot para comenzar a construir e instalar el sistema LFS final. Como el usuario `root` se ejecuta el siguiente comando para entrar en el entorno de chroot que por el momento solo contiene herramientas temporales:


```bash
chroot "$LFS" /tools/bin/env -i \
    HOME=/root                  \
    TERM="$TERM"                \
    PS1='(lfs chroot) \u:\w\$ ' \
    PATH=/bin:/usr/bin:/sbin:/usr/sbin:/tools/bin \
    /tools/bin/bash --login +h
```
La opción -i permite al comando `env` que limpie todas las variables del entorno de chroot. Después de eso, únicamente las variables HOME, TERM, PS1 y PATH se definen de nuevo. La construcción TERM=$TERM definirá la variable TERM con el mismo valor dentro que fuera de chroot. Esta variable se necesita para programas como ‘vim’ y ‘less’ para que funcione correctamente. Si otras variables son necesarias, como CFLAGS o CXXFLAGS, este es un buen lugar para definirlas de nuevo. 

A partir de este punto, no hay necesidad de usar la variable LFS más, ya que todo el trabajo estará restringido al sistema LFS. Esto es debido a que la consola considera que $LFS es el directorio raíz (/root)

Téngase en cuenta que /toos/bin aparece el último en el PATH. Esto significa que una herramienta temporal no necesitará volver a ser usada una vez su versión final sea instalada. Esto pasará cuando la consola no pueda “recordar” la ruta a los binarios ejecutados, por esa razón el hashing se desactiva cuando se le pasa la opción ‘+h’ a la consola.

Notar también, que la consola nos informa que no tiene nombre! Esto es de esperar ya que el archivo /etc/passwd no ha sido creado todavía.

NOTA: Es importante que los comandos durante el resto de este capítulo y los siguientes capítulos se ejecuten en el entorno de chroot. Si se abandona este entorno por alguna razón (como por ejemplo al reiniciar el sistema), hay que asegurarse que el sistema de archivos del kernel virtual sea montado como se explica en la sección 6.2.2 “Montando y populando /dev” y la sección 6.2.3 “Montando el sistema de archivos virtual del kernel” y volver a entrar en el entorno chroot de nuevo antes de comenzar la instalación. 

## 6.5 Creación de directorios

Ahora se creará una estructura en el sistema de archivos de LFS. Se creará el árbol de directorios estándar, mediante los siguientes comandos.


```bash
mkdir -pv /{bin,boot,etc/{opt,sysconfig},home,lib/firmware,mnt,opt}
mkdir -pv /{media/{floppy,cdrom},sbin,srv,var}
install -dv -m 0750 /root
install -dv -m 1777 /tmp /var/tmp
mkdir -pv /usr/{,local/}{bin,include,lib,sbin,src}
mkdir -pv /usr/{,local/}share/{color,dict,doc,info,locale,man}
mkdir -v  /usr/{,local/}share/{misc,terminfo,zoneinfo}
mkdir -v  /usr/libexec
mkdir -pv /usr/{,local/}share/man/man{1..8}
mkdir -v  /usr/lib/pkgconfig

case $(uname -m) in
 x86_64) mkdir -v /lib64 ;;
esac

mkdir -v /var/{log,mail,spool}
ln -sv /run /var/run
ln -sv /run/lock /var/lock
mkdir -pv /var/{opt,cache,lib/{color,misc,locate},local}
```

Los directorios se crean por defecto con los permisos 755, pero esto no es deseable para todos los directorios. En los comandos anteriores, se hacen dos cambios -uno en el directorio base del usuario ‘root’, y otro al directorio de los archivos temporales. 

El primer cambio asegura que nadie pueda entrar al directorio /root - lo mismo que un usuario normal haría con su propio directorio. El segundo modo hace  que cualquier usuario pueda escribir en los directorios /tmp y /var/tmp, pero no pueda borrar los archivos de otros usuarios. Lo último es prohibido por el llamado “sticky bit”, el bit (1) en la mascara 1777. 

### 6.5.1 Nota sobre el cumplimiento de la jerarquía de archivos estándar 

El árbol de directorios está basado en la  jerarquía de archivos estándar (FHS), (disponible en https://refspecs.linuxfoundation.org/fhs.shtml). El FHS también especifica la existencia opcional de algunos directorios como /usr/local/games y /usr/share/games. Únicamente se han creado los directorios necesarios. Pero se pueden crear los directorios que se quieran.


## 6.6 Creando los archivos esenciales y enlaces simbólicos.

Algunos programas usan rutas absolutas a programas que todavía no existen. Para satisfacer a dichos programas, se crean enlaces simbólicos que serán reemplazados por los archivos de verdad a lo largo del capítulo, cuando el software haya sido instalado. 


```bash
ln -sv /tools/bin/{bash,cat,chmod,dd,echo,ln,mkdir,pwd,rm,stty,touch} /bin
ln -sv /tools/bin/{env,install,perl,printf}         /usr/bin
ln -sv /tools/lib/libgcc_s.so{,.1}                  /usr/lib
ln -sv /tools/lib/libstdc++.{a,so{,.6}}             /usr/lib

ln -sv bash /bin/sh
```

El propósito de cada enlace:


/bin/bash
Muchos scripts, necesitan este enlace

/bin/cat
La ruta está definida explícitamente en el script de configuración de Glibc

/bin/dd
La ruta está definida explícitamente en el script de configuración de la utilidad /usr/bin/libtool

/bin/echo
Para uno de los test de los test de Glibc que espera /bin/echo

/usr/bin/env
Esta ruta está definida explícitamente en algunos de los procesos de construcción de algunos paquetes. 

/usr/bin/install
El path para install está definido explícitamente en /usr/lib/bash/Makefile.inc

/bin/ln
Esta ruta está definida explícitamente en /usr/lib/perl5/5.30.0/<target-triplet>/Config_heavy.pl 

/bin/pwd
Algunos scritps de configuración, particularmente Glibc, tienen esta ruta  definida explícitamente. 

/bin/rm
La ruta para rm está definida explícitamente en /usr/lib/perl5/5.30.0/<target-triplet>/Config_heavy.pl

/bin/stty
Esta ruta está definida explícitamente en Expect, y es necesaria para que pasen los test de Binutils y GCC.

/usr/bin/perl
Muchos scripts the perl, necesitan este enlace.

/usr/lib/libgcc_s.so{,.1}
Glib necesita esta ruta para que funcione la librería pthreads. 

/usr/lib/libstdc++{,.6}
Varios test de Glibc, necesitan este enlace, así como el soporte de C++ en GMP. 

/bin/sh
Muchos scripts, necesitan este enlace

Historicamente, Linux mantiene una lista de los sistemas de archivos montados en el directorio /etc/mtab. Kernels modernos mantiene esta lista internamente y la exponen en el directorio /proc. Para satisfacer aquellos programas que esperan la presencia de /etc/mtab, se crea el siguiente enlace simbólico: 


```bash
ln -sv /proc/self/mounts /etc/mtab
```
Para que el usuario raíz (root) pueda hacer login, y para que el nombre “root” sea reconocido, deben existir sus correspondientes entradas en los archivos /etc/passwd y /etc/group.

Se crea el archivo /etc/passwd ejecutando el comando:

```bash
cat > /etc/passwd << "EOF"
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/dev/null:/bin/false
daemon:x:6:6:Daemon User:/dev/null:/bin/false
messagebus:x:18:18:D-Bus Message Daemon User:/var/run/dbus:/bin/false
nobody:x:99:99:Unprivileged User:/dev/null:/bin/false
EOF
```
Se crea el archivo /etc/group con el siguiente comando:

```bash
cat > /etc/group << "EOF"
root:x:0:
bin:x:1:daemon
sys:x:2:
kmem:x:3:
tape:x:4:
tty:x:5:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
messagebus:x:18:
input:x:24:
mail:x:34:
kvm:x:61:
wheel:x:97:
nogroup:x:99:
users:x:999:
EOF
```

Los grupos creados no son parte de ningún estándar - son los grupos que se crean en parte por los requerimiento de la configuración Udev en este capítulo y en parte por una convención común usada por algunas distribuciones de Linux. Además algunos test dependen en usuarios o grupos específicos. La base estandar de linux (LSB - Linux Standard Base, en inglés. Disponible  en  http://www.linuxbase.org) únicamente recomienda que además del grupo root con la ID de grupo (GID) de 0, haya un grupo `bin` con GID de 1. Todos los demás nombres de grupos y GIDs pueden escogerse libremente por el administrador del sistema ya que los programas no depende de los números de GID, sinó más bien el los nombres del grupo.

Para quitar el “I have no name!” que aparece al abrir un terminal. Ya que desde que se instaló Glibc en el capítulo 5 y los archivos /etc/passwd y /etc/group han sido creados, la resolución del usuario y nombre del grupo, funcionará: 

```bash
exec /tools/bin/bash --login +h
```

Notar el uso de la directiva +h. Esto indica a ‘bash’ que no use su hash interno. Sin esta directiva, ‘bash’ recordaría las rutas a los directorios que ha ejecutado. Para asegurar que se usan los nuevos binarios compilados tan pronto como se instalan. La directiva +h se usará por el resto de este capítulo. 

Los programas ‘login’, ‘agetty’  e ‘init’ (y otros) usan archivos para grabar información como quién ha entrado al sistema y cuando. De todas formas estos programas no escribirán en estos archivos si estos no existen previamente. Hay que inicializar estos archivos y darles los permisos correspondientes. 

```bash
touch /var/log/{btmp,lastlog,faillog,wtmp}
chgrp -v utmp /var/log/lastlog
chmod -v 664  /var/log/lastlog
chmod -v 600  /var/log/btmp
```
El archivo /var/log/wtmp apunta todos los login y logouts. El archivo /var/log/lastlog apunta la última vez que entro cada usuario. El archivo /var/log/faillog apunta todos los intentos fallidos de login. El archivo /var/log/btmp apunta los intentos incorrectos de login

NOTA: el archivo /run/utmp apunta los usuarios que están actualmente en el sistema. Este archivo es creado dinámicamente en los scripts de arranque. 



## 6.7 Cabeceras API Linux-5.2.8 

La cabecera API para Linux (en linux-5.2.8.tar.xz) expone la API del kernel para el uso de Glibc.

**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:** 960 MB


### 6.7.1 Instalación de API de las cabeceras de Linux.

El kernel de linux necesita exponer su interfaz de aplicación (API) para que la librería de C (Glibc en LFS) lo pueda usar. Esto se hace adecuando varias cabeceras de C que están en el archivo tarball del kernel de Linux.

Para asegurarse de que no hay archivos pendientes o dependencias de actividades previas:


```bash
make mrproper
```

Ahora se extraen las cabeceras del kernel disponibles para el usuario. EStas se guardan en un directorio intermedio y se copian a las rutas necesarias. Ya que el proceso de extracción elimina los archivos existentes en el directorio final. Hay también algunos archivos ocultos que son usados por los desarrolladores del kernel y no son necesarios para LFS y que eliminarán del directorio intermedio.


```bash
make INSTALL_HDR_PATH=dest headers_install
find dest/include \( -name .install -o -name ..install.cmd \) -delete
cp -rv dest/include/* /usr/include
```

### 6.7.2 Contenidos de las cabeceras de Linux

- Cabeceras instaladas: /usr/include/asm/*.h, /usr/include/asm-generic/*.h, /usr/include/drm/*.h, /usr/include/linux/*.h, /usr/include/misc/*.h, /usr/include/mtd/*.h, /usr/include/rdma/*.h, /usr/include/scsi/*.h, /usr/include/sound/*.h, /usr/include/video/*.h, and /usr/include/xen/*.h

- Directorios instalados: /usr/include/asm, /usr/include/asm-generic, /usr/include/drm, /usr/include/linux, /usr/include/misc, /usr/include/mtd, /usr/include/rdma, /usr/include/scsi, /usr/include/sound, /usr/include/video, and /usr/include/xen

Descripciones breves:

/usr/include/asm/*.h <br>    Las cabeceras de Linux API ASM H

/usr/include/asm-generic/*.h   <br>    Cabeceras genéricas de Linux API ASM 

/usr/include/drm/*.h  <br>     Cabeceras de DRM Linux API 

/usr/include/linux/*.h  <br>   Las cabeceras de Linux API 

/usr/include/misc/*.h   <br>   Cabeceras de misceláneos de Linux API 

/usr/include/mtd/*.h   <br>    Cabeceras de Linux API MTD Headers

/usr/include/rdma/*.h  <br>    Cabeceras de API RDMA Headers

/usr/include/scsi/*.h   <br>   Cabeceras de Linux API SCSI

/usr/include/sound/*.h  <br>   Cabeceras de Linux API Sound

/usr/include/video/*.h  <br>   Cabeceras de Linux API Video 

/usr/include/xen/*.h   <br>    Cabeceras de Linux API Xen




## 6.8 Páginas Man 5.02

El paquete de las páginas del manual contiene más de 2200 páginas.


**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:** 31 MB



### 6.8.1 Instalación de las páginas Man 
Se instalan ejecutando:

```bash
make install
```

### 6.8.2 Contenido de las páginas Man

**Archivos instalados** varias páginas Man

**Descripción breve** 

Man pages:  Descripción de funciones de programación en C, archivos de dispositivos y archivos de configuración relevantes.


## 6.9 Glibc- 2.30

El paquete Glibc contiene la librería principal de C. Esta librería provee de las rutinas básicas para reservar memoria, búsqueda de directorios, abrir, cerrar, leer o escribir archivos, gestión de cadenas de caracteres, identificación de patrones, aritmética ...

**Tiempo aproximado de construcción** : 21 SBU

**Espacio requerido en el disco:** 3.3 MB


### 6.9.1 Instalación de Glibc

NOTA: el sistema de construcción de Glib es auto contenido y se instalará perfectamente, incluso si las propiedades del compilador y enlazador todavía apuntan a /tools. Las propiedades del compilador y el enlazador no puedes ser ajustadas antes que sea instalado Glib ya que los test de autoconfiguración de Glibc darían un resultado negativo y no se alcanzaría el objetivo de una construcción limpia.

Alguno de los programas que usa Glibc no respetan la jerarquía de directorios estándar (FHS) y usan el directorio /var/db para guardar datos en tiempo de ejecución . Se aplica el siguiente parche para asegurarse que esos programas pueden guardar sus datos en tiempo de ejecución en directorios que respeten la jerarquía de archivos estándar.

```bash
patch -Np1 -i ../glibc-2.30-fhs-1.patch
```

Para corregir un problema que se introdujo el el linux kernel 5.2:

```bash
sed -i '/asm.socket.h/a# include <linux/sockios.h>' \
   sysdeps/unix/sysv/linux/bits/socket.h
```


Para crear un link simbólico para cumplir LSB. Además para la arquitectura  x86_64, se crea un link simbólico de compatibilidad que es necesario para el enlazador de librerías dinámicas pueda funcionar correctamente:
 
```bash
case $(uname -m) in
    i?86)   ln -sfv ld-linux.so.2 /lib/ld-lsb.so.3
    ;;
    x86_64) ln -sfv ../lib/ld-linux-x86-64.so.2 /lib64
            ln -sfv ../lib/ld-linux-x86-64.so.2 /lib64/ld-lsb-x86-64.so.3
    ;;
esac
```
La documentación de Glib, recomienda construir Glibc en un directorio dedicado:


```bash
mkdir -v build
cd  build
```

Se prepara Glibc para la compilación:

```bash
CC="gcc -ffile-prefix-map=/tools=/usr" \
../configure --prefix=/usr                          \
             --disable-werror                       \
             --enable-kernel=3.2                    \
             --enable-stack-protector=strong        \
             --with-headers=/usr/include            \
             libc_cv_slibdir=/lib
```

Significado de las opciones y de los nuevos parámetros de configuración:

CC="gcc -ffile-prefix-map=/tools=/usr" <br>
Hace que GCC grabe cualquier referencia a archivos en /tools como resultado de la compilación y si os archivos estan en /usr. Esto evita la introducción de rutas incorrectas en los símbolos de debug.

--disable-werror <br>
Esta opción deshabilita la opción -Werror que se pasa a GCC. Esto es necesario para ejecutar los tests.

--enable-stack-protector=strong <br>
Esta opción aumenta la seguridad del sistema añadiendo código extra para proteger la pila de ataques de overflow.


--with-headers=/usr/include <br>
Esta opción indica al sistema de compilación donde encontrar las cabeceras de la API del kernel. Por defecto estas cabeceras están en /tools/include.

libc_cv_slibdir=/lib
Esta variable define la librería correcta para todos los sistemas. No se quiere que se usa la librería lib64.


Para compilar el paquete:

```bash
make
```

**IMPORTANTE**:
En esta sección la verificación de los test de Glib es considerada crítica. No se ha de saltar esta parte bajo ninguna circunstancia.

Generalmente, algunos test no pasan. Los fallos enumerados a continuación es seguro ignorarlos 

```bash
case $(uname -m) in
  i?86)   ln -sfnv $PWD/elf/ld-linux.so.2        /lib ;;
  x86_64) ln -sfnv $PWD/elf/ld-linux-x86-64.so.2 /lib ;;
esac
```

**NOTA**:
El enlace simbólico (symlink) anterior es necesario para ejecutar los test en este punto de la construcción en en entorno chroot. Esto se sobreescribirá en la fase de instalación a continuación. 

```bash
make check
```

Puede que algunos test que no pasen. Los test de Glibc dependen en cierta medida del sistema anfitrión. A continuación hay una lista de los incidentes más frecuentes encontrados en algunas versiones de LFS.

misc/tst-ttyname  se sabe que falla en el entorno de chroot.
inet/tst-idna_name_classify   se sabe que falla en el entorno de chroot.
posix/tst-getaddrinfo4 and posix/tst-getaddrinfo5  puede fallar en algunas arquitecturas
nss/tst-nss-files-hosts-multi puede fallar por motivos no determinados
Los tests rt/tst-cputimer{1,2,3}, pueden fallar por el kernel de sistema anfitrión. Se sabe que fallan los kernels  4.14.91–4.14.96, 4.19.13–4.19.18 y  4.20.0–4.20.5
Los test matemáticos se saben que pueden fallar en procesadores Intel o AMD que no son muy actuales.


Aunque es un mensaje sin peligro. Durante la instalación de Glibc, se reportará la ausencia del archivo /etc/ld.so.conf. Se puede prevenir esto, con:

```bash
touch /etc/ld.so.conf
```

Para hacer que el Makefile salte una verificación de seguridad que falla en el entorno parcial de LFS:


```bash
sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile
```

Para instalar el paquete:

```bash
make install
```

Para instalar el archivo de configuración y el directorio para ‘nscd’:

```bash
cp -v ../nscd/nscd.conf /etc/nscd.conf
mkdir -pv /var/cache/nscd
```

A continuación se instalaran la configuración local que hará que el sistema responda en diferentes lenguajes. Ninguna localización es necesaria, pero si alguna de ellas falta algunos test de futuros paquetes pueden saltarse casos de test relevantes.

Los archivos de localización individuales se instalan usando el programa ‘localedef’. Por ejemplo el primer ‘localedef’ a continuación combina el /usr/share/i18n/locales/cs_CZ_charset-índependent definición local con el  mapa de caracteres /usr/share/i18n/charmaps/UTF-8.gz y añade el resultado al archivo /usr/lib/locale/locale-archive. Las instrucciones a continuación instalarán el grupo mínimo de locales necesario para una cobertura óptima de los test. 


```bash
mkdir -pv /usr/lib/locale
localedef -i POSIX -f UTF-8 C.UTF-8 2> /dev/null || true
localedef -i cs_CZ -f UTF-8 cs_CZ.UTF-8
localedef -i de_DE -f ISO-8859-1 de_DE
localedef -i de_DE@euro -f ISO-8859-15 de_DE@euro
localedef -i de_DE -f UTF-8 de_DE.UTF-8
localedef -i el_GR -f ISO-8859-7 el_GR
localedef -i en_GB -f UTF-8 en_GB.UTF-8
localedef -i en_HK -f ISO-8859-1 en_HK
localedef -i en_PH -f ISO-8859-1 en_PH
localedef -i en_US -f ISO-8859-1 en_US
localedef -i en_US -f UTF-8 en_US.UTF-8
localedef -i es_MX -f ISO-8859-1 es_MX
localedef -i fa_IR -f UTF-8 fa_IR
localedef -i fr_FR -f ISO-8859-1 fr_FR
localedef -i fr_FR@euro -f ISO-8859-15 fr_FR@euro
localedef -i fr_FR -f UTF-8 fr_FR.UTF-8
localedef -i it_IT -f ISO-8859-1 it_IT
localedef -i it_IT -f UTF-8 it_IT.UTF-8
localedef -i ja_JP -f EUC-JP ja_JP
localedef -i ja_JP -f SHIFT_JIS ja_JP.SIJS 2> /dev/null || true
localedef -i ja_JP -f UTF-8 ja_JP.UTF-8
localedef -i ru_RU -f KOI8-R ru_RU.KOI8-R
localedef -i ru_RU -f UTF-8 ru_RU.UTF-8
localedef -i tr_TR -f UTF-8 tr_TR.UTF-8
localedef -i zh_CN -f GB18030 zh_CN.GB18030
localedef -i zh_HK -f BIG5-HKSCS zh_HK.BIG5-HKSCS
```

Además hay que instalar el archivo de localización ‘locale’ para el país del usuario, lenguaje y mapa de caracteres. 

Alternativamente, se pueden instalar todos los archivos de localización en el archivo glibc-2.30/localedata/SUPPORTED (esto incluye cada archivo de localización listado anteriormente y muchos más archivos), con el siguiente comandos:

```bash
make localedata/install-locales
```

Cuando se use el comando ‘localdef’ se crea y se instala archivos locales que no están listado en el fichero gilbc-2.30/localedata/SUPPORTED  en improbable caso que sea necesario.


NOTA:
Glibc usa ‘libidn2’ cuando resuelve nombres de dominios internacionales. Esto es una dependencia en tiempo de ejecución. Si se necesita esta habilidad, las instrucciones para instalar ‘libidn2’ están en BLFS libidn2 page.



### 6.9.2 Configuración de Glibc

#### 6.9.2.1 Añadiendo nsswitch.conf

Se ha de crear el archivo /etc/nsswitch.conf ya que por defecto Glibc no trabaja bien en entornos de redes.

Se crea un nuevo archivo /etc/nsswitch.conf con el siguiente comando:

```bash
cat > /etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf

passwd: files
group: files
shadow: files

hosts: files dns
networks: files

protocols: files
services: files
ethers: files
rpc: files

# End /etc/nsswitch.conf
EOF
```


#### 6.9.2.2  Añadiendo la zona temporal

Para instalar y definir la zona de tiempo se ejecuta:

```bash
tar -xf ../../tzdata2019b.tar.gz

ZONEINFO=/usr/share/zoneinfo
mkdir -pv $ZONEINFO/{posix,right}

for tz in etcetera southamerica northamerica europe africa antarctica  \
          asia australasia backward pacificnew systemv; do
    zic -L /dev/null   -d $ZONEINFO       ${tz}
    zic -L /dev/null   -d $ZONEINFO/posix ${tz}
    zic -L leapseconds -d $ZONEINFO/right ${tz}
done

cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
zic -d $ZONEINFO -p America/New_York
unset ZONEINFO
```

El significado de los comandos zic es:

zic -L /dev/null … <br>
Esto crea zonas de tiempo de acuerdo a posix, sin segundos de ajuste para seguir el UTC (son conocidos como segundos intercalares). Lo normal es poner estos en ambos zoneinfo y zoneinfo/posix. Es necesario poner la definición de las zonas de tiempo POSIX, en zoneinfo, o varios tests resultarán en fallos. En sistemas embarcados, donde el espacio es escaso se puede intentar no actualizar nunca las zonas horarias, y esto podría ahorrar 1.9MB al no usar el directorio POSIX, pero algunas aplicaciones o test podrían resultar en fallos. 


zic -L leapseconds … <br>
Esto crea las zonas de tiempo correctas, incluyendo los segundos intercalares. En sistemas embarcados, donde el espacio es escaso y puede no necesitar actualizarse nunca las zonas de tiempo, o no importar el tiempo correcto, se puede ahorrar 1.9MB omitiendo el directorio ‘rigth’.

zic ... -p … <br>
Esto crea el archivo de las reglas de posix. Se usa Nueva York porque POSIX necesita las reglas de ahorro de tiempo de luz solar de acuerdo a las reglas de EEUU.


Una forma de determinar el tiempo local es ejecutar el siguiente script: 

```bash
tzselect
```

Después de responder unas preguntas sobre la localización, el script presentará la zona de tiempo (por ejemplo America/Edmonton). Habrá también otras zonas de tiempo posibles listadas en /usr/share/zoneinfo como Canada/Eastern o EST5EDT que no son identificadas por el script, pero que pueden ser usadas.


Entonces se creará el archivo /etc/localtime ejecutando:

```bash
ln -sfv /usr/share/zoneinfo/<xxx> /etc/localtime
```

Se ha de sustituir <xxx> con el nombre de la zona de tiempo elegida (por ejemplo: Canada/Eastern):


#### 6.9.2.3 Configuración del cargador dinámico.

Por defecto el cargador dinámico (‘/lib/ld-linux.so.2’) busca a través de /lib y /usr/lib por las librerías dinámicas que son necesarias por los programas según esto se ejecutan. De todas formas, si hay librerías en otros directorios a parte de /lib y /usr/lib, estos necesitan ser añadidos al archivo  /etc/ld.so.conf para que el cargador dinámico pueda encontrarlas. Otros dos directorios donde normalmente hay librerías adicionales son /usr/local/lib y /opt/lib. Habrá que añadir esos directorios también a la ruta de búsqueda del cargador dinámico.

Se crea un nuevo archivo /etc/ld.so.conf ejecutando:


```bash
cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib
/opt/lib
EOF
```
Si se quiere, el cargador dinámico puede también buscar en un directorio y que incluya los contenido de los archivos presentes ahí. Generalmente los archivos en este directorio de inclusión, son una linea que especifica la ruta de búsqueda deseada. Para añadir esta habilidad se ejecutan los siguientes comandos: 

```bash
cat >> /etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf

EOF
mkdir -pv /etc/ld.so.conf.d
```


### 6.9.3 Contenidos de Glibc


**Programas instalados**: <br>
catchsegv, gencat, getconf, getent, iconv, iconvconfig, ldconfig, ldd, lddlibc4, locale, localedef, makedb, mtrace, nscd, pcprofiledump, pldd, sln, sotruss, sprof, tzselect, xtrace, zdump, y zic

**Librerías instaladas**: <br>
ld-2.30.so, libBrokenLocale.{a,so}, libSegFault.so, libanl.{a,so}, libc.{a,so}, libc_nonshared.a, libcrypt.{a,so}, libdl.{a,so}, libg.a, libm.{a,so}, libmcheck.a, libmemusage.so, libmvec.{a,so}, libnsl.{a,so}, libnss_compat.so, libnss_dns.so, libnss_files.so, libnss_hesiod.so, libpcprofile.so, libpthread.{a,so}, libpthread_nonshared.a, libresolv.{a,so}, librt.{a,so}, libthread_db.so, y libutil.{a,so}

**Directorios instalados**: <br>
/usr/include/arpa, /usr/include/bits, /usr/include/gnu, /usr/include/net, /usr/include/netash, /usr/include/netatalk, /usr/include/netax25, /usr/include/neteconet, /usr/include/netinet, /usr/include/netipx, /usr/include/netiucv, /usr/include/netpacket, /usr/include/netrom, /usr/include/netrose, /usr/include/nfs, /usr/include/protocols, /usr/include/rpc, /usr/include/sys, /usr/lib/audit, /usr/lib/gconv, /usr/lib/locale, /usr/libexec/getconf, /usr/share/i18n, /usr/share/zoneinfo, /var/cache/nscd, y /var/lib/nss_db


Descripciones breves:

catchsegv <br>
Puede ser usado para crear una traza de la pila, cuando un programa es terminado con una fallo de memoria (‘segmentation fault’).

gencat <br>
Genera catálogos de mensajes. 

getconf<br>
Reporta la configuración de los valores de configuración del sistema para un las variables específicos del sistema de archivos.

getent<br>
Coge las entradas de una base de datos administrativa.

iconv<br>
Realiza conversiones de caracteres

iconvconfig<br>
Crea un módulo con los archivos de configuración para iconv de carga rápida

ldconfig<br>
Configura el cargador de librerías dinámicas.

ldd<br>
Informa que librerías compartidas son necesarias por cada programa o librería compartida

lddlibc4<br>
Asiste a ldd con archivos de objetos.

locale<br>
Presenta información sobre los actuales archivos de localización

localedef<br>
Compila las especificaciones de los archivos de localización.

makedb<br>
Crea un base de datos simple desde una entrada de texto.

mtrace<br>
Lee e interpreta trazas de memoria de archivos y presenta un resumen en formato amigable para personas.

nscd<br>
Un daemon que provee caché para las peticiones de los servicios más comunes.

pcprofiledump<br>
Volcado de información generado por el profiler PC.

pldd<br>
Lista dinámica de los objetos compartidos usados por los procesos.

sln<br>
Un programa que crea links simbólicos estáticos

sotruss<br>
Traza las llamadas a librerías estáticas de un comando especificado.

sprof<br>
Lee y reporta los datos de los objetos compartidos

tzselect<br>
Pregunta al usuario sobre la localización del sistema e informa de la descripción de la correspondiente zona de tiempo.

xtrace<br>
Traza la ejecución de un programa imprimiendo la función actual ejecutada.

zdump<br>
Volcado de la zona de tiempo.

zic<br>
Compilador de la zona de tiempo.

ld-2.30.so<br>
Un programa auxiliar para los ejecutables de las librerías compartidas.

libBrokenLocale<br>
Usado internamente por Glib como un hack para ejecutar programas con defectos. Para más información ver los comentarios en glibc-2.30/locale/broken_cur_max.c

libSegFault<br>
La señal de segmentation fault usado por catchsegv.

libanl<br>
Una librería asíncrona para consultar nombres.

libc<br>
La librería principal de C.

libcrypt<br>
La libreria de cryptografía.

libdl<br>
La librería del interfaz de enlace dinámico.

libg<br>
Una librería vacía que no contiene funciones. Previamente era una librería de ejecución en tiempo real para g++.

libm<br>
La librería matemática.

libmcheck<br>
Activa la verificación de reserva de memoria, cuando se enlaza a una compilación 

libmemusage<br>
Usado por memusage para recolectar información sobre el uso de memoria de un programa.

libnsl<br>
La librería de los servicios de comunicaciones por red.

libnss<br>
Librería que contiene funciones para resolver los nombres de sistema anfitrión, usuarios, grupos, alias, servicios, protocolos...

libpcprofile<br>
Puede ser precargado al profile de un ejecutable

libpthread<br>La librería POSIX de hilos.

libresolv<br>Contiene funciones para crear, mandar e interpretar paquetes a los servidores de dominios de Internet.

librt<br>Contiene la mayoría de funciones que proveen los interfaces especificados por POSIX 1.b extensiones de tiempo real.

libthread_db<br>Contiene funciones útiles para construir debuggers para programas multihilo.

libutil<br> Contiene código para funciones “estándar” que se usan en distintas utilidades Unix.



## 6.10 Ajustando la cadena de herramientas.

Ahora que la librería final de C ha sido instalada, es hora de ajustar la cadena de herramientas, para que así enlace cualquier programa compilado con estas nuevas librerías. 

Primero se hace una copia de seguridad de /tools linker, y se reemplaza con el enlazador (linker)  hecho en el capítulo 5. También se crea un enlace a su equivalente /tools/$(uname -m)-pc-linux-gnu/bin. 


First, backup the /tools linker, and replace it with the adjusted linker we made in chapter 5. We'll also create a link to its counterpart in /tools/$(uname -m)-pc-linux-gnu/bin:

```bash
mv -v /tools/bin/{ld,ld-old}
mv -v /tools/$(uname -m)-pc-linux-gnu/bin/{ld,ld-old}
mv -v /tools/bin/{ld-new,ld}
ln -sv /tools/bin/ld /tools/$(uname -m)-pc-linux-gnu/bin/ld
```

A continuación, se ajustan las propiedades de GCC para que apunte al nuevo enlazador dinámico. Simplemente con borrar todas las instancias de “/tools” se deberá tener la ruta correcta para el enlazador dinámico. Se han de ajustar también las propiedades de GCC para que sepa donde encontrar las cabeceras correctas y los archivos de comienzo de Glibc. El comando ‘sed’ consigue esto:


```bash
gcc -dumpspecs | sed -e 's@/tools@@g'                   \
    -e '/\*startfile_prefix_spec:/{n;s@.*@/usr/lib/ @}' \
    -e '/\*cpp:/{n;s@$@ -isystem /usr/include@}' >      \
    `dirname $(gcc --print-libgcc-file-name)`/specs
```

Es una buena idea revisar visualmente que los cambios se han llevado a cabo.

Es necesario en este punto, asegurar que las funciones básicas (compilación y enlazado) de las herramientas ajustadas, están funcionando como deben. Para ello, se ejecutan las siguientes pruebas: 

```bash
echo 'int main(){}' > dummy.c
cc dummy.c -v -Wl,--verbose &> dummy.log
readelf -l a.out | grep ': /lib'
```
Si no hubiera errores y el resultado del último comando fuera (permitiendo diferencias según la plataforma del nombre del enlazado dinámico): 


```bash
[Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
```

Hay que tener en cuenta que para sistemas de 64 bits, /lib es la localización del enlazador dinámico, pero se accede a través del enlace simbólico /lib64.

NOTA: En sistemas de 32-bits, el interprete seria: /lib/ld-linux.so.2

Ahora nos aseguramos que hemos establecido los archivos de inicio correctos. 

```bash
grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log
```
La respuesta del último comando, suele ser:

```bash
/usr/lib/../lib/crt1.o succeeded
/usr/lib/../lib/crti.o succeeded
/usr/lib/../lib/crtn.o succeeded
```

Se comprueba que el compilador está buscando los ficheros de cabecera correctos.

```bash
grep -B1 '^ /usr/include' dummy.log
```

Este comando debería dar la siguiente respuesta:

```bash
#include <...> search starts here:
 /usr/include
```

A continuación se comprueba que el nuevo enlazador está usando las rutas correctas:


```bash
grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'
```
Las referencias a rutas, que tengan partes como ‘-linux-gnu’ deberían ser ignoradas, pero de otra manera la respuesta del comando anterior debería ser: 

```bash
SEARCH_DIR("/usr/lib")
SEARCH_DIR("/lib")
```

A continuación debemos asegurarnos que se usa la correcta libc:

```bash
grep "/lib.*/libc.so.6 " dummy.log
```

La respuesta del comando anterior deberá ser:



```bash
attempt to open /lib/libc.so.6 succeeded
```

Por último nos aseguramos que gcc está usando el enlazador dinámico correcto: 

```bash
grep found dummy.log
```

La respuesta del comando anterior debería ser:

```bash
found ld-linux-x86-64.so.2 at /lib/ld-linux-x86-64.so.2
```
Si la respuesta que se obtienen no aparecen como se muestra anteriormente o no se recibe, entonces algo es incorrecto. Hay que investigar y volver a hacer los pasos para encontrar dónde está el problema y corregirlo. La razón más probable es que algo ha ido mal con las propiedades al ajustarlas. Cualquier problema que aparezca ha de solucionarse antes de continuar con el proceso. 

Una vez todo esté funcionando correctamente, se pueden limpiar los archivos de test.

```bash
rm -v dummy.c a.out dummy.log
```
## 6.11 Zlib 1.2.11

El paquete Zlib contiene rutinas de compresión y descompresión de datos que son usadas por algunos programas.


**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:** 5.1 MB


### 6.11.1 Instalación de Zlib

Se prepara Zlib para la instalación.

```bash
./configure --prefix=/usr
```

Se compila el paquete

```bash
make
```

Se comprueba la instalación:

```bash
make check
```


Se instala el paquete

```bash
make install
```
Las librerías compartidas han de ser movidas a /lib, y como resultado el archivo .so en /usr/lib ha de ser recreado:

```bash
mv -v /usr/lib/libz.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libz.so) /usr/lib/libz.so
```

 
### 6.11.2  Contenidos de Zlib

**librerías instaladas**: libz.{a,so}

Descripción Breve:

Libz    contiene rutinas de compresión y descompresión de datos que son usadas por algunos programas.












## 6.12 File-5.37

El paquete File contiene una utilidad para determinar el tipo de un archivo o archivos.


**Tiempo aproximado de construcción** : 0.1 SBU

**Espacio requerido en el disco:** 19 MB

### 6.12.1 Instalación de File

Se prepara para la compilación:

```bash
./configure --prefix=/usr
```

Se compila el paquete

```bash
make
```
Se realizan los tests:

```bash
make test
```

```bash
Se instala el paquete
```

### 6.12.2 Contenidos de File

**Programas instalados**: file

**Librerías instaladas**: libmagic.so

Descripción breve:

File		
Intenta clasificar cada archivo dado; consigue esto ejecutando distintos test- test de archivos, identificación de números mágicos, y test de lenguajes. 

Libmagic
Contiene rutinas para reconocimiento de números mágicos, y es usado por el programa file.


## 6.13 Readline-8.0

El paquete Readline contiene un grupo de librerías que ofrecen la edición de comandos y edición de la historia de comandos. 


**Tiempo aproximado de construcción** : 0.1 SBU

**Espacio requerido en el disco:** 15 MB


### 6.13.1  Instalación de Readline


La reinstalación de Readline hará que las librerías anteriores cambien a <nombre_libreria>.old. Mientras que esto no suele ser un problema, en algunos casos, puede provocar un error al enlazar con ‘ldconfig’. Esto se puede evitar ejecutando los dos siguientes comandos ‘sed’:


```bash
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install
```

Se prepara Readline para la compilación:

```bash
./configure --prefix=/usr    \
            --disable-static \
           --docdir=/usr/share/doc/readline-8.0
```

Se compila el paquete:

```bash
make SHLIB_LIBS="-L/tools/lib -lncursesw"
```

Significado de las opciones de make:

SHLIB_LIBS="-L/tools/lib -lncursesw"
Esta opción hace que se enlace con la librería ‘libncursesw’ 

Este paquete viene sin tests

Se instala el paquete

```bash
make SHLIB_LIBS="-L/tools/lib -lncursesw" install
```


Se mueven las librerías dinámicas a una posición más adecuada, y se corrigen algunos permisos y enlaces simbólicos:

```bash
mv -v /usr/lib/lib{readline,history}.so.* /lib
chmod -v u+w /lib/lib{readline,history}.so.*
ln -sfv ../../lib/$(readlink /usr/lib/libreadline.so) /usr/lib/libreadline.so
ln -sfv ../../lib/$(readlink /usr/lib/libhistory.so ) /usr/lib/libhistory.so
```

Si se desea, se instala la documentación:

```bash
install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/readline-8.0
```


### 6.13.2 Contenidos de Readline


**Librerías instaladas**:  libhistory.so y libreadline.so

**Directorios instalados**: /usr/include/readline  y /usr/share/doc/readline-8.0


Descripción breve:

Lib history   Provee un interfaz de usuario para recuperar líneas de la historia.

Lib readline   Provee un grupo de comandos para manipular la entrada de texto en una sesión interactiva de un programa. 


## 6.14  M4-1.4.18

El paquete M4 contienen un procesador de macros.


**Tiempo aproximado de construcción** : 0.4 SBU

**Espacio requerido en el disco:** 33 MB


### 6.14.1  Instalación de M4-1.4.18

Primero, realizaremos unos ajustes necesarios en glibc-2.28.

```bash
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h
```

Se prepara M4 para la compilación:

```bash
./configure --prefix=/usr
```

Se compila el paquete

```bash
make
```

Se realizan los tests:

```bash
make check
```

Se instala el paquete:

```bash
make install
```

### 6.14.2  Contenidos de M4-1.4.18


**Programa instalado**  m4

Descripción breve:

m4

Copia los archivos dados, mientras que expande las macros que contienen [Estas macros son tanto internas o definidas por el usuario y pueden tener cualquier número de argumentos. A parte de realizar la expansión de macros, m4, tiene funciones para incluir ficheros, ejecutar comandos de Unix, y realizar operaciones aritméticas, manipular texto, recursión, etc.. El program m4, puede ser usado tanto antes de compilar como procesador de macros por sí solo].


## 6.15 Bc-2.1.3

El paquete B contiene un procesador aritmético con precisión numérica arbitraria.


**Tiempo aproximado de construcción** : 0.1 SBU

**Espacio requerido en el disco:** 2.8 MB


### 6.15.1  Instalación de Bc

Se prepara Bc para compilación.

```bash
PREFIX=/usr CC=gcc CFLAGS="-std=c99" ./configure.sh -G -O3
```
El significado de las operaciones de configuración:

CC=gcc CFLAGS="-std=c99"
Estos parámetros configuran el compilador y la versión del lenguaje C para compilar. 

-O3
Especifica la optimización a usar.

-G
Omite partes de los tests, que no funcionaran sin GNU bc presente.


Se compila el paquete:

```bash
make
```
Se ejecutan los tests:

```bash
make test
```

Se instala el paquete:

```bash
make install
```

### 6.15.2  Contenido de Bc

**Programa instalados**  bc y dc

Descripción breve:

bc    una calculadora por la línea de comandos
dc   una calculadora por línea de comandos que sigue la notación polaca inversa.


## 6.16. Binutils-2.32

El paquete Binutils contiene un enlazador, un ensamblador y otras herramientas para manejar archivos objeto.


**Tiempo aproximado de construcción** : 7.4 SBU

**Espacio requerido en el disco:** 5.1 GB

### 6.16.1 Instalación de Binutils.

Para verificar que los PTY están funcionado correctamente dentro del entorno chroot, se ejecuta el tests:

```bash
expect -c "spawn ls"
```

Este comando debería imprimir lo siguiente:

```bash
spawn ls
```

Si en vez del texto anterior, se presenta el mensaje a continuación, entonces el entorno no está correctamente establecido para el trabajo de PTY. Esto incidente, ha de ser resuelto antes de ejecutar los test de verificación de Binutils y GCC.




```bash
The system has no more ptys.
Ask your system administrator to create more.
```

Ahora se ha de quitar uno de los test que evita que el resto de tests se ejecuten hasta el final.
 
```bash
sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in
```
La documentación de Binutils, recomienda construirlo en un directorio dedicado.
 
```bash
mkdir -v build
cd build
```
Se prepara binutils para compilarlo:

```bash
../configure --prefix=/usr       \
             --enable-gold       \
             --enable-ld=default \
             --enable-plugins    \
             --enable-shared     \
             --disable-werror    \
             --enable-64-bit-bfd \
             --with-system-zlib
```
 
El significado de los parámetros de configuración:
 
--enable-gold <br>
Se monta le enlazador gold y se instala como ld.gold (junto con el enlazador por defecto)
--enable-ld=default <br>
Se construye el enlazador bdf original y se instala com oambos ld (el enlazador por defecto) y ld.bfd. 
 
--enable-plugins <br>
Permite que el enlazador soporte plugins.
--enable-64-bit-bfd <br>
Permite el soporte a 64-bits (en sistemas con palabras más pequeñas). Puede que no sea necesario en sistemas de 64-bits, pero tampoco hace daño tenerlo. 
 
--with-system-zlib <br>
Se usa la librería instalada zlib en vez construir la versión incluida.
 
Se compila el paquete:
 
```bash
make tooldir=/usr
```
Significado del parámetro de make:
 
tooldir=/usr <br>
Normalmente, el directorio toolsdir (el directorio donde se guardaran al final los ejecutables) está definido a $(exec_prefix)/$(target_alias): Por ejemplo,  sistemas x86_64 se expandirá a /usr/x86_64-cualquier-linux-gnu. Ya que este es un sistema personalizado, está directorio específico en /usr no es necesario. $(exec_prefix)/$(target_alias) debería ser usado si el sistema va a ser usado para una compilación cruzada (por ejemplo, compilar un paquete en un sistema Intel que genere código para un sistema PowerPC).
 
IMPORTANTE: Los test para Binutils en esta sección son considerados críticos. Nos se recomienda saltarse estos bajo ninguna circunstancia.
 
 
Para ver el resultado de los tests:
 
```bash
make -k check
```
En el entorno de LFS puede que fallen los test de PC-relative offset y debug_msg.sh.
 
Para instalar el paquete:
 
```bash
make tooldir=/usr install
```
 
 
### 6.16.2 Contenidos de Binutils.
 
Programas instalados: addr2line, ar, as, c++filt, dwp, elfedit, gprof, ld, ld.bfd, ld.gold, nm, objcopy, objdump, ranlib, readelf, size, strings, y strip
 
Librerías instaladas:  libbfd.{a,so} and libopcodes.{a,so}
 
Directorios instalados: /usr/lib/ldscripts
 
Descripciones breves:
 
addr2line  <br>
Identifica direcciones del programa a archivos y números de línea; dado una dirección y el nombre de un ejecutable, usa la información de depuración en el ejecutable para determinar qué archivo fuente y número de línea son asociados con la dirección. 
 
ar <br>
Crea modifica y extrae de archivos.
 
as <br>
Un ensamblador que ensambla la salida de gcc en archivos objeto.
 
C++filt <br>
Es usado por el enlazador para desenredar símbolos de C++ y Java, y evitar que las funciones sobrecargadas colisionen entre ellas.
 
dwp <br>
La utilidad DWARF de paquetes
 
elfedit <br>
Actualiza las cabeceras de los archivos ELF
 
gprof <br>
Realiza un gráfico de llamadas del código.
 
ld <br>
Un enlazador que combina archivos y objetos, recolocando sus datos y estableciendo los símbolos de referencia.
 
ld.gold <br>
Una versión reducida de ld que únicamente soporta ficheros de formato ELF.
 
ld.bfd <br>
Link a ld
 
nm <br>
Lista los símbolos que ocurren en un archivo objeto dado
 
objcopy <br>
Translada un archivo objeto en otro.
 
Objdump <br>
Muestra información sobre el archivo objeto dado, con opciones que controlen la salida de información. La información mostrada normalmente es útil a programadores que estén trabajando en las herramientas de compilación. 
 
ranlib <br>
Genera un índice de contenidos de un archivo y lo guarda en el archivo. El índice lista todos los símbolos definidos en los miembros del archivo y que son archivos objeto recolocables. 
 
readelf <br>
Muestra información sobre los archivos ELF.
 
size <br>
Lista los tamaños de las secciones y del total de un archivo dado.
 
strings <br>
Para cada archivo dado presenta las secuencias de caracteres imprimibles que al menos tienen una longitud determinada (por defecto 4). Para archivos objeto, imprime solo las cadenas de las secciones de inicialización y carga, mientras que para otros tipos de archivos recorre todo el archivo.
 
strip <br>
Quita  los símbolos de un archivo objeto.
 
libbfd <br>
La librería de descriptores de archivo binarios.
libopcodes <br>
Una librería para gestionar opcodes  - los “códigos legibles” de las instrucciones para el procesador; es usado por aplicaciones como objdump. 

 
## 6.17 GMP-6.1.2

El paquete GMP contiene librerías matemáticas que son útiles para funciones aritméticas de precisiones arbitrarias.


**Tiempo aproximado de construcción** : 1.2 SBU

**Espacio requerido en el disco:** 61 MB

## 6.17.1 Instalación de  GMP

**NOTA**: Si se está construyendo el sistema para una arquitectura de 32-bits x86, pero se posee una CPU que es capaz de ejecutar código de 64-bits y se han definido en el entorno CFLAGS, el script de configuración que intenta configurar para 64-bits, fallará. Se puede evitar esto con el comando a continuación

```bash
ABI=32 ./configure ...
```
 
**NOTA**: Los valores por defecto de GMP producen librerías optimizadas para el procesador del sistema anfitrión. Si se desea que las librerías sean apropiadas para procesadores menos potentes que el del procesador anfitrión, se pueden crear ejecutando lo siguiente:

```bash
cp -v configfsf.guess config.guess
cp -v configfsf.sub   config.sub
```
 
Se prepara GMP para la compilación:
```bash
./configure --prefix=/usr    \
            --enable-cxx     \
            --disable-static \
            --docdir=/usr/share/doc/gmp-6.1.2
```
 
El significado de las opciones de compilación: 
 
--enable-cxx <br>
Este parámetro permite que se soporte C++
 
--docdir=/usr/share/doc/gmp-6.1.2 <br>
Esta variable especifica el lugar correcto para la documentación.
 
Se compila el paquete y se genera la documentación HTML:
```bash
make
make html
```
 
**IMPORTANTE**:
Los test para GMP se consideran críticos. No saltarselos bajo ninguna circunstancia.
 
Se comprueban los resultados
 
```bash
make check 2>&1 | tee gmp-check-log
```
 
 
**PRECAUCIÓN**: 
El código en gmp es altamente optimizado por el procesador para el que está orientado. Ocasionalmente, el código que detecta el procesador incorrectamente identifica las capacidades del sistema y aparecerán errores en los test o en aplicaciones que usen las librerías gmp, con el mensaje “Illegal instruction”. En este caso, gmp debería ser reconfigurado con la opción --build=x86_64-unknown-linux-gnu y volver a ser construido.
 
Se ha de verificar que se han pasado con exito los 190 test.  Para ello se ejecuta el comando:
 
```bash
awk '/# PASS:/{total+=$3} ; END{print total}' gmp-check-log
```
Se instala el paquete y la documentación:
```bash
make install
make install-html
```
 
###6.17.2 Contenidos del paquete GMP
 
**Librerías instaladas**:  libgmp.so y libgmpxx.so <br>

**Directorios instalados**: /usr/share/doc/gmp-6.1.2 <br>
 
Descripción breve:
 
libgmp <br>
Contiene funciones matemáticas de precisión arbitraria.
 
libgmpxx <br>
Contiene funciones matemáticas en C++ de precisión arbitraria.


## 6.18 MPFR-4.0.2

El paquete MPFR contienen funciones matemáticas de precisiones múltiples.


**Tiempo aproximado de construcción** : 0.9 SBU

**Espacio requerido en el disco:** 37 MB


## 6.18.1 Instalación de  MPFR

Se prepara MPFR para la compilación:

```bash
./configure --prefix=/usr        \
            --disable-static     \
            --enable-thread-safe \
            --docdir=/usr/share/doc/mpfr-4.0.2
```

Se compila el paquete y se genera la documentación HTML.

```bash
make
make html
```
IMPORTANTE: Los test para verificar MPFR son considerádos críticos. No saltarse estos test bajo ningún concepto.
 
Se comprueban los resultados, para ver que todos los test han sido satisfactorios.
 
```bash
make test
```
Se instala el paquete y su documentación:
```bash
make install
make install-html
```

## 6.18.2 Contenidos de  MPFR
**Librerías instaladas**: libmpfr.so
**Directorios instalados**: /usr/share/doc/mpfr-4.0.2
 
Descripciones breves:
Libmpfr 	 contienen funciones matemáticas de precisiones múltiples.
 
 
## 6.19 MPC-1.1.0

El paquete MPC contienen funciones matemáticas para números complejos con precisión arbitraria y correcto redondeo del resultado.


**Tiempo aproximado de construcción** : 0.3 SBU

**Espacio requerido en el disco:** 22 MB


## 6.19.1 Instalación de  MPC

Se prepara MPC para la compilación

```bash
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/mpc-1.1.0
```
Se compara el paquete y se genera la documentación HTML.
 
```bash
make
make html
 
```
Se comprueban los resultados de los tests.
 
```bash
make check
```
 
Se instala el paquete y su documentación:
 
```bash
make install
make install-html
```

## 6.19.2 Contenidos de  MPC

**Librerías instaladas**:  libmpc.so <br>
**Directorios instalados**: /usr/share/doc/mpc-1.1.0 <br>

Descripciones breves:

limpc <br>
Contiene funciones matemáticas para números complejos


## 6.20 Shadow-4.7

El paquete Shadow permite manejar passwords de un modo seguro.


**Tiempo aproximado de construcción** : 0.2 SBU

**Espacio requerido en el disco:** 46 MB

### 6.20.1 Instalación de Shadow

**NOTA**: Para reforzar el uso de passwords fuertes, referirse a http://www.linuxfromscratch.org/blfs/view/9.0/postlfs/cracklib.html  para instalar CrackLib antes de construir Shadow. Y entonces añadir --with-libcrack a los comandos **configure** debajo:

Se han de deshabilitar la instalación de los grupos (groups) y su páginas de manual (man pages) , ya que Coreutils provee una versión mejor. También se ha de evitar la instalación de las páginas del manual que se instalaron previamente en la sección 6.8 “Man-pages-5.02”.
 
```bash
sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;
```
En vez de usar el método por defecto ‘crypt’, se usa el más seguro SHA-512, que además permite passwords de más de 8 caracteres. Es también necesario cambiar la dirección obsoleta /var/spool/mail, para los buzones de email de los usuarios , para que Shadow use por defecto el directorio /var/mail.
 
```
sed -i -e 's@#ENCRYPT_METHOD DES@ENCRYPT_METHOD SHA512@' \
       -e 's@/var/spool/mail@/var/mail@' etc/login.defs
```
 
**NOTA**: Si se elige usar Shadow junto con Cracklib, se ha de ejecutar también:
 
```bash
sed -i 's@DICTPATH.*@DICTPATH\t/lib/cracklib/pw_dict@' etc/login.defs
```

Se ha de hacer un pequeño cambio para hacer que el primer número de generado por useradd sea 1000: 

```
sed -i 's/1000/999/' etc/useradd
```

Se prepara Shadow para la compilación:


```bash
./configure --sysconfdir=/etc --with-group-name-max-length=32
```

Significado de las opciones de compilación:

--with-group-name-max-length=32
Para un nombre de usuario, el máximo número de caracteres es 32. Igual para el nombre de los grupos.

Se compila el paquete:

```
make
```

Este paquete no tiene tests de comprobación.

Se instala el paquete:

Se mueve el programa a su directorio correcto:

```
mv -v /usr/bin/passwd /bin
```

### 6.20.2 Configuracíon de Shadow

Este paquete contiene utilidades para añadir, modificar y borrar usuarios y grupos de usuarios, y cambiar sus passwords; además realiza otras tareas administrativas. Por ejemplo para ver una explicación completa de lo que significa el término “password shadowing”, ver el documento de HOWTO dentro del código. Si se usa el programa Shadow, hay que tener en cuenta que los programas que necesitan verificar passwords (gestores de display, FTP, pop3 , etc.) deben ser compatibles con Shadow. Esto quiere decir que han de ser capaces de trabajar con passwords “shadowed”.

Para permitir “shadowed” passwords, se ejecuta el siguiente comando:

```bash
pwconv
```
Para permitir “shadowed” passwords en grupos, se ejecuta el siguiente comando:

```bash
grpconv
```

La configuración de Shadow para el programa ‘useradd’ tiene algunos conceptos que necesitan explicación. Primero, la acción por defecto de ‘useradd’ es crear un usuario y un grupo  con el mismo nombre que el usuario. Por defecto el identificador del usuario (UID) y el identificador del grupo (GID) comenzarán con 1000. Esto significa que si no se dan parámetros a ‘useradd’ cada usuario será miembro de un grupo único en el sistema. Si este comportamiento no es deseable, se necesita pasar el parámetro -g a ‘useradd’ y cada usuario será miembro de un único grupo en el sistema. Los parámetros del sistema están guardados en el archivo /etc/default/useradd. Puede que necesite modificar dos parámetros en este archivo para ajustar su comportamiento según lo que necesite.


```bash
/etc/default/useradd
```

Explicación de los parámetros:

GROUP=1000
Este parámetro define el comienzo de los números de grupo usado en el archivo /etc/group. Se puede modificar a cualquier número. Hay que tener en cuenta que ‘useradd’ nunca reusará un UID o GID. Si el número de identificación en este parámetro es usado, entonces se usará el siguiente número a partir de este que esté disponible. Téngase en cuenta que si no hay un grupo 100 en el sistema, la primera vez que se use ‘useradd’ sin el parámetro -g, se obtendrá en el terminal el mensaje: ‘useradd: unknown GID 1000’ (grupo 100 desconocido). Se puede descartar este mensaje y usar el identificador numérico 1000.


CREATE_MAIL_SPOOL=yes
Este parámetro causa que ‘useradd’  cree un archivo de buzón de emails para un nuevo usuario. ‘useradd’ creará un grupo propietario de este archivo en el grupo ‘mail’ y le dará permisos 0660. Si se prefiere que estos buzones, no sean creado por ‘useradd’ se puede usar el siguiente comando:

```bash
sed -i 's/yes/no/' /etc/default/useradd
```

### 6.20.3 Definiendo el password de ‘root’

Para crear un password para el usuario raíz ‘root’, se ejecuta:

```bash
passwd root
```

### 6.20.4 Contenidos de Shadow

**Programas instalados**: chage, chfn, chgpasswd, chpasswd, chsh, expiry, faillog, gpasswd, groupadd, groupdel, groupmems, groupmod, grpck, grpconv, grpunconv, lastlog, login, logoutd, newgidmap, newgrp, newuidmap, newusers, nologin, passwd, pwck, pwconv, pwunconv, sg (link to newgrp), su, useradd, userdel, usermod, vigr (link to vipw), y vipw


**Directorio instalado**: /etc/default


Descripciones breves:

chage <br>   se usa para cambiar el máximo número de días entre cambios obligatorios de password
     
chfn <br>
Usado para cambiar el nombre y otras informaciones

Chgpasswd <br>
Se usa para cambiar passwords de grupos en modo batch.

Chpasswd <br>
Se usa para actualizar passwords de grupos en modo batch.

chsh <br>
Se usa para cambiar la la shell por defecto del usuario.

expiry <br>
Se usa para reforzar la política de cambio de passwords.

failog <br>
Se usa para ver el registro de fallos de acceso al sistema, y para establecer un número máximo de fallos antes de que se bloquee la cuenta o para resetear el contador de fallos.

gpasswd <br>
Se usa para añadir o borrar miembros y administradores a grupos.

groupadd <br>
Se crea un grupo con el nombre especificado

groupdel <br>
Borra el grupo con el nombre especificado.

groupmems <br>
Permite a un grupo administrar su pertenencia a un grupo, sin necesidad de derechos o privilegios elevados.

groupmod <br>
Se usa para modificar el nombre de un grupo o su GID

grpck <br>
Verifica la integridad de los archivos /etc/group y /etc/gshadow

grpconv <br>
Crea o actualiza el archivo shadow del grupo a partir del archivo normal del grupo.

grpuncov <br>
Actualiza /etc/group partiendo /etc/gshadow y borra /etc/gshadow.

lastlog <br>
Reporta el acceso al sistema (login) mas reciente, de todos los usuarios o de uno en concreto.

login <br>
Lo usa el sistema para dejar acceder a los usuarios.

logoutd <br>
Es un servicio auxiliar para reforzar restricciones en el acceso y puertos.

newgidmap <br>
Se usa para definir el mapa de identificadores de grupo y el espacio de usuarios.

newgrp <br>
Se usa para cambiar el UID actual durante el acceso a una sesión.

newuidmap <br>
Se usa para definir el mapa de identificadores de usuario y el espacio de usuarios.

newusers <br>
Se usa para crear o actualizar toda una serie de cuentas de usuarios.

nologin <br>
Muestra un mensaje indicando que la cuenta no está disponible. Está pensado para ser usado como la consola por defecto para cuentas que han sido desactivadas.

passwd <br>
Se usa para cambiar el password de la cuenta de un usuario o de un grupo.

pwck <br>
Verifica la integridad de los archivos de passwords /etc/passwd y /etc/shadow.

pwconv <br>
Crea o actualiza los archivos de password shadow desde el archivo de password normal.

pwunconv <br>
Actualiza el archivo  /etc/passwd  desde el archivo /etc/shadow y borra este último.

sg <br>
Ejecuta un comando especificado cuando el GID del usuario está definido como el de un determinado grupo.

su <br>
Ejecuta la consola con otro usuario o grupo.

useradd <br>
Crea un nuevo usuario con el nombre especificado, o actualiza la información por defecto del usuario.

userdel <br>
Borra la cuenta del usuario especificado

usermod <br>
Se usa para modificar el nombre de acceso del usuario especificado, el número de identificación del usuario UID, consola, grupo inicial, directorio del usuario …

vigr <br>
Edita los archivos /etc/group o /etc/gshadow.

vipw <br>
Edita el archivo /etc/passwd  o  /etc/shadow.


## 6.21 GCC-9.2.0

El paquete GCC contienen la colección de compiladores de GNU, que incluye los compiladores de C y C++.

 
**Tiempo aproximado de construcción** : 95 SBU (junto con los tests)

**Espacio requerido en el disco:**  4.2 GB

### 6.21.1 Instalación de GCC

Si se está construyendo el sistema en una arquitectura x86_64, se ha de cambiar el nombre por defecto de las librerías de 64 bits a “lib”.


```bash
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
esac
```

La documentación de GCC, recomienda construirlo en un directorio dedicado.


```bash
mkdir -v build
cd  build
```

Se prepara GCC para la compilación:

```bash
SED=sed                               \
../configure --prefix=/usr            \
             --enable-languages=c,c++ \
             --disable-multilib       \
             --disable-bootstrap      \
             --with-system-zlib
```

Téngase en cuenta que para otros lenguajes, hay algunos prerrequisitos que no están disponibles. Véase el libro BLFS para ver cómo construir todos los distintos lenguajes que soporta GCC.

Significado de los nuevos parámetros de configuración:


SED=sed <br>
Se define esta variable de entorno que previene el path explicito a /tools/bin/sed.

--with-system-zlib <br>
Esto indica a GCC enlazar con la copia instalada de Zlib, antes que con su  propia copia interna.


Se compila el paquete

```bash
make
```

IMPORTANTE: En esta sección, los test para GCC son considerados críticos. No se deben ignorar bajo ninguna circunstancia.

Es conocido que uno de los test para GCC agota el tamaño de la pila, con lo cual es necesario aumentar su tamaño antes de ejecutarlo:

```bash
ulimit -s 32768
```
Hay que ejecutar los test como un usuario sin privilegios, y no detenerse en los errores.

```bash
chown -Rv nobody . 
su nobody -s /bin/bash -c "PATH=$PATH make -k check"
```

Para obtener un resumen del resultado de los tests, se ejecuta:

```bash
../contrib/test_summary
```

Para solo leer los resúmenes se filtra con: ** grep -A7 Summ.**

Los resultados se pueden comparar con los obtenidos en  http://www.linuxfromscratch.org/lfs/build-logs/9.0/ and https://gcc.gnu.org/ml/gcc-testresults/.

Seis test relativos a get_time, se sabe que fallan. Esto está aparentemente relacionado a la localización en_HK.

Dos test llamandos lookup.cc y revese.cc en experimental/net se sabe que fallan en un entorno LFS chroot ya que necesitan /etc/host y iana-etc.

Otros dos test que se sabe que fallan son los denominados pr57193.c y pr90178.c

Algunos fallos inesperados no pueden ser evitados siempre. Los desarrolladores de GG normalmente están al corriente de estos incidentes, pero pueden no haberlos resuelto todavía. A menos que los resultados del test difieran notablemente de los presentados en las URL anteriores, es seguro continuar el proceso.





Se instala el paquete y se elimina el directorio innecesario:

```bash
make install
rm -rf /usr/lib/gcc/$(gcc-dumpmachine)/9.2.0/include-fixed/bits/
```
El directorio en el que se construye GCC es del usuario __nobody-_ y la propiedad del fichero de cabeceras (y su contenido) será incorrecto. Se ha de cambiar la propiedad al usuario _root__ y al grupo:


```bash
chown -v -R root:root \
    /usr/lib/gcc/*linux-gnu/9.2.0/include{,-fixed}
```

Se ha de crear un enlace simbólico necesario por FHS por razones históricas.

```bash
ln -sv ../usr/bin/cpp /lib
```

Muchos paquetes usan el nombre __cc__ para llamar al compilador de C. Para satisfacer esto, se crea el enlace simbólico:

```bash
ln -sv gcc /usr/bin/cc
```



Se añade un enlace simbólico para permitir construir programas usando la optimización de tiempo en el enlazado (Lint Time Optimization LTO, en inglés):

```bash
install -v -dm755 /usr/lib/bfd-plugins
ln -sfv ../../libexec/gcc/$(gcc -dumpmachine)/9.2.0/liblto_plugin.so \
        /usr/lib/bfd-plugins/
```

Ahora que al fin la cadena de herramientas está completa, es importante asegurarse de que esta compila y enlaza como se espera. Se hace esto realizando pruebas de seguridad tal y como se hizo en el capítulo anterior.


```bash
echo 'int main(){}' > dummy.c
cc dummy.c -v -Wl,--verbose &> dummy.log
readelf -l a.out | grep ': /lib'
```

No debería de haber errores y la salida del último comando será (permitiendo las diferencias de nombres entre plataformas):

```bash
[Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
```
Ahora nos debemos asegurar de que se han definido los archivos de comienzo correctos:

```bash
grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log
```

La salida del comando anterior debería ser:

```bash
/usr/lib/gcc/x86_64-pc-linux-gnu/9.2.0/../../../../lib/crt1.o succeeded
/usr/lib/gcc/x86_64-pc-linux-gnu/9.2.0/../../../../lib/crti.o succeeded
/usr/lib/gcc/x86_64-pc-linux-gnu/9.2.0/../../../../lib/crtn.o succeeded
```


Dependiendo en la arquitectura del sistema, la salida anterior puede diferir algo, la diferencia normalmente está en el nombre del directorio después de /usr/lib/gcc. Lo que hay que mirar es que gcc ha encontrado los tres archivos crt*.o en el directorio /usr/lib.

Se verifica que el compilador está buscando los archivos de cabecera correctos:

```bash
grep -B4 '^ /usr/include' dummy.log
```
Esto debería devolver la siguiente respuesta:

```bash
#include <...> search starts here:
 /usr/lib/gcc/x86_64-pc-linux-gnu/9.2.0/include
 /usr/local/include
 /usr/lib/gcc/x86_64-pc-linux-gnu/9.2.0/include-fixed
 /usr/include
```



De nuevo, téngase en cuenta que algunos archivos pueden tener un nombre distinto dependiendo en la arquitectura.

A continuación se verifica que el nuevo enlazador está siendo usado con las rutas de búsqueda correctas:

```bash
grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'
```

Se pueden ignorar las referencias a las rutas que tienen componentes con '-linux-gnu' , pero la respuesta al último comando, debería ser:

```bash
SEARCH_DIR("/usr/x86_64-pc-linux-gnu/lib64")
SEARCH_DIR("/usr/local/lib64")
SEARCH_DIR("/lib64")
SEARCH_DIR("/usr/lib64")
SEARCH_DIR("/usr/x86_64-pc-linux-gnu/lib")
SEARCH_DIR("/usr/local/lib")
SEARCH_DIR("/lib")
SEARCH_DIR("/usr/lib");
```

Un sistema de 32 bits puede ver una respuesta un poco distinta. Por ejemplo a continuación una respuesta de un sistema i686:

```bash
SEARCH_DIR("/usr/i686-pc-linux-gnu/lib32")
SEARCH_DIR("/usr/local/lib32")
SEARCH_DIR("/lib32")
SEARCH_DIR("/usr/lib32")
SEARCH_DIR("/usr/i686-pc-linux-gnu/lib")
SEARCH_DIR("/usr/local/lib")
SEARCH_DIR("/lib")
SEARCH_DIR("/usr/lib");
```

A continuación verificamos que se está usando la libc correcta:

```bash
grep "/lib.*/libc.so.6 " dummy.log
```
El resultado al comando anterior debería ser:

```bash
attempt to open /lib/libc.so.6 succeeded
```

Por último, nos aseguramos que GCC está usando el enlazador dinámico correcto: 

```bash
grep found dummy.log
```
La respuesta del comando anterior, debería ser (permitiendo pequeñas diferencias en el nombre del enlazador según la plataforma)


```bash
found ld-linux-x86-64.so.2 at /lib/ld-linux-x86-64.so.2
```


Si la respuesta no es como la mostrada antes, o no se recibe nada, entonces hay algo incorrecto. Se ha de investigar y repasar los pasos dados para descubrir qué problema hay y corregirlo. Lo más probable es que si algo ha ido mal con el ajuste de los archivos. Cualquier error ha de resolverse antes de continuar con el proceso.


Una vez todo esté funcionando correctamente se pueden limpiar los archivos de test:

```bash
rm -v dummy.c a.out dummy.log
```
Finalmente se recoloca el archivo:

```bash
mkdir -pv /usr/share/gdb/auto-load/usr/lib
mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib
```




### 6.21.2 Contenidos de GCC

**Programas instalados:** c++, cc (link to gcc), cpp, g++, gcc, gcc-ar, gcc-nm, gcc-ranlib, gcov, gcov-dump y  gcov-tool

**Librerías instaladas**:  libasan.{a,so}, libatomic.{a,so}, libcc1.so, libgcc.a, libgcc_eh.a, libgcc_s.so, libgcov.a, libgomp.{a,so}, libitm.{a,so}, liblsan.{a,so}, liblto_plugin.so, libquadmath.{a,so}, libssp.{a,so}, libssp_nonshared.a, libstdc++.{a,so}, libstdc++fs.a, libsupc++.a, libtsan.{a,so} y  libubsan.{a,so}

**Directorios instalados**: /usr/include/c++, /usr/lib/gcc, /usr/libexec/gcc y  /usr/share/gcc-9.2.0


Descripciones breves:


**c++** <br>

El compilador de C++

**cc** <br>

El compilador de C

**cpp** <br>

El preprocesador de C; que es usado por el compilador para expandir los #include, #define y demás declaraciones el los archivos del código. 

**g++** <br>

El compilador de C++.

**gcc** <br>

El compilador de C 

**gcc-ar** <br>

Es un envoltorio por encima de **ar** que añade un plugin a la línea de comandos. Este programa se usa solo para añadir la optimización de tiempo al enlazado (LTO “link time optimization” en inglés) y no es útil en las opciones de construcción por defecto.

**gcc-nm** <br>

Es un envoltorio en torno a **nm** que añade un plugin a la línea de comandas.  Este programa se usa solo para añadir la optimización de tiempo al enlazado (LTO “link time optimization” en inglés) y no es útil en las opciones de construcción por defecto.

**gcc-ranlib** <br>

envoltorio en torno a **ranlib ** que añade un plugin a la línea de comandas.  Este programa se usa solo para añadir la optimización de tiempo al enlazado (LTO “link time optimization” en inglés) y no es útil en las opciones de construcción por defecto.

**gcov** <br>

Una herramienta de cobertura de test. Se usa para analizar programas y determinar donde pueden tener mayor efecto las optimizaciones.

**gcov-dump** <br>

Una herramienta offline de  gcda y gcno.

**gcov-tool** <br>

Una herramienta offline de gcda

**libasan** <br>

La librería de verificación de direcciones de memoria

**libatomic** <br>

Librería de GCC para instrucciones atómicas

**libcc1** <br>

La librería de C para preprocesado.

**libgcc** <br>

Tiene el soporte de gcc para el tiempo de ejecución.

**libgcov** <br>

Esta librería se enlaza a un programa cuando se quiere instrumentar y permitir que se analize su rendimiento. 

**libgomp** <br>

Es la implementación de GNU de OpenMP API para programaciones en paralelo para múltiples plataformas y memorias compartidas, en C/C++ y Fortran. 

**liblsan** <br>

La librería de verificación de fugas de memoria. 

**Liblto_plugin** <br>

Librería para la optimización de tiempo al enlazado (LTO “link time optimization” en inglés) para hacer optimizaciones en las compilaciones.


**libquadmath** <br>

GCC Quad Precision Math Library API

**libssp** <br>

Contiene rutinas para las funciones de protección de la pila (GCC stack-smashing protection) 

**libstdc++** <br>

La librería de C++ estándar. 

**libstdc++fs** <br>

ISO/IEC TS 18822:2015 librería del sistema de archivos

**libsupc++** <br>

Da soporte a las rutinas de los lenguajes C/C++. 

**libtsan** <br>

La librería de verificación de hilos en tiempo de ejecución.

**libubsan** <br>

Librería de verificación de comportamientos no definidos en tiempo de ejecución.


## 6.22 Bzip2-1.0.8

El paquete Bzip2 contiene programas para comprimir y descomprimir archivos. La compresión de archivos de texto con bzip2 tiene rendimiento de compresión mucho mejor que el tradicional gzip.


**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:**  7.7 MB

###6.22.1 Instalación de Bzip2

Se aplica el parche que instalará la documentación para este paquete:

```bash
patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch
```

El siguiente comando asegura que la instalación de enlaces simbólicos es relativa:

```bash
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
```
Se comprueba que las páginas man están instaladas en el lugar correcto:

```bash
sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
```

Se prepara Bzip2 para la compilación:

```bash
make -f Makefile-libbz2_so
make clean
```

El significado de los parámetros de make:

-f Makefile-libbz2_so <br>
Esto hará que Bzip2 se construya usando un archivo Makefile distinto, en este caso el archivo Makefuile-libz2_so, que crea la librería dinámica libbbz2.so y enlaza la utilidad Bzip2 con ella.

Se compila y se prueba el paquete:

```bash
make
```

Se instala el programa:

```bash
make PREFIX=/usr install
```

Se instala el binario compartido bzip2, en el directorio /bin, se crean unos enlaces simbólicos necesarios y se limpia:

```bash
cp -v bzip2-shared /bin/bzip2
cp -av libbz2.so* /lib
ln -sv ../../lib/libbz2.so.1.0 /usr/lib/libbz2.so
rm -v /usr/bin/{bunzip2,bzcat,bzip2}
ln -sv bzip2 /bin/bunzip2
ln -sv bzip2 /bin/bzcat
```

###6.22.2 Contenidos de Bzip2

**Programas instalados**: bunzip2 (enlace a bzip2), bzcat (enlace a bzip2), bzcmp (enlace a bzdiff), bzdiff, bzegrep (enlace a bzgrep), bzfgrep (enalace a bzgrep), bzgrep, bzip2, bzip2recover, bzless (enlace a bzmore), y bzmore

**Librerías instaladas**: libbz2.{a,so}

**Directorios instalados**:   /usr/share/doc/bzip2-1.0.8


Descripciones breves:

bunzip2

Descomprime archivos de bzip.

bzcat

Descomprime a la salida estándar

bzcmp

Ejecuta ‘cmp’ en archivos de bzip.

bzdiff

Ejecuta 'diff' en archivos de bzip

bzegrep

Ejecuta 'egrep' en archivos de bzip

bzfgrep

Ejecuta 'fgrep' en archivos de bzip 

bzgrep

Ejecuta 'grep' en archivos de bzip

bzip2

Comprime archivos usando el algoritmo de compresión de Huffan con el de ordenación Burrows-Wheeler; la tasa de compresión es mejor que 
la que se obtiene con métodos más convencionales como usar algoritmos “Lempel-Ziv”, como hace gzip.

bzip2recover

Intenta recuperar datos de una archivo de bzip dañado.

bzless

Ejecuta 'less' en archivos de bzip


bzmore

Ejecuta 'more' en archivos de bzip

libbz2

La librería que implementa compresión sin pérdidas y ordenación de bloques usando el algoritmo de Burrows-Wheeler.


## 6.23 Pkg-config-0.29.2

El paquete pkg-config contiene una herramienta para pasar la localización de los  “includes” y/o las localizaciones de las librerías durante la configuración y la ejecución de “make”.


**Tiempo aproximado de construcción** : menos de 0.4 SBU

**Espacio requerido en el disco:**  30 MB


### 6.23.1 Instalación de Pkg-config-0.29.2

Se prepara el Pkg-config-0.29.2 para la compilación:

```bash
./configure --prefix=/usr              \
            --with-internal-glib       \
            --disable-host-tool        \
            --docdir=/usr/share/doc/pkg-config-0.29.2
```

El significado de las nuevas opciones de configuración:

--with-internal-glib
 Esto permitirá a pkg-config usar su versión interna de Glib ya que una versión externa no está disponible en LFS.

--disable-host-tool
Esta opción deshabilita la creación de un enlace no simbólico al programa pkg-config.


Para compilar el paquete:

```
make
```

Para verificar los resultados de los tests:

```
make check
```

Para instalar el paquete:

```
make install
```


### 6.23.2 Contenidos de Pkg-config-0.29.2

**Programas instalados**: pkg-config

**Directorio instalado**: /usr/share/doc/pkg-config-0.29.2


Descripción breve:

Pkg-config   devuelve metainformación sobre la librería o paquete indicado.


## 6.24 Ncurses-6.1

El paquete Ncurses contiene librerías para generar pantallas de caracteres d forma independiente al terminal.

**Tiempo aproximado de construcción** : 0.4 SBU

**Espacio requerido en el disco:**  42 MB

### 6.24.1 Instalación de Ncurses

No se instalará una librería estática que se utiliza en la configuración:

```bash
sed -i '/LIBTOOL_INSTALL/d' c++/Makefile.in
```

Se prepara Ncurses para la compilación:


```bash
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --enable-pc-files       \
            --enable-widec
```

El significado de las nuevas opciones de configuración, es:

--enable-widec <br>
Esta opción hace que las librerías de caracteres ampliados ( como libncursesw.so.6.1) sean construidas en vez de las normales (como: libncurses.so.6.1). Estas librerías de caracteres ampliados pueden ser usadas tanto en sistemas multi-byte como en sistemas con localizaciones de  de 8-bits, mientras que las librerías normales funcionan únicamente con localizaciones de usan 8-bits. Las librerías de caracteres ampliados son compatibles a nivel de código, pero no de binarios.

--enable-pc-files <br>
Esta opción genera e instala archivos .pc para pkg-config

--without-normal <br>
Esta opción deshabilita compilar e instalar la mayoría de librerías estáticas.

Se compila el paquete:

```
make
```

Este paquete tiene un conjunto de test, pero solo se pueden ejecutar después de que el paquete haya sido instalado. Léase el archivo README en el directorio para más detalles.
Para instalar el paquete:
 
```
make install
```
Se han de mover las librerías compartidas al directorio /lib, que es donde se espera que estén:

```
mv -v /usr/lib/libncursesw.so.6* /lib
```

Al haber movido las librerías al directorio /lib, un enlace simbólico apunta a un archivo inexistente, hay que redefinirlo.

```
ln -sfv ../../lib/$(readlink /usr/lib/libncursesw.so) /usr/lib/libncursesw.so
```

Muchas aplicaciones todavía querrán usar el enlazador para encontrar la librería de Ncurses de los caracteres de 8 bits. Se “engaña” a esas aplicaciones para enlazar las librerías de caracteres ampliados por medio de enlaces simbólicos y scripts:

```bas
for lib in ncurses form panel menu ; do
    rm -vf                    /usr/lib/lib${lib}.so
    echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
done
```
Finalmente, nos aseguramos que las aplicaciones anteriores que buscan -lcurses al compilar, todavía se pueden compilar:

```bash
rm -vf                     /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
ln -sfv libncurses.so      /usr/lib/libcurses.so
```

Si se desea se puede instalar la documentación de Ncurses:

```bash
mkdir -v       /usr/share/doc/ncurses-6.1
cp -v -R doc/* /usr/share/doc/ncurses-6.1
```

**NOTA:**
Las instrucciones anteriores no crean librerías de Ncurses con caracteres no ampliados, ya que ningún paquete instalado compilado desde el código enlazará con ellas en tiempo de ejecución. De todas formas el único binario conocido que enlaza con librerías de caracteres no ampliados requiere la versión 5. Si se deben tener dichas librerías debido a alguna aplicación binaria, o para ser compatible con LSB, se puede construir el paquete de nuevo con los siguientes comandos.


```bash
make distclean
./configure --prefix=/usr    \
            --with-shared    \
            --without-normal \
            --without-debug  \
            --without-cxx-binding \
            --with-abi-version=5 
make sources libs
cp -av lib/lib*.so.5* /usr/lib
```


### 6.24.2 Contenido de Ncurses

**Programas instalados**: captoinfo (enlaza con  tic), clear, infocmp, infotocap (enlaza con tic), ncursesw6-config, reset (enlaza con  tset), tabs, tic, toe, tput, y tset


**Librerías instaladas**: libcursesw.so (enlace simbólico y script para enlazar con libncursesw.so), libformw.so, libmenuw.so, libncursesw.so, libncurses++w.a, libpanelw.so, y sus correspondientes librerías con caracteres no ampliados, que no tienen la “w” en el nombre de la librería.

**Directorios instalados**: /usr/share/tabset, /usr/share/terminfo, y /usr/share/doc/ncurses-6.1

Descripciones breves:


**captoinfo** <br>

Convierte una descripción __termcap__ en una descripción __terminfo__. (un archivo terminfo contiene información sobre las capacidades de un terminal dado.) 

**clear** <br>

Limpia la pantalla, si es posible.

**infocmp** <br>

Compara o imprime las descripciones de terminfo.

**infotocap

Convierte una descripción de terminfo en una descripción de termcap.

**ncursesw6-config** <br>

Provee información de configuración para ncurses.

**reset** <br>

Reinicializa un terminal a sus valores por defecto.

**tabs** <br>

Limpia y define las paradas de tab en un terminal.

**tic** <br>

La descripción de entrada al compilador de terminfo que se traduce a un archivo terminfo desde el formato código a un formato binario para
las rutinas de la librería ncurses. (un archivo terminfo contiene información sobre las capacidades de un terminal dado) 

**toe** <br>

Lista todos los tipos de terminales disponibles, dando el nombre primario e información de cada uno.

**tput** <br>

Hace que los valores que dependen las propiedades de cada terminal disponible para la consola. Puede también reiniciar o inicializar 
un terminal o informar de su nombre completo.

**tset** <br>

Puede usarse para inicializar terminales.

**libcursesw** <br>

Enlace a libncursesw

**libncursesw** <br>

Contiene funciones para mostrar texto de formas complejas en la pantalla de un terminal; un buen ejemplo es el uso de estas funciones en el menú que se muestra en el menú de configuración del kernel.

**libformw** <br>

Contiene funciones que implementan formularios.

**libmenuw** <br>

Contiene funciones que implementan menús.

**libpanelw** <br>

Contiene funciones que implementan paneles.

## 6.25 Attr-2.4.48

El paquete Attr contiene utilidades para administrar atributos extra en el sistema de archivos.


**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:**  4,2 MB

### 6.25.1 Instalación de Attr


Se prepara Attr para la compilación:

```bash
./configure --prefix=/usr     \
            --bindir=/bin     \
            --disable-static  \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/attr-2.4.48
```

Se compila el paquete:

```bash
make
```

Los test necesitan ser ejecutados en un sistema de archivos que soporte atributos extendidos como ext2, ext3 o ext4. Para ejecutar los test, se ejecuta: 


```bash
make check
```
Se instala el paquete:

```bash
make install
```

Las librerías compartidas necesitar ser movidas a /lib, y como resultado el archivo .so en /usr/lib ha de ser recreado:

```bash
mv -v /usr/lib/libattr.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libattr.so) /usr/lib/libattr.so
```

### 6.25.2 Contenidos de Attr


**Programas instalados**: attr, getfattr, and setfattr

**Librerías instaladas**: libattr.so

**Directorios instalados**: /usr/include/attr and /usr/share/doc/attr-2.4.48

Descripciones breves:

**attr** <br>

Amplia los atributos del sistema de archivos.

**getfattr** <br>

Coje los atributos ampliados de los objetos de sistema de archivos.


**setfattr** <br>

Define los atributos ampliados del sistema de archivos.

**libattr** <br>

Contiene la librería de funciones para manipular atributos ampliados.


##  6.26 Acl-2.2.53

El paquete Acl contiene utilidades para administrar listas de control de accesos (Access Control List, en inglés), las cuales se usan para definir un 
control más minucioso y derechos de acceso para archivos y directorios.

**Tiempo aproximado de construcción** : menos de 0.1  SBU

**Espacio requerido en el disco:**  6.4 MB

###  6.26.1 Instalación de Acl

Se prepara Acl para la compilación:

```bash
./configure --prefix=/usr         \
            --bindir=/bin         \
            --disable-static      \
            --libexecdir=/usr/lib \
            --docdir=/usr/share/doc/acl-2.2.53
```

Se compila el paquete:

```bash
make
```

Los test de Acl necesitan ser ejecutados en un sistema de archivos que soporte los controles de acceso después de que Coreutils ha sido construido con las librerías Acl. Si se desea, se puede volver a este paquete y ejecutar ‘make check’ después de que Coreutils haya sido construido posteriormente en este capítulo.


Se instala el paquete:

```
make install
```

Las librerías compartidas necesitan ser movidas a /lib, y como resultado el archivo .so in /usr/lib necesita ser recreado.

```bash
mv -v /usr/lib/libacl.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libacl.so) /usr/lib/libacl.so
```

###  6.26.2 Contenidos de Acl

Programas instalados:  chacl, getfacl y setfacl
Librerías instaladas: libacl.so
Directorios instalados: /usr/include/acl and /usr/share/doc/acl-2.2.53
Descripciones breves:


**chacl**<br>

Cambios en la lista de control de accesos en un archivo o directorio.

**getfacl**<br>

Coje la lista de control de accesos

**setfacl**<br>

Define la lista de control de accesos

**libacl**<br>

Contiene la librería de funciones para trabajar con las listas de control de accesos.




## 6.27 Libcap-2.27


El paquete Libcap implementa en el espacio de usuario los interfaces para la normativa POSIX 1003.1e que dispone el kernel de Linux. Estas capacidades son una división de los privilegios de ‘root’ en una serie de distintos privilegios. 


**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:**  1,5 MB

### 6.27.1 Instalación de Libcap-2.27


Se previene la instalación de una de las librerías estáticas.

```bash
sed -i '/install.*STALIBNAME/d' libcap/Makefile
```
Se compila el paquete:

```bash
make
```
Este paquete no tienen un conjunto de tests.

Se instala el paquete:

```bash
make RAISE_SETFCAP=no lib=lib prefix=/usr install
chmod -v 755 /usr/lib/libcap.so.2.27
```

El significado de las opciones de compilación:

RAISE_SETFCAP=no <br>
Este parámetro evita usar ‘setcap’ en el mismo. Esto evita un error de instalación si el keren o el sistema de archivos no soporta capacidades extendidas.

lib=lib <br>
Este parámetro instala la librería en $prefix/lib, en vez de $prefix/lib64 en una arquitectura x86_64. No tienen ningún efecto en x86.  

La librería compartida necesita ser movida a /lib, y como resultado el archivo .so en /usr/lib necesita ser recreada:

```bash
mv -v /usr/lib/libcap.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libcap.so) /usr/lib/libcap.so
```


### 6.27.2 Contenidos de Libcap-2.27

**Programas instalados**: capsh, getcap, getpcaps y setcap

**Librerías instaladas**: libcap.so

Descripciones breves:

**capsh**<br>

Un envoltorio para la consola para explorar y mantener el soporte de las capacidades.

**getcap**<br>

Examina las capacidades/propiedades del archivo

**getpcaps**<br>

Muestra las capacidades/propiedades  en los procesos indicados

**setcap**<br>

Define las capacidades/propiedades de cada archivo.

**libcap**<br>

Contiene la libreria de funciones para manipular las capacidades/propiedades de POSIX 1003.1e



## 6.28 Sed-4.7

El paquete Sed contiene un editor de flujos de texto


**Tiempo aproximado de construcción** : menos de 0.4 SBU

**Espacio requerido en el disco:**  32 MB

### 6.28.1 Instalación de Sed

Primero se ha de corregir un punto del entorno LFS y quitar un test que falla:

```bash
sed -i 's/usr/tools/'                 build-aux/help2man
sed -i 's/testsuite.panic-tests.sh//' Makefile.in
```

Se prepara Sed para la compilación:

```bash
./configure --prefix=/usr --bindir=/bin
```

Se compila el paquete y se genera la documentación HTML.

```bash
make
make html
```

Se ejecutan los tests:

```bash
make check
```

Se instala el paquete y su documentación:

```bash
make install
install -d -m755           /usr/share/doc/sed-4.7
install -m644 doc/sed.html /usr/share/doc/sed-4.7
```

### 6.28.2 Contenidos  de Sed

**Programas instalados**: sed
**Librerías instaladas**: /usr/share/doc/sed-4.7

Descripciones breves:

Sed    filtra y modifica flujos de texto en una pasada.


## 6.29 Psmisc-23.2

El paquete Psmisc contiene programas para mostrar información sobre los procesos en ejecución. 


**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:**  4.6 MB

### 6.29.1 Instalación de Psmisc

Se prepara Psmisc para la compilación:

```bash
./configure --prefix=/usr
```

Se compila el paquete:

```bash
make
```
Este paquete no tiene un conjunto de tests.

Se instala:

```bash
make install
```

Por último, se mueven los programas ‘killall’ y ‘fuser’ a la ruta indicada por FHS: 


```bash
mv -v /usr/bin/fuser   /bin
mv -v /usr/bin/killall /bin
```

### 6.29 Contenidos de PSmisc

**fuser** <br>

Informa los Identificadores de los procesos (PIDs) que usan los archivos indicados.


**killall** <br>

Finaliza los procesos según el nombre indicado.

**peekfd** <br>

Mira a los descriptores de archivos de los procesos ejecutandose, indicando su PID.

**prtstat** <br>

Muestra información sobre un proceso

**pslog** <br>

Informa de las rutas de los registros (logs) de un proceso


**pstree** <br>

Muestra los procesos en ejecución en forma de árbol

**pstree.x11** <br>

Igual que pstree, pero espera por confirmación antes de cerrarse.



## 6.30 Iana-Etc-2.30

El paquete Iana-Etc provee datos para los servicios de redes y protocolos.


**Tiempo aproximado de construcción** : menos de 0.1 SBU

**Espacio requerido en el disco:**  2.3 MB


### 6.30.1 Instalación de Iana-Etc

El siguiente comando convierte los datos crudos proveídos por IANA en los formatos correctos para los archivos es /etc/protocols y /etc/services.

```bash
make
```

Este paquete no tiene ningún grupo de tests.

Para instalar el paquete, se ejecuta:

```bash
make install
```

### 6.30.2 Contenido de Iana-Etc

Archivos instalados: /etc/protocols y /etc/services.

Descripciones breves: 


/etc/protocols : Describe varios protocolos de internet DARPA que están disponibles en el subsistema TCP/IP 

/etc/services : Provee una relación entre nombres amigables de servicios de internet y sus números de puerto y nombres de protocolos.


# 6.31. Bison-3.4.1
 
Bison contiene un paquete con un generador de parseo.
 
**Tiempo aproximado de construcción**: 0.3 SBU
**Espacio requerido en el disco**: 39 MB
 
## 6.31.1.1. Instalación de Bison
 
Primero, se corrige un error que hay con la versión actual:
```bash
sed -i '6855 s/mv/cp/' Makefile.in
```
 
Se prepara Bison para la compilación:
 
```bash
./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.4.1
```
Se compila el paquete, con un método que solventa una condición de carrera en la versión actual.
 
```bash
make -j1
```
 
Hay una dependencia circular entre bison y flex con relación a los test de verificación. Si se desea después de instalar flex en la próxima sección se puede volver a compilar el paquete de Bison y efectuar los test con: make check
 
Se instala el paquete:
 
```bash
make install
```
 
## 6.31.2 Contenidos de Bison

Programas instalados: Bison y yacc

Librerías instaladas: liby.a

Directorios instalados: /usr/share/bison
 
Descripciones breves:
 
Bison
Genera, a partir de una serie de reglas, un programa para analizar la estructura de archivos de texto.
Yacc
Un envoltorio para Bison, destinado para programas que aun llamen a yacc en vez de a Bison; Este llama a Bison con la opción –y.
 
Liby
La librería de yacc, que contiene implementaciones compatibles con yacc, yyerror y funciones main. Esta librería no es muy útil, pero POSIX la requiere. 


## 6.33  Flex-2.6.4

El paquete Flex contienen una utilizad para generar programas que reconozcan patrones en un texto.

**Tiempo aproximado de construcción** : 0.4 SBU

**Espacio requerido en el disco:**  36 MB

### 6.33.1 Instalación de Flex

Primero se corrige un problema que se introdujo con glibc-2.26:

```bash
sed -i "/math.h/a #include <malloc.h>" src/flexdef.h
```

El proceso de construcción asume que el programa help2man está disponible para crear una página de manual desde la opción ejecutable --help.  Si no está presente se usa una variable de entorno para saltarse este paso. Ahora se prepara Flex para la compilación: 

```bash
HELP2MAN=/tools/bin/true \
./configure --prefix=/usr --docdir=/usr/share/doc/flex-2.6.4
```

Se compila el paquete:

```bash
make
```

Se verifican los resultados de los tests  (tarda sobre 0.5 SBU)

```bash
make check
```
Se instala el paquete:

```bash
make install
```

Algunos cuantos programas no tienen constancia de **Flex**, y tratan de usar su predecesor **lex**. Para dar soporte a estos programas, se crea un enlace simbólico denominado ‘lex’ que ejecute Flex en modo de emulación de ‘lex’:

```bash
ln -sv flex /usr/bin/lex
```

## 6.33.2 Contenidos de Flex

Programas instalados: flex, flex++ (link a flex), y lex (link a flex)

Librerías instaladas: libfl.so

Directorios instalados: /usr/share/doc/flex-2.6.4
 
Descripciones breves:
**flex**: Una herramienta para generar programas que reconocen patrones en un texto; esto permite la versatilidad de especificar las reglas para encontrar patrones, sin la complicación de desarrollar un programa específico.

**flex++**: Una extensión de flex, usada para generar código y clases de C++. Es un link simbólico a flex.

**lex**: Es un link simbólico que ejecuta ‘flex’, emulando ‘lex’.

**libfl** La librería de ‘flex’

## 6.34  Grep-3.4

El paquete Grep, contienen un programa para buscar en archivos.

**Tiempo aproximado de construcción** : 0.7 SBU

**Espacio requerido en el disco:**  39 MB

### 6.33.1 Instalación de Grep

Se prepara Grep para la compilación:

```bash
./configure --prefix=/usr --bindir=/bin
```

Se compila el paquete:

```bash
make
```
Se verifican los resultados de los tests.

```bash
make check
```
Se instala el paquete

```bash
make install
```
## 6.34.2 Contenidos de Flex
Programas instalados: egrep, fgrep y grep

Descripciones breves:

**egrep**  Reporta las líneas que coinciden con una expresión regular extendida.

**fgrep**  Reporta las líneas que coinciden con una lista de cadenas fijas.

**grep**  Reporta las líneas que coinciden con una expresión regular básica.


## 6.35  Bash-5.0

El paquete Bash contiene la consola Bourne-Again SHell.

**Tiempo aproximado de construcción** : 1.9 SBU

**Espacio requerido en el disco:**  62 MB

### 6.35.1 Instalación de Bash

Se incorporan algunas correcciones: 

```bash
patch -Np1 -i ../bash-5.0-upstream_fixes-1.patch
```
Se prepara Bash para la compilación:

```bash
./configure --prefix=/usr                    \
            --docdir=/usr/share/doc/bash-5.0 \
            --without-bash-malloc            \
            --with-installed-readline
```

Significado de la línea de compilación: 

--with-installed-readline

Esta opción le indica a Bash,  que use la librería ‘realine’ que ya está instalada en el sistema, antes que su propia versión de la librería.

Se compila el paquete:

```bash
make
```

Si no se van a ejecutar los tests, se salta hasta “Install the package”

Se preparan los test, para asegurar que el usuario ‘nobody’  puede escribir en el árbol de recursos.

```bash
chown -Rv nobody .
```

Ahora se ejecutan los test como el usuario ‘nobody’

```bash
su nobody -s /bin/bash -c "PATH=$PATH HOME=/home make tests"
```

Se instala el paquete y se mueve el ejecutable principal a /bin.

```bash
make install
mv -vf /usr/bin/bash /bin
```

Se ejecuta el nuevo programa compilado ‘bash’ (sustituyendo al que está ejecutándose).

```bash
exec /bin/bash --login +h
```

**NOTA:** Los parámetros usados hacen al proceso ‘bash’ un terminal interactivo, y hay que continuar deshabilitando el hashing para que los nuevos programas se vayan encontrando según estén disponibles. 

### 6.35.2 Contenidos de Bash

Programas instalados: bash, bashbug y  sh (link a bash)

Descripciones breves:

**bash**  Un intérprete de comandos ampliamente utilizado; ejecuta varios tipos de expansiones y sustituciones en un comando dado, antes de ejecutarlo, haciendo de este intérprete una poderosa herramienta.

**bashbug**  Un script de consola, para ayudar al usuario a escribir un email con formato estándar para reportar errores relacionados con bash.

**sh**  Un enlace simbólico con ‘bash’; cuando se invoca como ‘sh’, ‘bash’ intenta emular el comportamiento en arranque de las versiones históricas de ‘sh’ tan fielmente posible, mientras que también mantiene el estándar POSIX.



## 6.36  Libtool-2.4.6

El paquete Libtool contienen scripts de soporte genérico a las librerías de GNU. Este gestiona la complejidad  de usar librerías compartidas de una forma consistente, y portable.

**Tiempo aproximado de construcción** : 1.8 SBU

**Espacio requerido en el disco:**  43 MB

### 6.36.1 Instalación de Libtool

Se prepara Libtool para la compilación: 

```bash
./configure --prefix=/usr
```
Se compila el paquete:

```bash
make
```

Se verifican los resultados del test

```bash
make check
```

NOTA: El tiempo de testeo para Libtool, puede ser reducido considerablemente, en sistemas con varios núcleos. Para hacer esto, se ha de añadir ‘TESTSUITEFLAGS=-j<N>’ a la línea anterior. Por ejemplo, si se usa -j4 se puede reducir el tiempo de testeo más de un 60%. 


Se sabe que los cinco primeros tests fallan en el entorno de LFS, debido a una dependencia circular, pero todos los test han de pasar, después de haber instalado automake. 

Para instalar el paquete:

```bash
make install
```

### 6.36.2 Contenidos de Libtool

Programas instalados: libtool y libtoolize


Librerias Instaladas s: libltdl.so


Directorios instalados: /urs/include/libltdl y /usr/share/libtool


Descripciones Breves:

**Libtool**     Provee de servicios generales para construir librerías.


**Libtoolize**  Provee un método estándar para dar soporte de Libtool a un paquete.


**libltdl**  Oculta varias de las dificultades de usar librerías con dlopen.

## 6.37 GDBM-1.18.1

El paquete GDBM contiene el gestor de bases de datos de GNU. Es una librería con funciones para bases de datos que usa hashing ampliable, y que funciona de forma similar al estándar UNIX dbm. 

**Tiempo aproximado de construcción** : 0.1 SBU

**Espacio requerido en el disco:**  11 MB

### 6.37.1 Instalación de GDBM

Se prepara GDBM para la compilación: 

```bash
./configure --prefix=/usr    \
            --disable-static \
            --enable-libgdbm-compat
```
El significado de las opciones de configuración: 

--enable-libgdbm-compat

Esta opción permite a libgdbm librería de compatibilidad ser construida, ya que algunos paquetes externos a LGS pueden necesitar las antiguas rutinas DBM que provee.

Se compila el paquete:

```bash
make
```
Se verifican los resultados, con:

```bash
make check
```
Se instala el paquete:

```bash
make install
```

### 6.37.2 Contenidos de GDBM

Programas instalados: gdbm_dump, gdbm_load y gdbm_tool

Librerías instaladas: libgdbm.so y libgdm_compact.so

Descripciones breves:


**gdbm_dump**   vuelca una base de datos GDBM  en un archivo.

**gdbm_load**     recrea la base de datos GDBM  de un archivo.

**gdbmtool**       verifica y modifica una base de datos GDBM. 

**libgdbm**        contiene funciones para manipular una base de datos hasheada.

**libgdbm_compat**   Librería de compatibilidad conteniendo funciones antiguas de DBM. 

## 6.38 Gperf-3.1

El paquete Gperf, genera funciones hash perfectas dado un conjunto de claves.

**Tiempo aproximado de construcción** : menos 0.1 SBU

**Espacio requerido en el disco:**  6.3 MB

### 6.37.1 Instalación de Gperf

Se prepara Gperf para la compilación:

```bash
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1
```
 Se compila el paquete:

```bash
make
```

Se sabe que algunos test fallan si se ejecutan muchos test simultáneamente (cuando la opción -j es mayor que 1). Para ver el resultado de los tests:

```bash
make -j1 check
```
Se instala el paquete:

```bash
make install
```






## 6.32.2 Contenidos de Gperf
Programas instalados: gperf

Directorios instalados: /usr/share/doc/gperf-3.1

Descripciones breves:

**gperf**   Genera una función hash perfecta de un conjunto de claves.



# 6.39 Expat-2.2.9

El paquete Expat contiene una librería de C para parsear XML.


**Tiempo aproximado de construcción** :  0.1 SBU

**Espacio requerido en el disco:**  11 MB

### 6.39.1 Instalación de Expat

Primeramente se ha de arreglar un problema con los test de regresión en el entorno LFS.

```bash
sed -i 's|usr/bin/env |bin/|' run.sh.in
```

Se prepara Expat para la compilación: 

```bash
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/expat-2.2.9
```

Se compila el paquete:

```bash
make install
```

Si se desea se instala la documentación:

```bash
install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.9
```

### 6.39.2  Contenidos de Expat

Programas instalados: xmlwf

Librerias Instaladas s: libexpat.so

Directorios instalados: /usr/share/doc/expat-2.2.9

Descripciones Breves:

**xmlwf** es una utilidad para verificar si documentos XML están correctamente formados.

**libexpat** Contiene una API para parsear XML.


# 6.40 Inetutils-1.9.4

El paquete Inetutils, contiene programas para sistemas de comunicación.


**Tiempo aproximado de construcción** :  0.3 SBU

**Espacio requerido en el disco:**  29 MB


### 6.40.1 Instalación de Inetutils

Se prepara Inetutils para la compilación:

```bash
./configure --prefix=/usr        \
            --localstatedir=/var \
            --disable-logger     \
            --disable-whois      \
            --disable-rcp        \
            --disable-rexec      \
            --disable-rlogin     \
            --disable-rsh        \
            --disable-servers
```
Significado de las opciones de compilación:

--disable-logger
Esta opción hace que Inetutils no instale el programa “logger”, que es usado por scritps para pasar mensajes al daemon de logs del sistema. No se instala, ya que Util-linux, instala una versión más actual.




--disable-whois
Esta opción deshabilita Inetutils para instalar el cliente “whois” que está desactualizado. Hay instrucciones para instalar un cliente “whois” mejor, en el libro BLFS.

--disable-r*
Este parámetro deshabilita la compilación de programas obsoletos, que no deberían ser usados, debido a vulnerabilidades de seguridad. Las funciones de estos programas pueden ser obtenidas del paquete de “openssh”, en el libro BLFS.


--disable-servers,
Esta opción deshabilita la instalación de varios servidores de red incluidos como parte del paquete Inetutils. Se considera que estos servidores, no son apropiados, para un sistema LFS básico. Algunos no son seguros y solo se consideran seguros en redes seguras. Nótese que hay opciones disponibles mejores para muchos de estos servidores.

Se compila el paquete:

```bash
make
```
Para testear los resultados, se ejecuta:

```bash
make test
```

NOTA: Un test, libls.sh, puede que falle en el entorno de chroot inicial, pero este test pasará correctamente, si se repite una vez el sistema LFS esté finalizado. Otro test, ping-localhost.sh, fallará si el sistema anfitrión to soporta ipv6.

Se instala el paquete:

```bash
make install
```
Movemos algunos programas para que estén disponibles, si /usr no está disponible. 

```bash
mv -v /usr/bin/{hostname,ping,ping6,traceroute} /bin
mv -v /usr/bin/ifconfig /sbin
```


### 6.40.2 Instalación de Inetutils


Programas instalados: 	dnsdomainname, ftp, ifconfig, hostname, ping, ping6, talk, telnet, tftp y traceroute


Descripciones Breves:

**dnsdomainname**  Muestra el nombre del dominio del sistema DNS.

**ftp**  El programa del protocolo de transferencia de ficheros.

**hostname**  Identifica o define el nombre del sistema anfitrión

**ifconfig**  Gestiona interfaces de red

**ping**  Manda una petición de repetición e informa cuanto tiempo ha pasado hasta que se ha recibido la respuesta.

**ping6**  La versión de ping para redes de ipv6

**talk**   Un chat con otro usuario

**telnet**  Interfaz al protocolo telnet

**tfpt** Un programa del protocolo de transferencia de archivos.

**traceroute** Traza la ruta de los paquetes desde el sistema en el que se está trabajando hasta otro sistema o red, mostrando los saltos intermedios (puertas de enlace) por el camino.



# 6.41 Perl-5.30.1

El paquete Perl, contiene el Practical Extraction and Report Language.

**Tiempo aproximado de construcción** :  9.2 SBU

**Espacio requerido en el disco:**  272 MB


### 6.41.1 Instalación de Perl

Primero se crea el archivo /etc/hosts que servirá como referencia para uno de los archivos de configuración de Perl, como también para un test. 

```bash
echo "127.0.0.1 localhost $(hostname)" > /etc/hosts
```
Esta versión de Perl, construirá los módulos  Compress::Raw::Zlib y Compress::Raw::BZip2.

Por defecto Perl usará una copia interna de las fuentes para la construcción. Se usa el siguiente comando para que Perl use las librerías existentes en el sistema.

```bash
export BUILD_ZLIB=False
export BUILD_BZIP2=0
```
Para tener un control total sobre cómo se define el comportamiento de Perl, se pueden quitar las opciones “-des” de los siguientes comandos, y elegir de qué forma se construye el paquete. Otra forma es usar los comandos exactamente como se definen a continuación para usar los valores por defecto que Perl, puede auto-detectar.

```bash
sh Configure -des -Dprefix=/usr                 \
                  -Dvendorprefix=/usr           \
                  -Dman1dir=/usr/share/man/man1 \
                  -Dman3dir=/usr/share/man/man3 \
                  -Dpager="/usr/bin/less -isR"  \
                  -Duseshrplib                  \
                  -Dusethreads
```

El significado de las opciones de compilación:

-Dvendorprefix=/usr

Esto asegura, que Perl, sabe cómo indicar a los paquetes donde deberían instalar sus módulos de Perl.

-Dpager="/usr/bin/less -isR"

Esto asegura que se use el programa “less” en vez del programa “more”.

-Dman1dir=/usr/share/man/man1 -Dman3dir=/usr/share/man/man3

Ya que el Groff no está aún instalado, “Configure” que no se quieren páginas de manual (man pages) para Perl. Con esta opci se sobreescribe esta opción.

-Duseshrplib
Se construye una librería compartida libperl, que necesitan algunos módulos de Perl. 

-Dusethreads
Se construye Perl para que soporte hilos de ejecución.

Se compila el paquete.



```bash
make
```

Se verifican los resultados de test (aproximadamente 11 SBU) 

```bash
make test
```

Se instala el paquete y se limpian archivos:

```bash
make install
unset BUILD_ZLIB BUILD_BZIP2
```


### 6.41.2 Contenidos de Perl

Programas instalados: 

corelist, cpan, enc2xs, encguess, h2ph, h2xs, instmodsh, json_pp, libnetcfg, perl, perl5.30.1 (hard link to perl), perlbug, perldoc, perlivp, perlthanks (hard link to perlbug), piconv, pl2pm, pod2html, pod2man, pod2text, pod2usage, podchecker, podselect, prove, ptar, ptardiff, ptargrep, shasum, splain, xsubpp y zipdetails


Librerías instaladas:  Muchas, y nó serán listadas aquí.


Descripciones breves:


**corelist** Una línea de comandos para Module:CoreList

**cpan**   Una línea de comandos para CPAN (Comprehensive Perl Archive Network)

**enc2xs**   Construye extensiones de Perl para codificar módulos desde mapeos con caracteres Unicode o archivos codificados Tcl.

**encguess**  Identifica el tipo de codificación de uno o varios archivos.

**h2ph**  Convierte una cabecera de C .h en una cabecera de Perl .ph

**h2xs**   Convierte una cabecera de C .h en extensiones de Perl

**instmodsh** Un scrpit para examinar módulos instalados de Perl, y crear un archivo comprimido tarball desde un módulo instalado.

**json_pp** Convierte el formato de unos datos desde una entrada a una salida.

**libnetcfg** Se puede usar para configurar el módulo de Perl libnet.

**perl** Combina alguna de las funcionalidades de C, sed, awk y sh en un único lenguaje

**perl5.30.1** Un link a Perl

**perlbug** Se usa para generar reportes de bugs sobre Perl u otro de los módulos que vienen con él, y reportarlos por email.

**perldoc**  Muestra un apieza de documentación en formato pod que se integra en el árbol de instalación de Perol o en un script de Perl.

**perlivp** Procedimiento de verificación de la instalación de Perl. Se puede usar para verificar que Perl y sus librerías han sido instalados correctamente.

**perlthanks** Se usa para generar un mensaje de agradecimiento y enviarlo por email a los desarrolladores de Perl.

**piconv** La versión de Perl del convertidor de codificación de caracteres iconv.

**pl2pm** Una herramienta para convertir archivos Perl4 .pl a módulos de Perl5 .pm

**pod2html** Convierte archivos en formato pod a formato HTML.

**pod2man** Convierte pod de datos a una entrada formateada *roff.

**pod2text** Convierte pod de datos a texto formateado en ASCII

**pod2usage** Imprime mensajes de uso desde documentos pod en archivos.

**podchecker**  Verifica la sintaxis de archivos de documentación pod.

**podselect**  Muestra las secciones seleccionadas de documentación pod.

**prove**   Herramienta para la línea de comandos para ejecutar tests contra el módulo Test::Harness.

**ptar** Un programa como tar, escrito en Perl.

**ptardiff** Un programa de Perl que compara un archivo extraído con un archivo no extraído.

**ptargrep** Un programa de Perl, que aplica la coincidencia de patrones a los contenidos de archivos comprimidos en tar.

**shasum** Muestra o verifica un checksum SHA

**splain** Es usado para forzar avisos explícitos en los diagnósticos de Perl.

**xsubpp** Convierte código Perl XS en código C.

**zipdetails** Muestra detalles sobre la estructura interna de un archivo Zip.



# 6.42 XML::Parser-2.46

El módulo XML::Parser es un iterfaz de Perl al parseador de XML de James Clark, Expat.

**Tiempo aproximado de construcción** :  menos de 0.1 SBU

**Espacio requerido en el disco:**  2.4 MB


### 6.42.1 Instalación de Inetutils

Se prepara XML::Parser para la compilación:

```bash
perl Makefile.pl
```
Se compila el paquete:

```bash
make
```
Para verificar los test, se ejecuta:

```bash
make test
```
Se instala el paquete:

```bash
make install
```

### 6.42.2 Contenidos de XML::Parser

Módulo instalado:  Expat.so

Descripción breve:

**Expat**      Provee una interfaz para Expat Perl




# 6.43 Intltool-0.51.0

Intltool es una herramienta para la internacionalización de textos, que se usa para extraer textos traducibles del código.

**Tiempo aproximado de construcción** :  menos de 0.1 SBU

**Espacio requerido en el disco:**  1.5 MB


### 6.43.1 Instalación de Intltool:

Primero se arregla una advertencia causada por perl-5.22 y posteriores:

```bash
sed -i 's:\\\${:\\\$\\{:' intltool-update.in
```

Se prepara Intltool para la compilación:

```bash
./configure --prefix=/usr
```
Se compila el paquete:

```bash
make
```
Para verificar los test, se ejecuta:
```bash
make check
```

Se instala el paquete:
```bash
make install
install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO
```

### 6.43.2 Contenidos de Intltool:

**Programas instalados**: intltool-extract, intltool-merge, intltool-prepare, intltool-update y intltoolize

**Directorios instalados**: /usr/share/doc/intltool-0.51.0 y /usr/share/intltool

Descripciones breves:



**intltoolize**    Prepara un paquete para usar intltool

**intltool-extract**    Genera una fichero de cabecera para que pueda ser leído por gettext

**intltool-merge**      Unifica textos traducidos en varios tipos de archivos

**intltool-prepare**     Actualiza los archivos pot y los une con los archivos de las traducciones. 

**intltool-update**     Actualiza las archivos de plantillas y los une con los de las traducciones.









# 6.44 Autoconf-2.69

El paquete Autoconf contiene un programa que genera scripts que pueden configurar automáticamente código fuente.

**Tiempo aproximado de construcción** :  menos de 0.1 SBU (sobre 3.2 SBU con los tests)

**Espacio requerido en el disco:**  79 MB


### 6.44.1 Instalación de Autoconf:

Primero se arregla un problema, causado por Perl 5.28.

```bash
sed '361 s/{/\\{/' -i bin/autoscan.in
``` 
Se prepara Autoconfig  para la compilación:

```bash
./configure --prefix=/usr
```

Se compila el paquete

```bash
make
```

Los tests, no funcionan actualmente, debido a bash-5 y libtool-2.4.3. Para ejecutar igualmente los test, se ejecuta:

```bash
make check
```

Se instala el paquete

```bash
make install
```


### 6.44.2 Contenidos de Autoconf:

**Programas instalados**: autoconf, autoheader, autom4te, autoreconf, autoscan, autoupdate y ifnames

**Directorios instalados**: /usr/share/autoconf

**Descripciones breves**:

**autoconf**      Genera scripts que configuran automáticamente el código fuente para adaptarlo a varios tipos de sistemas de tipo Unix.Los scripts de configuración que produce son independientes y no necesitan que el programa autoconf esté instalado.

**autoheader**    Una herramienta para generar archivos plantilla de definiciones de C #define para usar.

**autom4te**       Un envoltorio para el procesador de macros M4.

**autoreconf**      Ejecuta automáticamente autoconf, autoheader, aclocal, automake, gettextize y libtoolize en el orden correcto para
ahorrar tiempo cuando se hacen cambios en las plantillas de autoconf y automake.

**autoscan**     Ayuda a crear un archivo configure.in para el paquete de software; examina los archivos de código fuente en el árbol de directorios,
y busca en ellos por posibles problemas de portabilidad, y crea un archivo configure.scan que sirve como archivo preliminar de configure.in para 
el paquete. 

**autoupdate**    Modifica el archivo configure.in que llama a las macros de autoconf por sus nombres antiguos y usas los nombres actuales de las macros.

**ifnames**      Ayuda cuando se escriben los ficheros configure.in para un paquete de software. Imprime el identificador del paquete y usar
directivas de preprocesador de C condicionales [Si un paquete ya se ha definido para tener cierta portabilidad, este programa puede
ayudar a determinar que configuración necesita ser verificada. También puede también corregir los huecos en un archivo configure.ini generado 
por autoscan.]



# 6.45 Automake-1.16.1

El paquete Automake contiene programas para generar Makefiles para el uso con Autoconf.

**Tiempo aproximado de construcción** :  menos de 0.1 SBU (sobre 8.1 SBU con los tests).

**Espacio requerido en el disco:**  107 MB


### 6.45.1 Instalación de Automake:


Se prepara Automake para la compilación:

```bash
./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.1
```

Se compila el paquete:

```bash
make
```

Usando la opción -j4 con make, se aumenta la velocidad de ejecución de los tests, incluso en sistemas con un único procesador, debido a retraso de  tiempos internos en los tests individuales. Para verificar los tests, se ejecuta:

```bash
make -j4 check
```

Se sabe que falla un tests, en el entorno LFS: subobj.sh.

Se instala el paquete:

```bash
make install
``` 


### 6.45.1 Contenidos de Automake:


**Programas instalados**: aclocal, aclocal-1.16 (enlazado con aclocal), automake, and automake-1.16 (enlazado con  automake)

**Directorios instalados**:/usr/share/aclocal-1.16, /usr/share/automake-1.16 y /usr/share/doc/automake-1.16.1








**Descripciones breves**:


**aclocal**:    Genera archivos aclocal.m4 basados en los contenidos de los archivos configure.in

**aclocal-1.16**:   un enlace a aclocal

**automake**:    una herramienta para generar automáticamente archivos Makefile.ini desde archivos Makefile.am (Para crear todos los archivos 
Makefile.in para un paquete, se ha de ejecutar este programa en el directorio de más alto nivel. Al escanear por el archivo configure.in, automáticamente encuentra  cada archivo Makefile.am y genera el correspondiente Makefile.in

**automake-1.16**   enlace a automake




# 6.46 Kmod-26

El paquete Kmod, contiene librerías y utilidades para cargar módulos en el kernel.

**Tiempo aproximado de construcción** :  0.1 SBU.

**Espacio requerido en el disco:**  13 MB


### 6.46.1 Instalación de Kmod:

Se prepara Kmod para la compilación:

```bash
./configure --prefix=/usr          \
            --bindir=/bin          \
            --sysconfdir=/etc      \
            --with-rootlibdir=/lib \
            --with-xz              \
            --with-zlib
```

El significado de las opciones de configuración:

--with-xz, --with-zlib

Esta opción permite que Kmod, pueda gestionar módulos del kernel comprimidos.

--with-rootlibdir=/lib


Esta opción asegura que diferentes archivos de la librería se guardan en los directorios correctos. 

Se compila el paquete:

```bash
make
```

Este paquete no está acompañado de un conjunto de tests que se pueda ejecutar en el entorno chroot de LFS. Como mínimo se necesita el programa git, y varios test, no se ejecutarán fuera de un repositorio git. 

Se instala el paquete y se crean links simbólicos por compatibilidad con Module-Init-Tools (el paquete que previamente gestionaba los módulos del kernel de Linux):

```bash
make install

for target in depmod insmod lsmod modinfo modprobe rmmod; do
  ln -sfv ../bin/kmod /sbin/$target
done

ln -sfv kmod /bin/lsmod
```


### 6.46.2 Contenidos de Kmod:


**Programas instalados**: depmod (enlace a kmod), insmod (enlace a kmod), kmod, lsmod (enlace a kmod), modinfo (enlace a kmod), modprobe (enlace akmod), y rmmod (enlace a kmod)

**Librerías instaladas**: libkmod.so

**Descripciones breves**:


**depmod**      Crea un archivo de dependencias basado en los símbolos que encuentran en un grupo de módulos dados. Este archivo de 
dependencias es usado por ‘modprove’ para cargar automáticamente los módulos requeridos.

**insmod**    Instala un módulo en el kernel en ejecución.

**kmod**    carga y retira módulos del kernel.

**lsmod**   Lista los módulos que están cargados.

**modinfo**   Examina un archivo objeto asociado con un módulo del kernel y muestra su información.

**modprobe**    Usa un archivo de dependencias, creado por depmod, y automáticamente carga los módulos relevantes.

**rmmod**      Retira módulos del kernel en ejecución.

libkmod   Esta libreria se usa por otros programas para cargar y descargar módulos del kernel. 






# 6.47 Gettext-0.201

El paquete Gettext contiene utilidades para la internacionalización y localizaciones. Esto permite a los programas ser compilados con NLS (Soporte de Lenguaje Nativo, del inglés:Native Language Support), permitiéndoles mostrar mensajes en el lenguaje nativo del usuario.

**Tiempo aproximado de construcción** :  2.7 SBU.

**Espacio requerido en el disco:**  249 MB


### 6.47.1 Instalación de Gettext

Se prepara Gettext para la compilación:

```bash
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/gettext-0.20.1
```

Se compila el paquete:

```bash
make
```

Se verifican los resultados de los tests (esto lleva  bastante tiempo, aproximadamente 3 SBUs), se ejecutan con:


```bash
make check
```
Se instala el paquete:

```bash
make install
chmod -v 0755 /usr/lib/preloadable_libintl.so
```

### 6.47.1 Contenidos de Gettext


**Programas instalados**: autopoint, envsubst, gettext, gettext.sh, gettextize, msgattrib, msgcat, msgcmp, msgcomm, msgconv, msgen, msgexec, msgfilter, msgfmt, msggrep, msginit, msgmerge, msgunfmt, msguniq, ngettext, recode-sr-latin y xgettext

**Librerías instaladas**: libasprintf.so, libgettextlib.so, libgettextpo.so, libgettextsrc.so, libtextstyle.so y preloadable_libintl.so

**Directorios instalados**: /usr/lib/gettext, /usr/share/doc/gettext-0.20.1, /usr/share/gettext y /usr/share/gettext-0.19.8

**Descripciones breves**:

**autopoint**     Copia los archivos de la infraestructura estándar de Gettext in un paquete fuente

**envsubst**     Substituye las variables de entorno del cadénas formateadas en el terminal.

**gettext**       Traduce un mensaje en lenguaje natural en el lenguaje del usuario, mirando la traducción del mensaje en un catálogo de mensajes.

**gettext.sh**    Principalmente sirve como librería de funciones de gettext para el terminal.

**gettextize**    Copia todos los ficheros estándar de Gettext en un directorio que se le indique o en un paquete para empezar a internacionalizar.

**msgattrib**     Filtra los mensajes de un catálogo de mensajes de acuerdo a sus atributos y gestiona los atributos.

**msgcat**        Concatena y une los archivos .po indicados. 

**msgcmp**        Compara dos archivos .po para verificar que ambos contienen el mismo conjunto de cadenas de texto msgid.

**msgcomm**       Encuentra los mensajes que son comunes para unos archivos .po indicados. 

**msgconv**       Convierte un catálogo de traducciones a una codificación de caracteres distinta.

**msgen**         Crea un catálogo de traducciones al inglés.

**msgexec**       Aplica un comando a todas las traducciones de un catálogo de traducciones. 

**msgfilter**     Aplica un filtro a todas las traducciones de un catálogo de traducciones. 

**msgfmt**        Genera un binario de un catálogo de mensajes a partir  un catálogo de mensajes. 

**msggrep**       Extrae todos los mensajes de un catálogo de traducciones que cumplan un patrón indicado o pertenezcan a unos ficheros fuente indicados. 

**msginit**       Crea un nuevo fichero .po, inicializando la metainformación con valores del entorno del usuario.

**msgmerge**      Combina dos traducciones en un único archivo.

**msgunfmt**      Decompila un binario de un catálogo de mensajes en una texto de traducciones. 

**msguniq**       Unifica traducciones duplicadas en un catálogo de traducciones. 

**ngettext**      Muestra las traducciones de lenguaje nativas de un mensaje textual cuya forma gramática dependa en un número.

**recode-sr-latin**    Recodifica texto Serbio de Cirílico a alfabeto Latino. 

**xgettext**        Extrae las líneas de mensajes traducibles de unos archivos de origen dados para crear una primera plantilla de traducciones.

**libasprintf**    Define la clase autosprintf, que hace que rutinas de texto formateado en C sean usables por programas en C++, para usar con cadenas de texto <string> y flujos <iostream>.

**libgettextlib**   Una librería privada, que contiene rutinas comunes que son usadas por varios programas de Gettext, y que no están orientadas a un uso general.

**libgettextpo**      Se usa para desarrollar programas especializados que procesen archivos .po. Esta libreria se usa cuando las aplicaciones estándar usan Gettext (como msgcomm, msgcmp, msgattrib y msgen) no sean suficientes. 

**libgettextsrc**    Una librería privada, que contiene rutinas comunes que son usadas por varios programas de Gettext, y que no están orientadas a un uso general.

**libtextstyle**    Una librería para estilos de texto. 

**preloadable_libintl**      Una librería que tiene como finalidad ser usada por LD_PRELOAD y que ayuda libintl en registrar mensajes no traducidos. 


